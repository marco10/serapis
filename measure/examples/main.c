#include <stdio.h>
#include <time.h>
#include <math.h>
#include "rapl.h"

//#include "ex2/pb.h"
//#include "ex1/fib_fat.h"

#define RUNTIME 1


int main (int argc, char **argv) 
{ char command[500],res[500];
  int  ntimes = 1, mtimes = 10;
  int  core = 0;
  int  i=0, j=0;

#ifdef RUNTIME
  printf("%s\n", "RUNTIME set!");
  clock_t begin, end;
  double time_spent;

  struct timeval tvb,tva;
#endif
  
  FILE * fp;

  // printf("Program to be executed: %d",argc);
  //strcpy( command, "./" );
  if (argc < 3){
    printf("ERROR: Invalid number of arguments \n");
    return 1;
  }
  strcpy(command,argv[1]);
  printf("Program to be executed: %s\n",command);

  ntimes = atoi(argv[2]);

  if(ntimes <= 0 || mtimes <= 0){
    printf("ERROR: Invalid values for #measurements and/or #repetitions\n");
    return 1;
  }

  strcpy(res,command);
  strcat(res,".J");
  printf("Command: %s  %d-times res: %s\n",command,ntimes,res);
  

  printf("\n\n RUNNING THE PARAMETRIZED PROGRAM:  %s\n\n\n",command);

  fp = fopen(res,"w");
  rapl_init(core);

  //fprintf(fp,"Package ; Core(s) ; GPU ; DRAM? ; Time (us) \n");
  
  int val = 0;
  for(j=0; j<mtimes; j++){
    rapl_before(fp,core);
      
    #ifdef RUNTIME
      begin = clock();
      gettimeofday(&tvb, 0);
    #endif
    for (i = 0 ; i < ntimes ; i++){
      //mainA(1,"");
      printf("OLA\n");
    }
    #ifdef RUNTIME
      end = clock();
      gettimeofday(&tva, 0);
      //time_spent = (double)(end - begin) / CLOCKS_PER_SEC;
      time_spent = (tva.tv_sec-tvb.tv_sec)*1000000 + tva.tv_usec-tvb.tv_usec;
    #endif
      rapl_after(fp,core);

    #ifdef RUNTIME	
      fprintf(fp," %G \n",time_spent);
    #endif	
  }
    
  printf("\n\n END of PARAMETRIZED PROGRAM: \n");

  fclose(fp);
  fflush(stdout);

  return 0;
}
