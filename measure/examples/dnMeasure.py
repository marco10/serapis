#!/usr/bin/python

from subprocess import call, check_output
from decimal import *
from argGenerator import *

import re, os

sep = os.sep

NTIMES = 1 #NTIMES = 400000
MTIMES = 200
SLEEPTIME = 1

OUTFILE = 'mainA'

dict_ex_0 = [
        {'name' : 'exp1', 'cpp': [''], 'path' : 'ex1', 'inputs' : []}
        ]
        # 'inputs' can be: (int|long|float|double|string|char), [min..max]
        #                  (int|long|float|double|string|char), [n1, n2, ..., N]
        #                  files[f1, f2, ..., fN]


home = "/home/marco/repos/serapis"
main_pwd = home+"/SPLs/MAT_FEUP/src/"
dict_ex = [
        #{'name' : '1-Original-NoZ3', 'cpp': [''], 'path' : main_pwd + '1-Original-NoZ3', 'inputs' : []},
        #{'name' : '2-Original-Z3', 'cpp': [''], 'path' : main_pwd + '2-Original-Z3', 'inputs' : []},
        #{'name' : '3-Unchecked', 'cpp': [''], 'path' : main_pwd + '3-Unchecked', 'inputs' : []},
        #{'name' : '4-Modified1', 'cpp': [''], 'path' : main_pwd + '4-Modified1', 'inputs' : []},
        {'name' : '5-Modified2', 'cpp': [''], 'path' : main_pwd + '5-Modified2', 'inputs' : []},
        {'name' : '6-ModifiedC-NoUnnecessaryIntermediates', 'cpp': [''], 'path' : main_pwd + '6-ModifiedC-NoUnnecessaryIntermediates', 'inputs' : []},
        {'name' : '7-ModifiedC-ManualInterchange', 'cpp': [''], 'path' : main_pwd + '7-ModifiedC-ManualInterchange', 'inputs' : []},
        {'name' : 'nbody', 'cpp': [''], 'path' : home + '/examples/nbody', 'inputs' : '5000'}
]

def randInput(testDescr):
  randVal = ''
  strVals = []
  for descr in testDescr:
    if len(descr) == 2:
      tp = descr[0]
      if ',' in descr[1]:
        strVals = re.split(',', (descr[1].replace('[','').replace(']','')))
        rv = strVals[int(randIntString('', 0, len(strVals)-1))]
        if tp == 'files':
          f = open('workfile', 'r+')
          randVal += f.read()
        else:
          randVal += strVals[int(randIntString('', 0, len(strVals)-1))]
      else:
        aux = re.split('\\.\\.', (descr[1].replace('[','').replace(']','')))
        try:
          minV = aux[0]
          maxV = aux[1]
        except Exception as e:
          raise
        if tp == 'int':
          randVal += randIntString('', int(minV), int(maxV))
        elif tp == 'long':
          randVal += randLongString('', long(minV), long(maxV))
        elif tp == 'float':
          randVal += randFloatString('', float(minV), float(maxV))
        elif tp == 'double':
          randVal += randDoubleString('', float(minV), float(maxV))
        elif tp == 'string':
          randVal += randString('', int(maxV))
        elif tp == 'char':
          randVal += randCharString()
        elif tp == 'files':
          pass
        else:
          print('something went wrong...')
    randVal += '\n'
  return randVal

def analyzeInputType(descr):
  tp, minV, maxV, values = 'unknown', 0, 0, []
  case = descr[int(randIntString('', 0, len(descr)-1))]
  print(case)
  r = randInput(case)
  values = [r for i in range(NTIMES)]
    
  return tp, minV, maxV, values

def genInputValues(cont, descr):
  tp, minV, maxV, values = analyzeInputType(descr)
  content = ''
  if(cont == (MTIMES/2)):
  	values = [str(maxV) for i in range(NTIMES)]
  for i in values:
    content += i
  f = open('values','w+')
  f.write(content)
  f.close()

def parseMeasures():
  data = open(OUTFILE + '.J')
  dat = data.read()
  lst = dat.splitlines()
  cpu = []
  for elem in lst:
    values = elem.split(';')
    if len(values) == 5:
      cpu.append(Decimal(values[1]))
    else:
      cpu.append(Decimal(0))
  return cpu

def execute(cpp, path, inputs):
  measures = []
  defs = ''
  lst = []
  allFiles, cfiles =  [], []
  if len(cpp) > 0:
    lst = cpp.split(' ')
  for c in lst:
    defs += '-D ' + c + ' '
  #get C files to execute
  for r, p, f in os.walk(path):
    for file in f:
      allFiles.append(r + sep + file)
      if file.endswith('.c'):
        cfiles.append(r + sep + file)
  sources = ''.join(list(map(lambda x: re.sub(path+sep, '', x) + ' ', cfiles)))
  sources = 'main.c ' + sources
  allF = ''.join(list(map(lambda x: re.sub(path+sep, '', x) + ' ', allFiles)))
  
  check_output('cp -r ' + path+sep +'* .', shell=True)
  for x in range(MTIMES):
    #print '[EXECUTION ' + str(x+1) + ']'
    #generate input values
    #genInputValues(x, inputs)
    
    call(['sleep', str(SLEEPTIME)])
    call(['make', 'clean'])
    call(['make', 'DEFS=' + defs +'', 'CFILES=' + sources])
    call(['make', 'measure', 'OUTFILE=' + OUTFILE, 'NTIMES=' + str(NTIMES), 'INPUTS='+inputs])
    call(['sleep', str(SLEEPTIME)])
    #catch the result, save it in 'measures'
    cons = parseMeasures()
    measures.append(cons[0]/Decimal(NTIMES))
  check_output('rm -rf '+ allF, shell=True)
  return measures

def saveFile(filename, content):
  #write to file
  f = open(filename,'w+')
  f.write(content)
  f.close()

def main():
  #testF()
  #genInputValues(10, [[('int', '[1..20]')]])
  for elem in dict_ex_0:
    path = elem['path']
    inputs = elem['inputs']
    allProducts = elem['cpp']
    content = '::: TESTING \'' + elem['name'] + '\' :::\n'
    for p in allProducts:
      measures = execute(p, path, inputs)
      maxC = max(measures)
      content += '\tFEATURES  \'' + p + '\' -> MAX CONSUMPTION: ' + str(maxC) + '| meas: ' + str(measures) + '\n'
    saveFile(str(elem['name']) + '.log', content)

if __name__ == '__main__':
  main()
