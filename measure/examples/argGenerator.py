#!/usr/bin/python

import random, string, sys

INT_MAX = 2147483647
INT_MIN = -2147483647
LONG_MAX = 9223372036854775807
LONG_MIN = -9223372036854775807
FLOAT_MAX = 340282346638528859811704183484516925440
FLOAT_MIN = -340282346638528859811704183484516925440
DOUBLE_MAX = 179769313486231570814527423731704356798070567525844996598917476803157260780028538760589558632766878171540458953514382464234321326889464182768467546703537516986049910576551282076245490090389328944075868508455133942304583236903222948165808559332123348274797826204144723168738177180919299881250404026184124858368
DOUBLE_MIN = -179769313486231570814527423731704356798070567525844996598917476803157260780028538760589558632766878171540458953514382464234321326889464182768467546703537516986049910576551282076245490090389328944075868508455133942304583236903222948165808559332123348274797826204144723168738177180919299881250404026184124858368

def randFloatString(default='', minVal=INT_MIN, maxVal=FLOAT_MAX):
  if len(default):
    return default
  else:
    return str(random.uniform(minVal, maxVal))

def randIntString(default='', minVal=INT_MIN, maxVal=INT_MAX):
  if len(default):
    return default
  else:
    return str(random.randint(minVal, maxVal))

def randLongString(default='', minVal=LONG_MIN, maxVal=LONG_MAX):
  if len(default):
    return default
  else:
    return str(random.randint(minVal, maxVal))

def randString(default='', size=random.randint(1,30), chars=string.ascii_uppercase + string.ascii_lowercase + string.digits):
  if len(default):
    return default
  else:
    return ''.join(random.choice(chars) for _ in range(size))

def randCharString(default=''):
  return randString('', 1)

def randDoubleString(default='', minVal=FLOAT_MIN, maxVal=FLOAT_MAX):
  if len(default):
    return default
  else:
    return str(random.uniform(minVal, maxVal))

#random list of values
def randIntList(size, minVal=INT_MIN, maxVal=INT_MAX):
  r = ''
  for x in range(size):
    r += randIntString('',minVal,maxVal) + '\n'
  return r

def randLongList(size, minVal=LONG_MIN, maxVal=LONG_MAX):
  r = ''
  for x in range(size):
    r += randLongString('',minVal,maxVal) + '\n'
  return r

def randFloatList(size, minVal=FLOAT_MIN, maxVal=FLOAT_MAX):
  r = ''
  for x in range(size):
    r += randFloatString('',minVal,maxVal) + '\n'
  return r

def randDoubleList(size, minVal=DOUBLE_MIN, maxVal=DOUBLE_MAX):
  r = ''
  for x in range(size):
    r += randDoubleString('',minVal,maxVal) + '\n'
  return r


def randCharList(size):
  r = ''
  for x in range(size):
    r += randCharString() + '\n'
  return r


def randStringList(size):
  r = ''
  for x in range(size):
    r += randString() + '\n'
  return r