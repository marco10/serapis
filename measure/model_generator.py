#!/usr/bin/python

from subprocess import call
from decimal import *
from convolution import *

import csv, json, sys
from argGenerator import *

MODELFILE = 'model.csv'
OUTFILE = 'mainA'
FUNCFILE = 'stmts/funCall.c'

JSON_INST = 'instructions.json'
JSON_FUNC = 'functions.json'

NTIMES = 600000     #NTIMES = 20000000
MTIMES = 200        #MTIMES = 100
SLEEPTIME = 5       #
OUTLIERS = 0        #OUTILER = 10

delta = 0.55

OUT_REM_POLICY = 'MAX_MIN'  # 'MAX_MIN', 'MAX', 'MIN'

model_dict0 = [{'code' : 0, 'cpp' : 'NULL', 'dependencies' : [], 'measures' : []}]

model_dict = [
        {'code' : 0, 'cpp' : 'NULL', 'dependencies' : [], 'measures' : []},                         #
        {'code' : 1, 'cpp' : 'DEC_INT_INIT', 'dependencies' : [0], 'measures' : []},                #
        {'code' : 2, 'cpp' : 'DEC_FLOAT_INIT', 'dependencies' : [0], 'measures' : []},              #
        {'code' : 3, 'cpp' : 'DEC_DOUBLE_INIT', 'dependencies' : [0], 'measures' : []},             #
        {'code' : 4, 'cpp' : 'ASS_VAR_INT', 'dependencies' : [0], 'measures' : []},                 #
        {'code' : 5, 'cpp' : 'ASS_VAR_FLOAT', 'dependencies' : [0], 'measures' : []},               #
        {'code' : 6, 'cpp' : 'ASS_VAR_DOUBLE', 'dependencies' : [0], 'measures' : []},              #
        {'code' : 7, 'cpp' : 'ASS_VAR_VAR_INT', 'dependencies' : [0,4], 'measures' : []},           #
        {'code' : 8, 'cpp' : 'ASS_VAR_VAR_FLOAT', 'dependencies' : [0,5], 'measures' : []},         #
        {'code' : 9, 'cpp' : 'ASS_VAR_VAR_DOUBLE', 'dependencies' : [0,6], 'measures' : []},        #
        {'code' : 10, 'cpp' : 'ASS_ADD_INT_INT', 'dependencies' : [0], 'measures' : []},            #
        {'code' : 11, 'cpp' : 'ASS_ADD_FLOAT_FLOAT', 'dependencies' : [0], 'measures' : []},        #
        {'code' : 12, 'cpp' : 'ASS_ADD_DOUBLE_DOUBLE', 'dependencies' : [0], 'measures' : []},      #
        {'code' : 13, 'cpp' : 'ASS_ADD_VAR_VAR_INT', 'dependencies' : [0,4,4], 'measures' : []},      #--[0,4,4]
        {'code' : 14, 'cpp' : 'ASS_ADD_VAR_VAR_FLOAT', 'dependencies' : [0,5,5], 'measures' : []},    #--[0,5,5]
        {'code' : 15, 'cpp' : 'ASS_ADD_VAR_VAR_DOUBLE', 'dependencies' : [0,6,6], 'measures' : []},   #--[0,6,6]
        {'code' : 16, 'cpp' : 'ASS_ADD_VAR_INT', 'dependencies' : [0,4], 'measures' : []},          #
        {'code' : 17, 'cpp' : 'ASS_ADD_VAR_FLOAT', 'dependencies' : [0,5], 'measures' : []},        #
        {'code' : 18, 'cpp' : 'ASS_ADD_VAR_DOUBLE', 'dependencies' : [0,6], 'measures' : []},       #
        {'code' : 19, 'cpp' : 'ASS_SUB_INT_INT', 'dependencies' : [0], 'measures' : []},            #
        {'code' : 20, 'cpp' : 'ASS_SUB_FLOAT_FLOAT', 'dependencies' : [0], 'measures' : []},        #
        {'code' : 21, 'cpp' : 'ASS_SUB_DOUBLE_DOUBLE', 'dependencies' : [0], 'measures' : []},      #
        {'code' : 22, 'cpp' : 'ASS_SUB_VAR_VAR_INT', 'dependencies' : [0,4,4], 'measures' : []},      #--[0,4,4]
        {'code' : 23, 'cpp' : 'ASS_SUB_VAR_VAR_FLOAT', 'dependencies' : [0,5,5], 'measures' : []},    #--[0,5,5]
        {'code' : 24, 'cpp' : 'ASS_SUB_VAR_VAR_DOUBLE', 'dependencies' : [0,6,6], 'measures' : []},   #--[0,6,6]
        {'code' : 25, 'cpp' : 'ASS_SUB_VAR_INT', 'dependencies' : [0,4], 'measures' : []},          #
        {'code' : 26, 'cpp' : 'ASS_SUB_VAR_FLOAT', 'dependencies' : [0,5], 'measures' : []},        #
        {'code' : 27, 'cpp' : 'ASS_SUB_VAR_DOUBLE', 'dependencies' : [0,6], 'measures' : []},       #
        {'code' : 28, 'cpp' : 'ASS_MUL_INT_INT', 'dependencies' : [0], 'measures' : []},            #
        {'code' : 29, 'cpp' : 'ASS_MUL_FLOAT_FLOAT', 'dependencies' : [0], 'measures' : []},        #
        {'code' : 30, 'cpp' : 'ASS_MUL_DOUBLE_DOUBLE', 'dependencies' : [0], 'measures' : []},      #
        {'code' : 31, 'cpp' : 'ASS_MUL_VAR_VAR_INT', 'dependencies' : [0,4,4], 'measures' : []},      #--[0,4,4]
        {'code' : 32, 'cpp' : 'ASS_MUL_VAR_VAR_FLOAT', 'dependencies' : [0,5,5], 'measures' : []},    #--[0,5,5]
        {'code' : 33, 'cpp' : 'ASS_MUL_VAR_VAR_DOUBLE', 'dependencies' : [0,6,6], 'measures' : []},   #--[0,6,6]
        {'code' : 34, 'cpp' : 'ASS_MUL_VAR_INT', 'dependencies' : [0,4], 'measures' : []},          #
        {'code' : 35, 'cpp' : 'ASS_MUL_VAR_FLOAT', 'dependencies' : [0,5], 'measures' : []},        #
        {'code' : 36, 'cpp' : 'ASS_MUL_VAR_DOUBLE', 'dependencies' : [0,6], 'measures' : []},       #
        {'code' : 37, 'cpp' : 'ASS_DIV_INT_INT', 'dependencies' : [0], 'measures' : []},            #
        {'code' : 38, 'cpp' : 'ASS_DIV_FLOAT_FLOAT', 'dependencies' : [0], 'measures' : []},        #
        {'code' : 39, 'cpp' : 'ASS_DIV_DOUBLE_DOUBLE', 'dependencies' : [0], 'measures' : []},      #
        {'code' : 40, 'cpp' : 'ASS_DIV_VAR_VAR_INT', 'dependencies' : [0,4,4], 'measures' : []},      #--[0,4,4]
        {'code' : 41, 'cpp' : 'ASS_DIV_VAR_VAR_FLOAT', 'dependencies' : [0,5,5], 'measures' : []},    #--[0,5,5]
        {'code' : 42, 'cpp' : 'ASS_DIV_VAR_VAR_DOUBLE', 'dependencies' : [0,6,6], 'measures' : []},   #--[0,6,6]
        {'code' : 43, 'cpp' : 'ASS_DIV_VAR_INT', 'dependencies' : [0,4], 'measures' : []},          #
        {'code' : 44, 'cpp' : 'ASS_DIV_VAR_FLOAT', 'dependencies' : [0,5], 'measures' : []},        #
        {'code' : 45, 'cpp' : 'ASS_DIV_VAR_DOUBLE', 'dependencies' : [0,6], 'measures' : []},       #
        {'code' : 46, 'cpp' : 'CMP_VAR_INT', 'dependencies' : [0,4], 'measures' : []},              #
        {'code' : 47, 'cpp' : 'CMP_VAR_FLOAT', 'dependencies' : [0,5], 'measures' : []},            #
        {'code' : 48, 'cpp' : 'CMP_VAR_DOUBLE', 'dependencies' : [0,6], 'measures' : []},           #
        {'code' : 49, 'cpp' : 'CMP_VAR_VAR_INT', 'dependencies' : [0,4,4], 'measures' : []},          #--[0,4,4]
        {'code' : 50, 'cpp' : 'CMP_VAR_VAR_FLOAT', 'dependencies' : [0,5,5], 'measures' : []},        #--[0,5,5]
        {'code' : 51, 'cpp' : 'CMP_VAR_VAR_DOUBLE', 'dependencies' : [0,6,6], 'measures' : []},       #--[0,6,6]
        {'code' : 52, 'cpp' : 'ASS_VAR_CHAR', 'dependencies' : [0], 'measures' : []},
        {'code' : 53, 'cpp' : 'ASS_VAR_VAR_CHAR', 'dependencies' : [0,52], 'measures' : []},
        {'code' : 54, 'cpp' : 'CMP_VAR_CHAR', 'dependencies' : [0,52], 'measures' : []},
        {'code' : 55, 'cpp' : 'CMP_VAR_VAR_CHAR', 'dependencies' : [0,52,52], 'measures' : []},
        {'code' : 56, 'cpp' : 'DEC_ARRAY', 'dependencies' : [0], 'measures' : []},
        {'code' : 57, 'cpp' : 'DEC_STRUCT', 'dependencies' : [0], 'measures' : []},
        {'code' : 58, 'cpp' : 'ASS_VAR_ADDR', 'dependencies' : [0], 'measures' : []},
        {'code' : 60, 'cpp' : 'ADDR_CALC', 'dependencies' : [0,4,56], 'measures' : []},
        {'code' : 61, 'cpp' : 'ARR_POS_LOAD', 'dependencies' : [0,4,56], 'measures' : []},
        {'code' : 62, 'cpp' : 'ASS_VAR_REF_INT', 'dependencies' : [0,4,58], 'measures' : []},
        {'code' : 63, 'cpp' : 'ASS_VAR_REF_CHAR', 'dependencies' : [0,52,58], 'measures' : []},
        {'code' : 64, 'cpp' : 'ASS_VAR_REF_FLOAT', 'dependencies' : [0,5,58], 'measures' : []},
        {'code' : 65, 'cpp' : 'ASS_VAR_REF_DOUBLE', 'dependencies' : [0,6,58], 'measures' : []},
        {'code' : 66, 'cpp' : 'ASS_VAR_REF_ADDR', 'dependencies' : [0,58], 'measures' : []},
        {'code' : 67, 'cpp' : 'CAST_INT', 'dependencies' : [0,6], 'measures' : []},                # casts FROM different types => different instructions
        {'code' : 68, 'cpp' : 'CAST_CHAR', 'dependencies' : [0,4], 'measures' : []},
        {'code' : 69, 'cpp' : 'CAST_FLOAT', 'dependencies' : [0,6], 'measures' : []},
        {'code' : 70, 'cpp' : 'CAST_DOUBLE', 'dependencies' : [0,5], 'measures' : []},
        {'code' : 71, 'cpp' : 'CAST_ADDR', 'dependencies' : [0,57], 'measures' : []}

]
#{'code' : i, 'cpp' : 'SOME_MACRO', 'dependencies' : [j,f,...], 'measures' : [..., ...], 'consumption' : J}

model_func_dict = [
        {'cpp' : 'FUN_scanf', 'name' : 'scanf', 'include' : ['stdio.h'], 'argsTypes' : ['char *', 'int *'], 'argsValues' : ['%d', ''], 'return' : 'int', 'input' : 'int', 'dependencies' : [0], 'measures' : []},
        {'cpp' : 'FUN_printf', 'name' : 'printf', 'include' : ['stdio.h'], 'argsTypes' : ['char *', 'int *'], 'argsValues' : ['%d', ''], 'return' : 'int', 'input' : '', 'dependencies' : [0], 'measures' : []},
        {'cpp' : 'FUN_free', 'name' : 'free', 'include' : ['stdlib.h'], 'argsTypes' : ['int'], 'argsValues' : [''], 'return' : '', 'input' : '', 'dependencies' : [0], 'measures' : []},
        {'cpp' : 'FUN_malloc', 'name' : '(int *)malloc', 'include' : ['stdlib.h'], 'argsTypes' : ['int'], 'argsValues' : ['sizeof(int)'], 'return' : 'int *', 'input' : '', 'dependencies' : [0], 'measures' : []},
        {'cpp' : 'FUN_exit', 'name' : 'exit', 'include' : ['stdlib.h'], 'argsTypes' : ['int'], 'argsValues' : ['0'], 'return' : '', 'input' : '', 'dependencies' : [0], 'measures' : []},
        {'cpp' : 'FUN_strlen', 'name' : 'strlen', 'include' : ['string.h'], 'argsTypes' : ['char *'], 'argsValues' : [''], 'return' : 'int', 'input' : '', 'dependencies' : [0], 'measures' : []},
        {'cpp' : 'FUN_strcpy', 'name' : 'strcpy', 'include' : ['string.h'], 'argsTypes' : ['char *', 'char *'], 'argsValues' : [''], 'return' : '', 'input' : '', 'dependencies' : [0], 'measures' : []},
        {'cpp' : 'FUN_strcat', 'name' : 'strcat', 'include' : ['string.h'], 'argsTypes' : ['char *', 'char *'], 'argsValues' : [''], 'return' : '', 'input' : '', 'dependencies' : [0], 'measures' : []},
        {'cpp' : 'FUN_sprintf', 'name' : 'sprintf', 'include' : ['stdio.h'], 'argsTypes' : ['char *', 'char *', 'char *'], 'argsValues' : ['_______________', 'data/%s.dat', 'imleft'], 'return' : '', 'input' : '', 'dependencies' : [0], 'measures' : []},
        {'cpp' : 'FUN_fprintf', 'name' : 'fprintf', 'include' : ['stdio.h'], 'argsTypes' : ['FILE *', 'char *', 'char *'], 'argsValues' : ['stdout', 'Could not open file %s', 'filename'], 'return' : '', 'input' : '', 'dependencies' : [0], 'measures' : []},
        {'cpp' : 'FUN_fopen', 'name' : 'fopen', 'include' : ['stdio.h'], 'argsTypes' : ['char *', 'char *'], 'argsValues' : ['test.txt', 'r'], 'return' : 'FILE *', 'input' : '', 'dependencies' : [0], 'measures' : []},
        {'cpp' : 'FUN_fclose', 'name' : 'FILE *fp=fopen(\"test.txt\"); fclose', 'include' : ['stdio.h'], 'argsTypes' : ['FILE *'], 'argsValues' : ['fp'], 'return' : '', 'input' : '', 'dependencies' : [0], 'measures' : []},
        {'cpp' : 'FUN_fread', 'name' : 'FILE *fp=fopen(\"test.txt\"); fread', 'include' : ['stdio.h'], 'argsTypes' : ['char *', 'size_t', 'size_t', 'FILE *'], 'argsValues' : ['0000 0000 00c0 5440 0000 0000 00c0 5440', '8', '8', 'fp'], 'return' : 'int', 'input' : '', 'dependencies' : [0], 'measures' : []},
        {'cpp' : 'FUN_time', 'name' : 'time', 'include' : ['stdlib.h'], 'argsTypes' : ['time_t *'], 'argsValues' : ['NULL'], 'return' : 'time_t', 'input' : '', 'dependencies' : [0], 'measures' : []},
        {'cpp' : 'FUN_srand', 'name' : 'srand', 'include' : ['stdlib.h'], 'argsTypes' : ['unsigned int'], 'argsValues' : ['10'], 'return' : '', 'input' : '', 'dependencies' : [0], 'measures' : []},
        {'cpp' : 'FUN_rand', 'name' : 'srand(10); rand', 'include' : ['stdlib.h'], 'argsTypes' : [''], 'argsValues' : [''], 'return' : 'int', 'input' : '', 'dependencies' : [0], 'measures' : []},
        {'cpp' : 'FUN_atoi', 'name' : 'atoi', 'include' : ['math.h'], 'argsTypes' : ['char *'], 'argsValues' : ['2000'], 'return' : 'int', 'input' : '', 'dependencies' : [0], 'measures' : []},
        {'cpp' : 'FUN_floor', 'name' : 'floor', 'include' : ['math.h'], 'argsTypes' : ['double'], 'argsValues' : [''], 'return' : 'double', 'input' : '', 'dependencies' : [0], 'measures' : []},
        {'cpp' : 'FUN_fmod', 'name' : 'fmod', 'include' : ['math.h'], 'argsTypes' : ['double', 'double'], 'argsValues' : [''], 'return' : 'double', 'input' : '', 'dependencies' : [0], 'measures' : []}
]

model_func_dict0 = [
        {'cpp' : 'FUN_scanf_int', 'name' : 'scanf', 'input' : 'int', 'include' : ['stdio.h'], 'argsTypes' : ['char *', 'int *'], 'argsValues' : ['%d', ''], 'return' : 'int', 'dependencies' : [], 'measures' : []},
        {'cpp' : 'FUN_printf_int', 'name' : 'printf', 'input' : '', 'include' : ['stdio.h'], 'argsTypes' : ['char *', 'int *'], 'argsValues' : ['%d', ''], 'return' : 'int', 'dependencies' : [], 'measures' : []},
        {'cpp' : 'FUN_scanf_float', 'name' : 'scanf', 'input' : 'float', 'include' : ['stdio.h'], 'argsTypes' : ['char *', 'int *'], 'argsValues' : ['%f', ''], 'return' : 'int', 'dependencies' : [], 'measures' : []},
        {'cpp' : 'FUN_printf_float', 'name' : 'printf', 'input' : '', 'include' : ['stdio.h'], 'argsTypes' : ['char *', 'int *'], 'argsValues' : ['%f', ''], 'return' : 'int', 'dependencies' : [], 'measures' : []},
        {'cpp' : 'FUN_scanf_double', 'name' : 'scanf', 'input' : 'double', 'include' : ['stdio.h'], 'argsTypes' : ['char *', 'int *'], 'argsValues' : ['%lf.', ''], 'return' : 'int', 'dependencies' : [], 'measures' : []},
        {'cpp' : 'FUN_printf_double', 'name' : 'printf', 'input' : '', 'include' : ['stdio.h'], 'argsTypes' : ['char *', 'int *'], 'argsValues' : ['%.16f.', ''], 'return' : 'int', 'dependencies' : [], 'measures' : []},
        {'cpp' : 'FUN_scanf_char', 'name' : 'scanf', 'input' : 'char', 'include' : ['stdio.h'], 'argsTypes' : ['char *', 'int *'], 'argsValues' : ['%c', ''], 'return' : 'int', 'dependencies' : [], 'measures' : []},
        {'cpp' : 'FUN_printf_char', 'name' : 'printf', 'input' : '', 'include' : ['stdio.h'], 'argsTypes' : ['char *', 'int *'], 'argsValues' : ['%c', ''], 'return' : 'int', 'dependencies' : [], 'measures' : []},
        {'cpp' : 'FUN_scanf_int_2', 'name' : 'scanf', 'input' : '2int', 'include' : ['stdio.h'], 'argsTypes' : ['char *', 'int *', 'int *'], 'argsValues' : ['%d,%d', '', ''], 'return' : 'int', 'dependencies' : [], 'measures' : []},
        {'cpp' : 'FUN_printf_int_2', 'name' : 'printf', 'input' : '', 'include' : ['stdio.h'], 'argsTypes' : ['char *', 'int *', 'int *'], 'argsValues' : ['%d,%d', '', ''], 'return' : 'int', 'dependencies' : [], 'measures' : []},
        {'cpp' : 'FUN_scanf_int_3', 'name' : 'scanf', 'input' : '3int', 'include' : ['stdio.h'], 'argsTypes' : ['char *', 'int *', 'int *', 'int *'], 'argsValues' : ['%d,%d,%d', '', '', ''], 'return' : 'int', 'dependencies' : [], 'measures' : []},
        {'cpp' : 'FUN_printf_int_3', 'name' : 'printf', 'input' : '', 'include' : ['stdio.h'], 'argsTypes' : ['char *', 'int *', 'int *', 'int *'], 'argsValues' : ['%d,%d,%d', '', '', ''], 'return' : 'int', 'dependencies' : [], 'measures' : []},
        {'cpp' : 'FUN_scanf_int_4', 'name' : 'scanf', 'input' : '4int', 'include' : ['stdio.h'], 'argsTypes' : ['char *', 'int *', 'int *', 'int *', 'int *'], 'argsValues' : ['%d,%d,%d,%d', '', '', '', ''], 'return' : 'int', 'dependencies' : [], 'measures' : []},
        {'cpp' : 'FUN_printf_int_4', 'name' : 'printf', 'input' : '', 'include' : ['stdio.h'], 'argsTypes' : ['char *', 'int *', 'int *', 'int *', 'int *'], 'argsValues' : ['%d,%d,%d,%d', '', '', '', ''], 'return' : 'int', 'dependencies' : [], 'measures' : []},
        {'cpp' : 'FUN_scanf_int_5', 'name' : 'scanf', 'input' : '5int', 'include' : ['stdio.h'], 'argsTypes' : ['char *', 'int *', 'int *', 'int *', 'int *', 'int *'], 'argsValues' : ['%d,%d,%d,%d,%d', '', '', '', '', ''], 'return' : 'int', 'dependencies' : [], 'measures' : []},
        {'cpp' : 'FUN_printf_int_5', 'name' : 'printf', 'input' : '', 'include' : ['stdio.h'], 'argsTypes' : ['char *', 'int *', 'int *', 'int *', 'int *', 'int *'], 'argsValues' : ['%d,%d,%d,%d,%d', '', '', '', '', ''], 'return' : 'int', 'dependencies' : [], 'measures' : []},
        {'cpp' : 'FUN_scanf_int_6', 'name' : 'scanf', 'input' : '6int', 'include' : ['stdio.h'], 'argsTypes' : ['char *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *'], 'argsValues' : ['%d,%d,%d,%d,%d,%d', '', '', '', '', '', ''], 'return' : 'int', 'dependencies' : [], 'measures' : []},
        {'cpp' : 'FUN_printf_int_6', 'name' : 'printf', 'input' : '', 'include' : ['stdio.h'], 'argsTypes' : ['char *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *'], 'argsValues' : ['%d,%d,%d,%d,%d,%d', '', '', '', '', '', ''], 'return' : 'int', 'dependencies' : [], 'measures' : []},
        {'cpp' : 'FUN_scanf_int_7', 'name' : 'scanf', 'input' : '7int', 'include' : ['stdio.h'], 'argsTypes' : ['char *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *'], 'argsValues' : ['%d,%d,%d,%d,%d,%d,%d', '', '', '', '', '', '', ''], 'return' : 'int', 'dependencies' : [], 'measures' : []},
        {'cpp' : 'FUN_printf_int_7', 'name' : 'printf', 'input' : '', 'include' : ['stdio.h'], 'argsTypes' : ['char *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *'], 'argsValues' : ['%d,%d,%d,%d,%d,%d,%d', '', '', '', '', '', '', ''], 'return' : 'int', 'dependencies' : [], 'measures' : []},
        {'cpp' : 'FUN_scanf_int_8', 'name' : 'scanf', 'input' : '8int', 'include' : ['stdio.h'], 'argsTypes' : ['char *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *'], 'argsValues' : ['%d,%d,%d,%d,%d,%d,%d,%d', '', '', '', '', '', '', '', ''], 'return' : 'int', 'dependencies' : [], 'measures' : []},
        {'cpp' : 'FUN_printf_int_8', 'name' : 'printf', 'input' : '', 'include' : ['stdio.h'], 'argsTypes' : ['char *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *'], 'argsValues' : ['%d,%d,%d,%d,%d,%d,%d,%d', '', '', '', '', '', '', '', ''], 'return' : 'int', 'dependencies' : [], 'measures' : []},
        {'cpp' : 'FUN_scanf_int_9', 'name' : 'scanf', 'input' : '9int', 'include' : ['stdio.h'], 'argsTypes' : ['char *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *'], 'argsValues' : ['%d,%d,%d,%d,%d,%d,%d,%d,%d', '', '', '', '', '', '', '', '', ''], 'return' : 'int', 'dependencies' : [], 'measures' : []},
        {'cpp' : 'FUN_printf_int_9', 'name' : 'printf', 'input' : '', 'include' : ['stdio.h'], 'argsTypes' : ['char *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *'], 'argsValues' : ['%d,%d,%d,%d,%d,%d,%d,%d,%d', '', '', '', '', '', '', '', '', ''], 'return' : 'int', 'dependencies' : [], 'measures' : []},
        {'cpp' : 'FUN_scanf_int_10', 'name' : 'scanf', 'input' : '10int', 'include' : ['stdio.h'], 'argsTypes' : ['char *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *'], 'argsValues' : ['%d,%d,%d,%d,%d,%d,%d,%d,%d,%d', '', '', '', '', '', '', '', '', '', ''], 'return' : 'int', 'dependencies' : [], 'measures' : []},
        {'cpp' : 'FUN_printf_int_10', 'name' : 'printf', 'input' : '', 'include' : ['stdio.h'], 'argsTypes' : ['char *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *'], 'argsValues' : ['%d,%d,%d,%d,%d,%d,%d,%d,%d,%d', '', '', '', '', '', '', '', '', '', ''], 'return' : 'int', 'dependencies' : [], 'measures' : []},
        {'cpp' : 'FUN_scanf_int_20', 'name' : 'scanf', 'input' : '20int', 'include' : ['stdio.h'], 'argsTypes' : ['char *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *'], 'argsValues' : ['%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''], 'return' : 'int', 'dependencies' : [], 'measures' : []},
        {'cpp' : 'FUN_printf_int_20', 'name' : 'printf', 'input' : '', 'include' : ['stdio.h'], 'argsTypes' : ['char *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *'], 'argsValues' : ['%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''], 'return' : 'int', 'dependencies' : [], 'measures' : []},
        {'cpp' : 'FUN_scanf_int_30', 'name' : 'scanf', 'input' : '30int', 'include' : ['stdio.h'], 'argsTypes' : ['char *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *'], 'argsValues' : ['%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''], 'return' : 'int', 'dependencies' : [], 'measures' : []},
        {'cpp' : 'FUN_printf_int_30', 'name' : 'printf', 'input' : '', 'include' : ['stdio.h'], 'argsTypes' : ['char *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *'], 'argsValues' : ['%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''], 'return' : 'int', 'dependencies' : [], 'measures' : []},
        {'cpp' : 'FUN_scanf_int_40', 'name' : 'scanf', 'input' : '40int', 'include' : ['stdio.h'], 'argsTypes' : ['char *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *'], 'argsValues' : ['%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''], 'return' : 'int', 'dependencies' : [], 'measures' : []},
        {'cpp' : 'FUN_printf_int_40', 'name' : 'printf', 'input' : '', 'include' : ['stdio.h'], 'argsTypes' : ['char *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *'], 'argsValues' : ['%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''], 'return' : 'int', 'dependencies' : [], 'measures' : []},
        {'cpp' : 'FUN_scanf_int_50', 'name' : 'scanf', 'input' : '50int', 'include' : ['stdio.h'], 'argsTypes' : ['char *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *'], 'argsValues' : ['%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''], 'return' : 'int', 'dependencies' : [], 'measures' : []},
        {'cpp' : 'FUN_printf_int_50', 'name' : 'printf', 'input' : '', 'include' : ['stdio.h'], 'argsTypes' : ['char *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *', 'int *'], 'argsValues' : ['%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''], 'return' : 'int', 'dependencies' : [], 'measures' : []},
        {'cpp' : 'FUN_malloc_int', 'name' : 'malloc', 'input' : '', 'include' : ['stdlib.h'], 'argsTypes' : ['int'], 'argsValues' : ['4'], 'return' : 'int*', 'dependencies' : [], 'measures' : []},
        {'cpp' : 'FUN_malloc_double', 'name' : 'malloc', 'input' : '', 'include' : ['stdlib.h'], 'argsTypes' : ['int'], 'argsValues' : ['8'], 'return' : 'double*', 'dependencies' : [], 'measures' : []},
        {'cpp' : 'FUN_malloc_short', 'name' : 'malloc', 'input' : '', 'include' : ['stdlib.h'], 'argsTypes' : ['int'], 'argsValues' : ['2'], 'return' : 'int*', 'dependencies' : [], 'measures' : []},
        {'cpp' : 'FUN_malloc_char', 'name' : 'malloc', 'input' : '', 'include' : ['stdlib.h'], 'argsTypes' : ['int'], 'argsValues' : ['1'], 'return' : 'char*', 'dependencies' : [], 'measures' : []},
        {'cpp' : 'FUN_malloc_stringN5', 'name' : 'malloc', 'input' : '', 'include' : ['stdlib.h'], 'argsTypes' : ['int'], 'argsValues' : ['5'], 'return' : 'char*', 'dependencies' : [], 'measures' : []},
        {'cpp' : 'FUN_malloc_stringN10', 'name' : 'malloc', 'input' : '', 'include' : ['stdlib.h'], 'argsTypes' : ['int'], 'argsValues' : ['10'], 'return' : 'char*', 'dependencies' : [], 'measures' : []},
        {'cpp' : 'FUN_malloc_stringN15', 'name' : 'malloc', 'input' : '', 'include' : ['stdlib.h'], 'argsTypes' : ['int'], 'argsValues' : ['15'], 'return' : 'char*', 'dependencies' : [], 'measures' : []},
        {'cpp' : 'FUN_malloc_stringN20', 'name' : 'malloc', 'input' : '', 'include' : ['stdlib.h'], 'argsTypes' : ['int'], 'argsValues' : ['20'], 'return' : 'char*', 'dependencies' : [], 'measures' : []},
        {'cpp' : 'FUN_malloc_stringN30', 'name' : 'malloc', 'input' : '', 'include' : ['stdlib.h'], 'argsTypes' : ['int'], 'argsValues' : ['30'], 'return' : 'char*', 'dependencies' : [], 'measures' : []}
]
#{'cpp' : 'CPP-CODE', 'include' : ['list', 'of', 'include', 'needed'], 'argsTypes' : ['list', 'of', 'argsTypes'], ...}

#Model Energy of Instructions
def execute():
  for elem in model_dict:
    cpp = elem['cpp']
    call(['make', 'clean'])
    call(['make', 'DEFS=' + cpp])
    call(['sudo', 'make', 'run', 'OUTFILE=' + OUTFILE, 'NTIMES=' + str(NTIMES), 'MTIMES=' + str(MTIMES)])
    call(['sleep', str(SLEEPTIME)])
    #catch the result, save it in 'measures'
    cons = parseMeasures()
    elem['measures'] = cons

def parseMeasures():
  data = open(OUTFILE + '.J')
  dat = data.read()
  lst = dat.splitlines()
  cpu = []
  for elem in lst:
    values = elem.split(';')
    if len(values) == 5:
      cpu.append(Decimal(values[1]))
    else:
      cpu.append(Decimal(0))
  return cpu

def deriveConsumption():
  consumption = 0
  for elem in model_dict:
    lst = elem['measures']
    lst = list(map(lambda x: x/Decimal(NTIMES),lst))
    lst.sort()
    # remove outilers
    if OUTLIERS > 0:
      if OUT_REM_POLICY == 'MIN':
        del lst[:OUTLIERS]   #remove first OUTLIERS from list
      elif OUT_REM_POLICY == 'MAX':
        del lst[-OUTLIERS:]  #remove last OUTILERS from list
      else:
        del lst[:OUTLIERS]   #remove first OUTLIERS from list
        del lst[-OUTLIERS:]  #remove last OUTILERS from list
    #consumption = sum(lst)/Decimal(len(lst))
    consumption = max(lst)
    elem['consumption'] = consumption
  for elem in model_dict:
    depends = elem['dependencies']
    for d in depends:
      dep = model_dict[d]
      elem['consumption'] -= dep['consumption']

def deriveConsumptionB():
  consumption = 0
  for elem in model_dict:
    lst = elem['measures']
    lst.sort()
    # remove outilers
    if OUTLIERS > 0:
      if OUT_REM_POLICY == 'MIN':
        del lst[:OUTLIERS]   #remove first OUTLIERS from list
      elif OUT_REM_POLICY == 'MAX':
        del lst[-OUTLIERS:]  #remove last OUTILERS from list
      else:
        del lst[:OUTLIERS]   #remove first OUTLIERS from list
        del lst[-OUTLIERS:]  #remove last OUTILERS from list
    lst = list(filter(lambda x: x > 0, lst))    #avoid null measurement
    elem['avg'] = sum(lst)/Decimal(max(1,len(lst)))
    lst = list(map(lambda x: x/Decimal(NTIMES),lst))
    depends = elem['dependencies']
    for d in depends:
      dep = model_dict[d]
      dc = dep['consumption']
      #lst = list(map(lambda x: x-(Decimal(0.8)*dc), lst))
      lst = list(map(lambda x: x-dc, lst))
    elem['measures'] = lst
    lst = list(filter(lambda x: x > 0, lst))
    if len(lst) > 1:
      consumption = sum(lst)/Decimal(len(lst))
    else:
      consumption = Decimal(-1)
    elem['consumption'] = consumption

def deriveConsumptionC():
  for elem in model_dict:
    ##lst = elem['measures']
    lst = elem['measures']  #-
    ##lst = list(map(lambda x: x/Decimal(NTIMES),lst))
    lst.sort()
    # remove outilers
    if OUTLIERS > 0:
      if OUT_REM_POLICY == 'MIN':
        del lst[:OUTLIERS]   #remove first OUTLIERS from list
      elif OUT_REM_POLICY == 'MAX':
        del lst[-OUTLIERS:]  #remove last OUTILERS from list
      else:
        del lst[:OUTLIERS]   #remove first OUTLIERS from list
        del lst[-OUTLIERS:]  #remove last OUTILERS from list
    depends = elem['dependencies']
    for d in depends:
      dep = model_dict[d]
      ##dc = dep['consumption']
      dc = dep['avg']  #-
      #lst = list(map(lambda x: x-(Decimal(0.8)*dc), lst))
      lst = list(map(lambda x: x-dc, lst))
    lst = list(filter(lambda x: x > 0, lst))
    if len(lst) > 1:
      elem['avg'] = sum(lst)/Decimal(len(lst))
    else:
      elem['avg'] = Decimal(0)
    ##elem['consumption'] = consumption
    elem['consumption'] = elem['avg']/Decimal(NTIMES)  #-


#Model energy of Functions
def genFunctionCode(elem):
  name = elem['name']
  includes = elem['include']
  argsTypes = elem['argsTypes']
  argsValues = elem['argsValues']
  ret = elem['return']
  cpp = elem['cpp']

  inc = ''
  decs = ''
  for i in includes:
    inc+='#include <' + i + '>\n'
  allVars = []
  cont = 0
  for j in argsTypes:
    var = genVarDec(elem, j, argsValues, cont)
    if var == '':
      allVars.append(argsValues[cont])
    else:
      var += ';'
      decs += var + '\n'
      allVars.append('a' + str(cont))
    cont = cont + 1
  if (ret == 'void') | (ret == ''):
    r = name + '('
  else:
    r = 'res = ' + name + '('
    decs += ret + ' res;'
  for v in allVars:
    r += v + ','
  r = r[:-1]
  r+=');\n'
  if (cpp == 'FUN_fopen') | (cpp == 'FUN_fread'):
    r+='fclose(fp);' + '\n'

  return inc, (decs + '\n' + r)

def genVarDec(elem, tp, values, cont):
  var = ''
  val = ''
  if len(values):
    if len(values[cont]):
      val = str(values[cont])

  if tp == 'int':
    var = 'int a' + str(cont) + ' = ' + str(randIntString(val))
    elem['dependencies'].append(4)
  elif tp == 'char':
    var = 'char a' + str(cont) + ' = \'' + str(randCharString(val)) + '\''
    elem['dependencies'].append(4)    #DUMMY...
  elif tp == 'float':
    var = 'float a' + str(cont) + ' = ' + str(randFloatString(val))
    elem['dependencies'].append(5)
  elif tp == 'double':
    var = 'double a' + str(cont) + ' = ' + str(randDoubleString(val))
    elem['dependencies'].append(6)
  elif tp == 'char *':
    s = randString(val)
    var = 'char a' + str(cont) + '[' + str(len(s)) + '] = ' + '\"' + s + '\"'
    elem['dependencies'].append(4)    #DUMMY...
  elif tp == 'int *':
    length = randIntString('',1,20)
    s = '{'
    for x in range(0,int(length)):
      s += randIntString(val) + ','
      elem['dependencies'].append(4)
    s = s[:-1]
    s += '}'
    var = 'int a' + str(cont) + '[' + length + '] = ' + s
  elif tp == 'float *':
    length = randIntString('',1,20)
    s = '{'
    for x in range(0,int(length)):
      s += randFloatString(val) + ','
      elem['dependencies'].append(5)
    s = s[:-1]
    s += '}'
    var = 'float a' + str(cont) + '[' + length + '] = ' + str(s)
  elif tp == 'double *':
    length = randFloatString('',1,20)
    s = '{'
    for x in range(0,int(length)):
      s += randDoubleString(val) + ','
      elem['dependencies'].append(6)
    s = s[:-1]
    s += '}'
    var = 'double a' + str(cont) + '[' + length + '] = ' + str(s)
  else:
    pass
    #we'll consider that the only type left is the file pointer (FILE *), and it is ALLWAYS given in the argValues list
  return var

def saveFuncFile(inc, code):
  content = '#include \"funCall.h\"\n' + inc + '\n\n' + 'int funCall(){\n' + code + '\nreturn 0;\n}\n'
  #write to file
  f = open(FUNCFILE,'w+')
  f.write(content)
  f.close()

def executeFunctions():
  cpp = 'FUN'
  for e in model_func_dict:
    inp = e['input']
    if inp:
      inp += 'Vals'
    for x in range(0, MTIMES):
      print("Execution '" + str(x) + "' of " + e['cpp'])
      inc, code = genFunctionCode(e)
      saveFuncFile(inc, code)
      call(['make', 'clean'])
      call(['make', 'DEFS=' + cpp])
      call(['sudo', 'make', 'run', 'OUTFILE=' + OUTFILE, 'NTIMES=' + str(NTIMES), 'MTIMES=1', 'INPUTS=' + str(inp)])
      call(['sleep', str(SLEEPTIME)])
      #catch the result, save it in 'measures'
      cons = parseMeasures()[0]/Decimal(NTIMES)
      cons = subclearDependencies(cons, e)
      e['measures'].append(cons)

def subclearDependencies(value, elem):
  depends = elem['dependencies']
  for d in depends:
    dep = model_dict[d]
    value -= dep['consumption']
  #clear dependencies
  elem['dependencies'] = [0]
  return value

def deriveConsumptionFunctions():
  consumption = 0
  for elem in model_func_dict:
    lst = elem['measures']
    #lst = list(map(lambda x: x/Decimal(NTIMES),lst))
    #lst.sort()
    ## remove outilers
    #del lst[:OUTLIERS]
    #del lst[-OUTLIERS:]
    avg = sum(lst)/Decimal(len(lst))
    maxC = max(lst)
    elem['consumption'] = maxC
    elem['avg'] = avg

def writeModel():
  with open(MODELFILE, 'wb') as csv_file:
    writer = csv.writer(csv_file)
    for elem in model_dict:
      cpp = elem['cpp']
      cons = elem['consumption']
      writer.writerow([cpp, cons])
    for f in model_func_dict:
      cpp = f['cpp']
      cons = f['consumption']
      writer.writerow([cpp, cons])

def readModel():
  with open(MODELFILE, 'rb') as csv_file:
    reader = csv.reader(csv_file)
    model_dict = dict(reader)

def saveJSON():
  for e in model_dict:
    e['consumption'] = str(e['consumption'])
    e['avg'] = str(e['avg'])  #-
    for i in range(len(e['measures'])):
      e['measures'][i] = str(e['measures'][i])

  for f in model_func_dict:
    f['consumption'] = str(f['consumption'])
    f['avg'] = str(f['avg'])  #-
    for j in range(len(f['measures'])):
      f['measures'][j] = str(f['measures'][j])

  with open(JSON_INST, 'w+') as fp1:
    json.dump(model_dict, fp1, indent=4, sort_keys=True)

  with open(JSON_FUNC, 'w+') as fp2:
    json.dump(model_func_dict, fp2, indent=4, sort_keys=True)

def loadJSON():
  with open(JSON_INST, 'r') as fp1:
    model_dict = json.load(fp1)
  with open(JSON_FUNC, 'r') as fp2:
    model_func_dict = json.load(fp2)

  for e in model_dict:
    e['consumption'] = Decimal(e['consumption'])
    for i in range(len(e['measures'])):
      e['measures'][i] = Decimal(e['measures'][i])

  for f in model_func_dict:
    f['consumption'] = Decimal(f['consumption'])
    for j in range(len(f['measures'])):
      f['measures'][j] = Decimal(f['measures'][j])

def main():
  #testF()
  #print("Modeling Single Instructions")
  execute()
  deriveConsumptionB()  #deriveConsumption()

  #print("Modeling Functions")
  executeFunctions()
  deriveConsumptionFunctions()

  #print("Writing Model")
  writeModel()

  #print("...|Saving Dictionaries for Debug|...")
  saveJSON()
  
if __name__ == '__main__':
  if len(sys.argv) > 1:
    OUTLIERS = int(sys.argv[1])
    if len(sys.argv) > 2:
      OUT_REM_POLICY = sys.argv[2]
      if len(sys.argv) > 3:
        NTIMES = int(sys.argv[3])
  main()
