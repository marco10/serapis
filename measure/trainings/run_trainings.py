#!/usr/bin/python3

import json, os, time

import pandas as pd
import numpy as np

from collections import OrderedDict
from subprocess import Popen, PIPE, TimeoutExpired
from shutil import *

trainings_file = "trainings.json"
sleep_time     = 30   # 1/2 minute
measure_times  = 100  # number of measurements to be obtained for each training
repeat_times   = 2000 # number of times a training is repeated in each measurement


def collect_data(reference):
	out_file = "energy_" + reference + ".J"
	df = pd.read_csv(out_file, sep=";")

	consumption = df["Core(s)"].max()

	return (consumption/repeat_times)

def run_experiment(reference):
	out_file = "energy_" + reference
	if os.path.isfile(out_file+".J"):
		return
	
	defs     = reference.upper()
	run_command("sleep " + str(sleep_time))
	r, o, e = run_command("make clean")
	print("\t\tcleaned:", "[", r, "]")
	r, o, e = run_command("make DEFS=" + defs)
	print("\t\tbuilt  :", "[", r, "]")
	r, o, e = run_command("make run OUTFILE=" + out_file + " NTIMES=" + str(repeat_times) + " MTIMES=" + str(measure_times))
	print("\t\ttested :", "[", r, "]")
	run_command("sleep " + str(sleep_time))

def run_command(command, dir=None, timeout=None):
	pipes = Popen(command, shell=True, stdout=PIPE, stderr=PIPE, cwd=dir)
	try:
		std_out, std_err = pipes.communicate(timeout=timeout)
	except TimeoutExpired as e:
		return -9, "", "Command '"+ command +"' timed out after "+ str(timeout) +" seconds"
	else:
		return pipes.returncode, std_out.decode("UTF-8", errors="ignore"), std_err.decode("UTF-8", errors="ignore")

def main():
	data = OrderedDict()
	with open(trainings_file, "r") as fp:
		data = json.load(fp, object_pairs_hook=OrderedDict)

	start_time = time.time()
	for i, item in enumerate(data["trainings"]):
		ref = item["reference"]
		print("\t[" + str(i) + "]", "Now running:", ref)
		run_experiment(ref)
		energy = collect_data(ref)
		item["observed"] = energy

	elapsed_time = time.time() - start_time
	print("\n\nElapsed Time:", elapsed_time)
	with open(trainings_file, "w+") as fp:
		json.dump(data, fp, indent=4)

if __name__ == '__main__':
	main()
