#include <stddef.h>
#include <stdint.h>

#define DIGITS_ARRAY_SIZE 64

#define LENGTH(A) (sizeof(A)/sizeof(A[0]))

size_t rebase(int8_t digits[DIGITS_ARRAY_SIZE], int16_t from_base, int16_t to_base, size_t num_digits);
