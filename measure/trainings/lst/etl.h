#include <stddef.h>

typedef struct {
   int value;
   const char *keys;
} legacy_map;

typedef struct {
   char key;
   int value;
} new_map;

int etl_convert(const legacy_map * input, const size_t input_len, new_map ** output);
