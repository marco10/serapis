#include <stddef.h>

#define ARRAY_SIZE(arr) (sizeof(arr)/sizeof(arr[0]))

char **annotate(const char **minefield, const size_t rows);
void free_annotation(char **annotation);

