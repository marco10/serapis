#include "run_length_decoding.h"
#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>
#include <math.h>

const int D_READ = 0;
const int D_WRITE = 1;

/* count_digits: return the count of digits in a number */
static size_t count_digits(size_t num)
{
   return log10(num) + 1;
}

/**
 * enc_helper: calculate the encoded size of 'text';
 *             encode in 'encoded' if write mode is set
 */
static size_t enc_helper(char *encoded, const char *text,
                         const int mode)
{
   size_t enc_len = 0;
   while (*text) {
      /* smallest count means a single char */
      size_t count = 1;
      while (*text == *(text + 1)) {
         count++;
         text++;
      }
      if (count > 1) {
         const size_t cdigits = count_digits(count);
         enc_len += cdigits;
         if (mode == D_WRITE) {
            sprintf(encoded, "%ld", count);
            encoded += cdigits;
         }
      }
      if (mode == D_WRITE)
         *encoded++ = *text;

      enc_len++;
      text++;
   }

   return enc_len;
}

/**
 * dec_helper: calculate the decoded size of 'data';
 *             decode in 'decoded' if write mode is set
 */
static size_t dec_helper(char *decoded, const char *data,
                         const int mode)
{
   size_t dec_len = 0;
   char check_data = *data;
   while (check_data) {
      while (*data && !isdigit(*data)) {
         dec_len++;
         if (mode == D_WRITE)
            *decoded++ = *data;
         data++;
      }
      size_t count = 0;
      int check = isdigit(*data);
      while (check!=0) {
         count = count * 10 + *data - '0';
         data++;
         check = isdigit(*data);
      }
      for (size_t i = 0; i < count; i++) {
         dec_len++;
         if (mode == D_WRITE)
            *decoded++ = *data;
      }
      if (*data != '\0')
         data++;

      check_data = *data;
   }

   return dec_len;
}


char *run_length_decode(const char *data)
{
   if (data == NULL)
      return NULL;
   if (*data == '\0')
      return calloc(1, 1);

   /* get length */
   const size_t dec_len = dec_helper(NULL, data, D_READ);

   char *decoded = malloc(dec_len + 1);
   if (decoded == NULL) {
      fprintf(stderr, "Memory error!\n");
      return NULL;
   }

   /* decode */
   dec_helper(decoded, data, D_WRITE);
   decoded[dec_len] = '\0';

   return decoded;
}
