#!/usr/bin/env Rscript

library(distr)

args = commandArgs(trailingOnly=TRUE)

if (length(args) >= 1) {
	tag <- args[1]

	action <- ""
	if (length(args) == 2) {
		action <- args[2]
	}

	last.conv <- paste("convolutions/", tag, "_last_conv.rds", sep="")
	setX <- paste("convolutions/", tag, "_setX.csv", sep="")
	setY <- paste("convolutions/", tag, "_setY.csv", sep="")
	if (action == "") {
	
		if (file.exists(last.conv)) {
			X <- readRDS(last.conv)
		} else {
			x <- read.csv(setX, header = F)$V1
			X <- Norm(mean=mean(x), sd=sd(x))
		}
	
		y = read.csv(setY, header = F)$V1
		Y <- Norm(mean=mean(y), sd=sd(y))
	
		conv <- convpow(X+Y,1)  # object of class AbscontDistribution
	}else{
		rdsfiles <- list.files("convolutions", "*.rds")
		c <- 0
		for (i in rdsfiles) {
			file <- paste("convolutions/", i, sep="")
			if (c == 0){
				X <- readRDS(file)
			}else{
				Y <- readRDS(file)
				X <- convpow(X+Y,1)
			}
		}

		conv <- X
	}

	saveRDS(conv, last.conv)

}else{
	print("Wrong number of arguments")
	exit(-1)
}
