//#include <stdio.h>
//#include <math.h>
//#include "lib.h"
#include "instr.h"
//#include <math.h>


  int void_call(){
  	return 0;
  }


//declarations
  int dec_int_init(){
  	int a=1;
  	return 0;
  }

  int dec_double_init(){
  	double a=1.0;
  	return 0;
  }

  int dec_float_init(){
  	float a=1.0;
  	return 0;
  }

  int dec_array(){
    int arr[20];
    return 0;
  }

  int dec_struct(){
    St s;
    return 0;
  }

//simple assignments
  int assign_var_int(){
  	int a;
  	a=10;
  	return 0;
  }

  int assign_var_char(){
    char a;
    a='a';
    return 0;
  }

  int assign_var_float(){
  	float a;
  	a=10;
  	return 0;
  }

  int assign_var_double(){
  	double a;
  	a=10;
  	return 0;
  }

  int assign_var_var_int(){
  	int a;
  	int b=1;
  	a=b;
  	return 0;
  }

  int assign_var_var_char(){
    char a;
    char b='a';
    a=b;
    return 0;
  }

  int assign_var_var_float(){
  	float a;
  	float b=1;
  	a=b;
  	return 0;
  }

  int assign_var_var_double(){
  	double a;
  	double b=1;
  	a=b;
  	return 0;
  }

  int assign_var_addr(){
    int *p;
    int s;
    p = &s;
    return 0;
  }

//Operations Assignments
  //ADDITION
  int assign_add_int_int(){
  	int a;
  	a=1+2;
  	return 0;
  }

  int assign_add_float_float(){
  	float a;
  	a=1+2;
  	return 0;
  }

  int assign_add_double_double(){
  	double a;
  	a=1+2;
  	return 0;
  }

  int assign_add_var_var_int(){
  	int a;
  	int b=1;
  	int c=2;
  	a=b+c;
  	return 0;
  }

  int assign_add_var_var_float(){
  	float a;
  	float b=1;
  	float c=2;
  	a=b+c;
  	return 0;
  }

  int assign_add_var_var_double(){
  	double a;
  	double b=1;
  	double c=2;
  	a=b+c;
  	return 0;
  }

  int assign_add_var_int(){
  	int a;
  	int b=1;
  	a=b+2;
  	return 0;
  }

  int assign_add_var_float(){
  	float a;
  	float b=1;
  	a=b+2;
  	return 0;
  }

  int assign_add_var_double(){
  	double a;
  	double b=1;
  	a=b+2;
  	return 0;
  }

  //SUBTRACTION
  int assign_sub_int_int(){
  	int a;
  	a=1-2;
  	return 0;
  }

  int assign_sub_float_float(){
  	float a;
  	a=1-2;
  	return 0;
  }

  int assign_sub_double_double(){
  	double a;
  	a=1-2;
  	return 0;
  }

  int assign_sub_var_var_int(){
  	int a;
  	int b=1;
  	int c=2;
  	a=b-c;
  	return 0;
  }

  int assign_sub_var_var_float(){
  	float a;
  	float b=1;
  	float c=2;
  	a=b-c;
  	return 0;
  }

  int assign_sub_var_var_double(){
  	double a;
  	double b=1;
  	double c=2;
  	a=b-c;
  	return 0;
  }

  int assign_sub_var_int(){
  	int a;
  	int b=1;
  	a=b-2;
  	return 0;
  }

  int assign_sub_var_float(){
  	float a;
  	float b=1;
  	a=b-2;
  	return 0;
  }

  int assign_sub_var_double(){
  	double a;
  	double b=1;
  	a=b-2;
  	return 0;
  }

  //MULTIPLICATION
  int assign_mul_int_int(){
  	int a;
  	a=1*2;
  	return 0;
  }

  int assign_mul_float_float(){
  	float a;
  	a=1*2;
  	return 0;
  }

  int assign_mul_double_double(){
  	double a;
  	a=1*2;
  	return 0;
  }

  int assign_mul_var_var_int(){
  	int a;
  	int b=1;
  	int c=2;
  	a=b*c;
  	return 0;
  }

  int assign_mul_var_var_float(){
  	float a;
  	float b=1;
  	float c=2;
  	a=b*c;
  	return 0;
  }

  int assign_mul_var_var_double(){
  	double a;
  	double b=1;
  	double c=2;
  	a=b*c;
  	return 3;
  }

  int assign_mul_var_int(){
  	int a;
  	int b=1;
  	a=b*2;
  	return 0;
  }

  int assign_mul_var_float(){
  	float a;
  	float b=1;
  	a=b*2;
  	return 0;
  }

  int assign_mul_var_double(){
  	double a;
  	double b=1;
  	a=b*2;
  	return 0;
  }

  //DIVISION
  int assign_div_int_int(){
  	int a;
  	a=1/2;
  	return 0;
  }

  int assign_div_float_float(){
  	float a;
  	a=1/2;
  	return 0;
  }

  int assign_div_double_double(){
  	double a;
  	a=1/2;
  	return 0;
  }

  int assign_div_var_var_int(){
  	int a;
  	int b=1;
  	int c=2;
  	a=b/c;
  	return 0;
  }

  int assign_div_var_var_float(){
  	float a;
  	float b=1;
  	float c=2;
  	a=b/c;
  	return 0;
  }

  int assign_div_var_var_double(){
  	double a;
  	double b=1;
  	double c=2;
  	a=b/c;
  	return 3;
  }

  int assign_div_var_int(){
  	int a;
  	int b=1;
  	a=b/2;
  	return 0;
  }

  int assign_div_var_float(){
  	float a;
  	float b=1;
  	a=b/2;
  	return 0;
  }

  int assign_div_var_double(){
  	double a;
  	double b=1;
  	a=b/2;
  	return 0;
  }

  //pointer assignments
  int assign_var_ref_int(){
    int *a;
    int b;
    int c;
    a=&b;
    b=1;
    c=*a;
    return 0;
  }

  int assign_var_ref_char(){
    char *a;
    char b;
    char c;
    a=&b;
    b='a';
    c=*a;
    return 0;
  }

  int assign_var_ref_float(){
    float *a;
    float b;
    float c;
    a=&b;
    b=1;
    c=*a;
    return 0;
  }
      
  int assign_var_ref_double(){
    double *a;
    double b;
    double c;
    a=&b;
    b=1;
    c=*a;
    return 0;
  }
      
  int assign_var_ref_addr(){
    St *a;
    St b;
    a=&b;
    St c=*a;
    return 0;
  }
      
  //CONDITIONS
  int cmp_var_int(){
    int a=0;
    while(a!=0){

    }
    return 0;
  }

  int cmp_var_char(){
    char a='a';
    while(a!='a'){

    }
    return 0;
  }

  int cmp_var_float(){
    float a=0;
    while(a!=0){

    }
    return 0;
  }

  int cmp_var_double(){
    double a=0;
    while(a!=0){

    }
    return 0;
  }

  int cmp_var_var_int(){
    int a=0;
    int b=0;
    while(a!=b){

    }
    return 0;
  }

  int cmp_var_var_char(){
    char a='a';
    char b='a';
    while(a!=b){

    }
    return 0;
  }

  int cmp_var_var_float(){
    float a=0;
    float b=0;
    while(a!=b){

    }
    return 0;
  }

  int cmp_var_var_double(){
    double a=0;
    double b=0;
    while(a!=b){

    }
    return 0;
  }

  //addresses and casts

  int addr_calc(){
    int arr[20];
    int d=2;
    *(arr+d)=0;
    return 0;
  }

  int arr_pos_load(){
    int arr[20];
    int d=2;
    int c;
    c = arr[d];
    return 0;
  }

  int cast_int(){
    int a;
    double b=1;
    a=(int)b;
    return 0;
  }

  int cast_char(){
    char a;
    int b=57;
    a=(char)b;
    return 0;
  }

  int cast_float(){
    float a;
    double b=1;
    a=(float)b;
    return 0;
  }

  int cast_double(){
    double a;
    float b=1;
    a=(double)b;
    return 0;
  }

  int cast_addr(){
    int *a;
    float *b;
    a=(int*)b;
    return 0;
  }
