#!/usr/bin/python

import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
import numpy as np
import pandas as pd

import re
import itertools
from subprocess import call, check_output, Popen, PIPE
from lazyme.string import color_print

from decimal import *
from collections import Counter, OrderedDict

"""
	AUXILIAR FUNCTIONS
"""


def decimals_length(val):
	str_float = str(val)
	m = re.search(r'(e|E)-\d+$', str_float)
	if m is not None:
		res = re.sub(r'(e|E)-(.+)',r'\2', m.group())
		return int(res)

	return str_float[::-1].find('.')


def _matrix_inferior_left_triangle(matrix, decimals):
	res = []
	n = len(matrix)

	if n == 0:
		return []

	m = len(matrix[0])
	if m == 0:
		return []

	s = max(0, (m-n))
	i = 1
	for l in matrix:
		j = 0
		while ((j < (i+s)) & (j < m)):
			if decimals > 0:
				val = gen_round(sum(l[j]), decimals)
			else:
				val = sum(l[j])
			res.append(val)
			j += 1
		i += 1
	return list(sorted(res))


def compute_domain(matrix, method="default", decimals=0):
	res = []
	n = len(matrix)
	m = len(matrix[0])

	max_size = n+m+1  # longest possible size of resulting convolution

	if method == "default":
		for line in matrix:
			for cell in line:
				res.append(sum(cell))

		res = sorted(list(set(res)))   # to remove duplicates
		#while len(res) > max_size:
		#	lowest = res[0]
		#	rem = 0
		#	last = None
		#	for i,v in enumerate(res):
		#		if last:
		#			diff = abs(v-last)
		#			if diff < lowest:
		#				lowest = diff
		#				rem = i
		#		last = v
		#	del(res[rem])

	elif method == "inferior-triangle":
		res = _matrix_inferior_left_triangle(matrix, decimals)

	res = sorted(list(set(res)))   # to remove duplicates

	return res


def product_matrix(x, y):
	res = []
	for v in x:
		res.append(list(itertools.product([v], y)))

	return res


def dict_value_by_id(d, i):
	if i < 0:
		return 0

	j = 0
	for k,v in d.items():
		if j == i:
			return v
		j += 1

	return 0


def gen_round(value, decimals=0):
	if isinstance(value, Decimal):
		fun = round
	else:
		fun = np.round
	return fun(value, decimals)


"""
	FEATURE FUNCTIONS
"""


def direct_convolution(fun1={}, fun2={}, y=0, decimals=0):
	res = 0
	i = 0
	for k,v in fun2.items():
		a = v
		j = y-i
		b = dict_value_by_id(fun1, j)
		res += a*b
		i += 1
	return res


def convolution(fun1={}, fun2={}, y=0, decimals=0, precision_loss=0):
	res = 0
	min_key = min(fun2.keys())
	max_key = max(fun2.keys())
	for k,v in fun2.items():
		a, b = v, 0

		y_k = round(y-k, decimals)
		fun1_keys = list(fun1.keys())
		#closest_key = min(fun1_keys, key=lambda x: abs(x-y_k))
		#diff = abs(y_k - closest_key)
		#if diff <= precision_loss:
		if y_k in fun1.keys():
			b = fun1[y_k]
		else:
			b = 0
		res += a*b
	return res


def apply_convolution(function1, function2, decimals, precision_loss=0):
	convolved = {}

	x1 = sorted(list(function1.keys()))
	x2 = sorted(list(function2.keys()))

	domain = compute_domain(product_matrix(x1,x2))
	#print("domain size: "+str(domain))

	for count,i in enumerate(domain):
#		if len(convolved) > 0:
#			all_keys = list(convolved.keys())
#			differences = list(map(lambda j: abs(i-j), all_keys))
#			min_diff = min(differences)
#			if min_diff <= precision_loss:  # duplicated key; calculation done already
#				print("continuing...")
#				continue
		res = convolution(fun1=function1, fun2=function2, y=i, decimals=decimals, precision_loss=precision_loss)
		#res = direct_convolution(fun1=function1, fun2=function2, y=count, decimals=decimals)
		if res > 0:  # store only if resultant probability > 0
			convolved[i] = res
		else:
			color_print("BOOM! "+str(i)+"  done so far: "+str(len(convolved)), color="red")
			#exit(1)
	print("    test results: sum(prob) = "+str(sum(convolved.values())))
	print("                  size = "+str(len(convolved)))
	return convolved


def r_convolve(setA=[], setB=[], tag="", decimals=0, method="single"):
	setA_file = "convolutions/" + tag + "_setX.csv"
	setB_file = "convolutions/" + tag + "_setY.csv"
	convolution_file = "convolutions/" + tag + "_last_conv.rds"

	#_count = len(setA)
	cmd = ""
	if tag == "final":
		cmd = "Rscript convolution.r " + tag + " reduce"

	elif method == "single":
		if not isinstance(setA, str):
			setA = list(map(lambda i: i*1e9, setA))
			with open(setA_file, "w+") as fp_a:
				for v in setA:
					bw = fp_a.write(str(v)+"\n")
	
		setB = list(map(lambda j: j*1e9, setB))
		with open(setB_file, "w+") as fp_b:
				for v in setB:
					bw = fp_b.write(str(v)+"\n")
	
		# Command to execute the R script for convolving functions
		cmd = "Rscript convolution.r " + tag

	elif method == "all-together":
		raw_data = {}
		for i,v in enumerate(setA):
			title = str(i)
			raw_data[title] = list(map(lambda i : i*1e9, v))

		df = pd.DataFrame(raw_data)
		df.to_csv(setA_file, header=False, index=False)
		print("Done writing to file")

		# Command to execute the R script for convolving only two functions
		cmd = "Rscript all_convolutions.r " + tag
		#_cmd = "Rscript single_convolution.r " + tag + " " + str(count)

	pipes = Popen(cmd, shell=True, stdout=PIPE, stderr=PIPE)
	std_out, std_err = pipes.communicate()

	if pipes.returncode != 0:
		# an error happened!
		err_msg = "%s. Code: %s" % (std_err.strip(), pipes.returncode)
		color_print('[E] Error on \'' + cmd + '\': ', color='red', bold=True)
		print("   "+err_msg)
		exit(1)
	elif len(std_err):
		# return code is 0 (no error), but we may want to
		# do something with the info on std_err
		# i.e. logger.warning(std_err)
		#color_print('[OK]', color='green')
		pass
	else:
		#color_print('[OK]', color='green')
		pass

	return convolution_file

def py_convolve(setA=[], setB=[], decimals=0, draw=False, drawWhat="result", count=1):
	
	if isinstance(setA, dict):
		distribution_setA = setA
	else:
		# 1st: Round all values in the lists
		precision = decimals #min(decimals_length(min(setA)), decimals)
		setA = list(map(lambda i: round(i, precision), setA))

		# 2nd: Create a distribution for both lists
		distribution_setA = {round(count*k*1e9): v/len(setA) for k, v in Counter(setA).items()}

	if isinstance(setB, dict):
		distribution_setB = setB
	else:
		# 1st: Round all values in the lists
		precision = decimals #min(decimals_length(min(setB)), decimals)
		setB = list(map(lambda j: round(j, precision), setB))

		# 2nd: Create a distribution for both lists
		distribution_setB = {round(count*k*1e9): v/len(setB) for k, v in Counter(setB).items()}

	color_print("SIZES: "+str(len(distribution_setA))+" (+) "+str(len(distribution_setB)), color="yellow")
	# 3rd: Convolve both distributions
	precision_loss = 0 #min(max(distribution_setA.keys()), max(distribution_setB.keys()))
	print(str(distribution_setA)+" !|! "+str(distribution_setB))
	convolution_result = apply_convolution(distribution_setA, distribution_setB, 0, precision_loss)
	# 4th (Optional): Draw the results
	plot_convolution = None
	if draw:
		if drawWhat != "result":
			dist_space_a = np.linspace(min(list(distribution_setA.keys())), max(list(distribution_setA.keys())), len(distribution_setA))
			dist_space_b = np.linspace(min(list(distribution_setB.keys())), max(list(distribution_setB.keys())), len(distribution_setB))
			
			plot_A = plt.plot(dist_space_a, list(distribution_setA.values()), color='red', label="set A")
			plot_B = plt.plot(dist_space_b, list(distribution_setB.values()), color='blue', label="set B")

		dist_space_result = np.linspace(min(list(convolution_result.keys())), max(list(convolution_result.keys())), len(convolution_result))
		
		plot_convolution = plt.plot(dist_space_result, list(convolution_result.values()), color='green', label='convolution (A * B)')

		plt.legend(borderaxespad=0.)
	
		if drawWhat != "result":
			plt.show([plot_A, plot_B, plot_convolution])
		else:
			plt.show([plot_convolution])

	return convolution_result
