#include <stdio.h>
#include <time.h>
#include <math.h>
#include "rapl.h"

#include "stmts/funCall.h"
#include "stmts/instr.h"

#define RUNTIME 1


int main (int argc, char **argv) 
{ char command[500],res[500];
  int  ntimes = 1, mtimes = 1;
  int  core = 0;
  int  i=0, j=0;

#ifdef RUNTIME
  printf("%s\n", "RUNTIME set!");
  clock_t begin, end;
  double time_spent;

  struct timeval tvb,tva;
#endif
  
  FILE * fp;

  // printf("Program to be executed: %d",argc);
  //strcpy( command, "./" );
  if (argc < 4){
    printf("ERROR: Invalid number of arguments \n");
    return 1;
  }
  strcpy(command,argv[1]);
  printf("Program to be executed: %s\n",command);

  ntimes = atoi(argv[2]);
  mtimes = atoi(argv[3]);

  if(ntimes <= 0 || mtimes <= 0){
    printf("ERROR: Invalid values for #measurements and/or #repetitions\n");
    return 1;
  }

  strcpy(res,command);
  strcat(res,".J");
  printf("Command: %s  %d-times res: %s\n",command,ntimes,res);
  

  printf("\n\n RUNNING THE PARAMETRIZED PROGRAM:  %s\n\n\n",command);

  fp = fopen(res,"w");
  rapl_init(core);

  //fprintf(fp,"Package ; Core(s) ; GPU ; DRAM? ; Time (us) \n");
  
  int val = 0;
  for(j=0; j<mtimes; j++){
    rapl_before(fp,core);
      
    #ifdef RUNTIME
      begin = clock();
      gettimeofday(&tvb, 0);
    #endif
    for (i = 0 ; i < ntimes ; i++){
      //Function Calls
      #ifdef FUN
        val=funCall();
      #endif

      #ifdef NULL
        val=void_call();
      #endif
      //declarations
      #ifdef DEC_INT_INIT
        val=dec_int_init();
      #endif

      #ifdef DEC_DOUBLE_INIT
        val=dec_double_init();
      #endif

      #ifdef DEC_FLOAT_INIT
        val=dec_float_init();
      #endif

      #ifdef DEC_ARRAY
        val=dec_array();
      #endif

      #ifdef DEC_STRUCT
        val=dec_struct();
      #endif

      //simple assignments
      #ifdef ASS_VAR_INT
        val=assign_var_int();
      #endif

      #ifdef ASS_VAR_CHAR
        val=assign_var_char();
      #endif

      #ifdef ASS_VAR_FLOAT
        val=assign_var_float();
      #endif

      #ifdef ASS_VAR_DOUBLE
        val=assign_var_double();
      #endif

      #ifdef ASS_VAR_VAR_INT
        val=assign_var_var_int();
      #endif

      #ifdef ASS_VAR_VAR_CHAR
        val=assign_var_var_char();
      #endif

      #ifdef ASS_VAR_VAR_FLOAT
        val=assign_var_var_float();
      #endif

      #ifdef ASS_VAR_VAR_DOUBLE
        val=assign_var_var_double();
      #endif

      #ifdef ASS_VAR_ADDR
        val=assign_var_addr();
      #endif

      //operations assignments
      //ADD
      #ifdef ASS_ADD_INT_INT          //a=1+2 (int)
        val=assign_add_int_int();
      #endif

      #ifdef ASS_ADD_FLOAT_FLOAT      //a=1.0+2.0 (float)
        val=assign_add_float_float();
      #endif

      #ifdef ASS_ADD_DOUBLE_DOUBLE    //a=1.0+2.0 (double)
        val=assign_add_double_double();
      #endif

      #ifdef ASS_ADD_VAR_VAR_INT      //a=b+c (int)
        val=assign_add_var_var_int();
      #endif

      #ifdef ASS_ADD_VAR_VAR_FLOAT    //a=b+c (float)
        val=assign_add_var_var_float();
      #endif

      #ifdef ASS_ADD_VAR_VAR_DOUBLE   //a=b+c (double)
        val=assign_add_var_var_double();
      #endif

      #ifdef ASS_ADD_VAR_INT          //a=b+1 (int)
        val=assign_add_var_int();
      #endif

      #ifdef ASS_ADD_VAR_FLOAT    //a=b+1.0 (float)
        val=assign_add_var_float();
      #endif

      #ifdef ASS_ADD_VAR_DOUBLE       //a=b+1.0 (double)
        val=assign_add_var_double();
      #endif

      //SUB
      #ifdef ASS_SUB_INT_INT          //a=1-2
        val=assign_sub_int_int();
      #endif

      #ifdef ASS_SUB_FLOAT_FLOAT      //a=1.0-2.0
        val=assign_sub_float_float();
      #endif

      #ifdef ASS_SUB_DOUBLE_DOUBLE    //a=1.0-2.0
        val=assign_sub_double_double();
      #endif

      #ifdef ASS_SUB_VAR_VAR_INT      //a=b-c (int)
        val=assign_sub_var_var_int();
      #endif

      #ifdef ASS_SUB_VAR_VAR_FLOAT    //a=b-c (float)
        val=assign_sub_var_var_float();
      #endif

      #ifdef ASS_SUB_VAR_VAR_DOUBLE   //a=b-c (double)
        val=assign_sub_var_var_double();
      #endif

      #ifdef ASS_SUB_VAR_INT          //a=b-1 (int)
        val=assign_sub_var_int();
      #endif

      #ifdef ASS_SUB_VAR_FLOAT    //a=b-1.0 (float)
        val=assign_sub_var_float();
      #endif

      #ifdef ASS_SUB_VAR_DOUBLE       //a=b-1.0 (float)
        val=assign_sub_var_double();
      #endif

      //MUL
      #ifdef ASS_MUL_INT_INT          //a=1*2
        val=assign_mul_int_int();
      #endif

      #ifdef ASS_MUL_FLOAT_FLOAT      //a=1.0*2.0
        val=assign_mul_float_float();
      #endif

      #ifdef ASS_MUL_DOUBLE_DOUBLE    //a=1.0*2.0
        val=assign_mul_double_double();
      #endif

      #ifdef ASS_MUL_VAR_VAR_INT      //a=b*c (int)
        val=assign_mul_var_var_int();
      #endif

      #ifdef ASS_MUL_VAR_VAR_FLOAT    //a=b*c (float)
        val=assign_mul_var_var_float();
      #endif

      #ifdef ASS_MUL_VAR_VAR_DOUBLE   //a=b*c (double)
        val=assign_mul_var_var_double();
      #endif

      #ifdef ASS_MUL_VAR_INT          //a=b*1 (int)
        val=assign_mul_var_int();
      #endif

      #ifdef ASS_MUL_VAR_FLOAT    //a=b*1.0 (float)
        val=assign_mul_var_float();
      #endif

      #ifdef ASS_MUL_VAR_DOUBLE       //a=b*1.0 (float)
        val=assign_mul_var_double();
      #endif

      //DIV
      #ifdef ASS_DIV_INT_INT          //a=1/2
        val=assign_div_int_int();
      #endif

      #ifdef ASS_DIV_FLOAT_FLOAT      //a=1.0/2.0
        val=assign_div_float_float();
      #endif

      #ifdef ASS_DIV_DOUBLE_DOUBLE    //a=1.0/2.0
        val=assign_div_double_double();
      #endif

      #ifdef ASS_DIV_VAR_VAR_INT      //a=b/c (int)
        val=assign_div_var_var_int();
      #endif

      #ifdef ASS_DIV_VAR_VAR_FLOAT    //a=b/c (float)
        val=assign_div_var_var_float();
      #endif

      #ifdef ASS_DIV_VAR_VAR_DOUBLE   //a=b/c (double)
        val=assign_div_var_var_double();
      #endif

      #ifdef ASS_DIV_VAR_INT          //a=b/1 (int)
        val=assign_div_var_int();
      #endif

      #ifdef ASS_DIV_VAR_FLOAT    //a=b/1.0 (float)
        val=assign_div_var_float();
      #endif

      #ifdef ASS_DIV_VAR_DOUBLE       //a=b/1.0 (float)
        val=assign_div_var_double();
      #endif

      //pointer assignments
      #ifdef ASS_VAR_REF_INT
        val=assign_var_ref_int();
      #endif

      #ifdef ASS_VAR_REF_CHAR
        val=assign_var_ref_char();
      #endif

      #ifdef ASS_VAR_REF_FLOAT
        val=assign_var_ref_float();
      #endif

      #ifdef ASS_VAR_REF_DOUBLE
        val=assign_var_ref_double();
      #endif

      #ifdef ASS_VAR_REF_ADDR
        val=assign_var_ref_addr();
      #endif

      //CONDITIONS
      #ifdef CMP_VAR_INT
        val=cmp_var_int();
      #endif

      #ifdef CMP_VAR_CHAR
        val=cmp_var_char();
      #endif

      #ifdef CMP_VAR_FLOAT
        val=cmp_var_float();
      #endif

      #ifdef CMP_VAR_DOUBLE
        val=cmp_var_double();
      #endif

      #ifdef CMP_VAR_VAR_INT
        val=cmp_var_var_int();
      #endif

      #ifdef CMP_VAR_VAR_CHAR
        val=cmp_var_var_char();
      #endif

      #ifdef CMP_VAR_VAR_FLOAT
        val=cmp_var_var_float();
      #endif

      #ifdef CMP_VAR_VAR_DOUBLE
        val=cmp_var_var_double();
      #endif

      //addresses and casts
      #ifdef ADDR_CALC
        val=addr_calc();
      #endif

      #ifdef ARR_POS_LOAD
        val=arr_pos_load();
      #endif

      #ifdef CAST_INT
        val=cast_int();
      #endif

      #ifdef CAST_CHAR
        val=cast_char();
      #endif

      #ifdef CAST_FLOAT
        val=cast_float();
      #endif

      #ifdef CAST_DOUBLE
        val=cast_double();
      #endif

      #ifdef CAST_ADDR
        val=cast_addr();
      #endif

    }
    #ifdef RUNTIME
      end = clock();
      gettimeofday(&tva, 0);
      //time_spent = (double)(end - begin) / CLOCKS_PER_SEC;
      time_spent = (tva.tv_sec-tvb.tv_sec)*1000000 + tva.tv_usec-tvb.tv_usec;
    #endif
      rapl_after(fp,core);

    #ifdef RUNTIME	
      fprintf(fp," %G \n",time_spent);
    #endif	
  }
    
  printf("\n\n END of PARAMETRIZED PROGRAM: \n");

  fclose(fp);
  fflush(stdout);

  return 0;
}
