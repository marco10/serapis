#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "Test.h"
#include "base/Client.h"
#include "base/Email.h"
#include "base/Util.h"

//setup hooks

void 
setup_bob(CLIENT bob) 
{
    setClientId(bob, bob);
    setClientName(bob, "bob");
    setClientOutbuffer(bob, 1);

    #ifdef ADDRESS_BOOK
      bobSetAddressBook();
    #endif

    #ifdef KEYS
      setClientPrivateKey(bob, 123);
      bobKeyAddRjh();
      bobKeyAddChuck();
    #endif
}


void
rjhEnableForwarding() 
{
  setClientForwardReceiver(rjh, chuck);
}

void
rjhSetAutoRespond()
{
  setClientAutoResponse(rjh, 1);
}

void 
setup_rjh(CLIENT rjh)
{    
    setClientId(rjh, rjh);
    setClientName(rjh, "rjh");
    setClientOutbuffer(rjh, 2);

    #ifdef AUTO_RESPOND
      rjhSetAutoRespond();
    #endif

    #ifdef FORWARD
      rjhEnableForwarding();
    #endif

    #ifdef KEYS
      setClientPrivateKey(rjh, 456);
      rjhKeyAddBob();
      rjhKeyAddChuck();
    #endif
}

void 
setup_chuck(CLIENT chuck)
{
    setClientId(chuck, chuck);
    setClientName(chuck, "chuck");
    setClientOutbuffer(chuck, 2);
    
    #ifdef KEYS
      setClientPrivateKey(chuck, 789);
      chuckKeyAddBob();
      chuckKeyAddRjh();
    #endif
}


// KEYS
void
bobKeyAddRjh()
{ 
    createClientKeyringEntry(bob);    
    setClientKeyringUser(bob, 0, 1);
    setClientKeyringPublicKey(bob, 0, 456);
    /*
    puts("bob added rjhs key");
    printf("%d\n",getClientKeyringUser(bob, 0));
    printf("%d\n",getClientKeyringPublicKey(bob, 0));
    */
}

void
bobKeyAddChuck()
{
    createClientKeyringEntry(bob);
    setClientKeyringUser(bob, 1, 3);
    setClientKeyringPublicKey(bob, 1, 789);
}


void
rjhKeyAddBob()
{
    createClientKeyringEntry(rjh);
    setClientKeyringUser(rjh, 0, 0);
    setClientKeyringPublicKey(rjh, 0, 123);
}

void
rjhKeyAddChuck()
{
    createClientKeyringEntry(rjh);
    setClientKeyringUser(rjh, 1, 2);
    setClientKeyringPublicKey(rjh, 1, 789);
}

void
chuckKeyAddBob()
{
    createClientKeyringEntry(chuck);
    setClientKeyringUser(chuck, 0, 1);
    setClientKeyringPublicKey(chuck, 0, 123);
}

void
chuckKeyAddRjh()
{
    createClientKeyringEntry(chuck);
    setClientKeyringUser(chuck, 1, 2);
    setClientKeyringPublicKey(chuck, 1, 456);
}


void
bobKeyChange()
{
  generateKeyPair(bob, 777);
}

void
rjhKeyChange()
{
  generateKeyPair(rjh, 666);
}


//actions

void
bobToRjh(char *arg)
{

  char subj[15] = "Hi, RJH [";
  strcat(subj, arg);
  strcat(subj, "]");
  char* body = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer pretium risus vitae enim porta, non eleifend lectus luctus. Fusce vitae commodo arcu. Ut tempor elit ut purus vulputate, vel porta.";
  sendEmail(bob, rjh, subj, body);
  if (!is_queue_empty()) {
    outgoing(get_queued_client(), get_queued_email());
  }
}

void
rjhToBob(char *arg)
{

  char subj[15] = "Hi, Bob [";
  strcat(subj, arg);
  strcat(subj, "]");
  char* body = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer pretium risus vitae enim porta, non eleifend lectus luctus. Fusce vitae commodo arcu. Ut tempor elit ut purus vulputate, vel porta.";
  sendEmail(rjh, bob, subj, body);
}

void
bobSetAddressBook()
{
    setClientAddressBookSize(bob, 1);
    setClientAddressBookAlias(bob, 0, rjh);
}

void setup() {
  bob = 0;
  setup_bob(bob);
  printf("bob: %d\n",bob);
  //rjh = createClient("rjh");
  rjh = 1;
  setup_rjh(rjh);
  printf("rjh: %d\n",rjh);  
  //chuck = createClient("chuck");
  chuck = 2;
  setup_chuck(chuck);
  printf("chuck: %d\n",chuck);
}

void test() {
  int i = 0;
  int j = 0;
  int n = 10000;
  char buff_i[3];
  char buff_j[3];
  while (n > (i+j)) {
    sprintf(buff_i, "%d", i);
    sprintf(buff_j, "%d", j);
    if (i > j) {
      j++;
      rjhToBob(buff_j);
    } else {
      i++;
      bobToRjh(buff_i);
    }
  }
  int m = getEmail(rjh, bob, "Hi, Bob [4999]");
  if (m >= 0) {
    printf("\n[EMAIL FOUND]\n::::\n");
    printMail(m, 1);
  }
}

int
main (void)
{ 
  setup();
  test();
  return 0;
}
