#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "Main.h"

// Client

int queue_empty = 1;

EMAIL queued_message;

CLIENT queued_client;

// ADDRESS_BOOK
void
sendToAddressBook (CLIENT client, EMAIL msg)
{

}

// AUTO_RESPOND
void
autoRespond (CLIENT client, EMAIL msg)
{
  #ifdef AUTO_RESPOND
    //puts("sending autoresponse\n");
    int sender = getEmailFrom(msg);
    setEmailTo(msg, sender);
    queue(client, msg);
  #endif
}

// FORWARD
void
forward (CLIENT client, EMAIL msg)
{  
  //puts("Forwarding message.\n");  
  //printMail(msg);
  queue(client, msg);
 
}

// outgoing emails leave the client at this point. here they are put in an outgoing queue instead.
void
mail (CLIENT client, EMAIL msg)
{
  int buffer = getClientOutbuffer(client);
  FILE* fp = outBuffer(buffer);
  fputs("mail sent!\n", fp);
  incoming (getEmailTo(msg), msg);
}

// emails to be sent are processed by this method before beeing mailed.
void
outgoing (CLIENT client, EMAIL msg)
{
  #ifdef SIGN
    sign (client, msg);
  #endif

  #ifdef ENCRYPT
    int receiver = getEmailTo(msg);
    int pubkey = findPublicKey(client, receiver);
    if (pubkey) {
        setEmailEncryptionKey(msg, pubkey);
        setEmailIsEncrypted(msg, 1);
    }
  #endif

  #ifdef ADDRESS_BOOK
    int size = getClientAddressBookSize(client);
    if (size) {
      sendToAddressBook(client, msg);
      //puts("sending to alias in address book\n");
      int receiver = getEmailTo(msg);

      //puts("sending to second receipient\n");
      int second = getClientAddressBookAlias(client, 1);
      setEmailTo(msg, getClientAddressBookAlias(client, 0));
      setEmailFrom(msg, getClientId(client));
      mail(client, msg);

      setEmailTo(msg, getClientAddressBookAlias(client, 0));
    }

  #endif

  setEmailFrom(msg, getClientId(client));
  mail(client, msg);
}

// incoming emails reach the user at this point. here they are put in a mailbox.
void
deliver (CLIENT client, EMAIL msg)
{
   int buffer = getClientOutbuffer(client);
   FILE* fp = outBuffer(buffer);
   fputs("mail received!\n", fp);
   printMail(msg, buffer);
}

void
verify (CLIENT client, EMAIL msg)
{

  if (!isReadable (msg) || !isSigned(msg))
    return;
    
  int pubkey = findPublicKey(client, getEmailFrom(msg));
  if (pubkey && isKeyPairValid(getEmailSignKey(msg), pubkey)) {
    setEmailIsSignatureVerified(msg, 1);
  }

}

// incoming emails are processed by this method before delivery.
void
incoming (CLIENT client, EMAIL msg)
{
  #ifdef DECRYPT
    int privkey = getClientPrivateKey(client);
    if (privkey) {
      
        if (isEncrypted(msg)
            && isKeyPairValid(getEmailEncryptionKey(msg), privkey))
          {
            setEmailIsEncrypted(msg, 0);
            setEmailEncryptionKey(msg, 0);
          }
    }
  #endif

  #ifdef VERIFY
    verify (client, msg);
  #endif

  deliver (client, msg);

  #ifdef AUTO_RESPOND
    if (getClientAutoResponse(client)) {
      autoRespond (client, msg);
    }
  #endif

  #ifdef FORWARD
    int fwreceiver = getClientForwardReceiver(client);
    if (fwreceiver >= 0) {
      
      setEmailTo(msg, fwreceiver);  
      forward(client, msg);
      
    }
  #endif
}

CLIENT createClient(char *name) {
    CLIENT client = initClient();
    return client;
}

void
sign (CLIENT client, EMAIL msg)
{
  int privkey = getClientPrivateKey(client);
  if (!privkey)
    return;
  setEmailIsSigned(msg, 1);
  setEmailSignKey(msg, privkey);
}

void
sendEmail (CLIENT sender, CLIENT receiver, char* subj, char* body) 
{
  EMAIL email = createEmail (sender, receiver, subj, body);
  outgoing (sender, email);
  //incoming (receiver, email);
  
}

void
queue (CLIENT client, EMAIL msg)
{
    queue_empty = 0;
    queued_message = msg;
    queued_client = client;
}

int
is_queue_empty ()
{
    return queue_empty;
}

CLIENT
get_queued_client ()
{
    return queued_client;
}

EMAIL
get_queued_email ()
{
    return queued_message;
}

int
isKeyPairValid (int publicKey, int privateKey)
{
    printf("keypair valid %d %d", publicKey, privateKey);
  if (!publicKey || !privateKey)
    return 0;
  return privateKey == publicKey;
}

void
generateKeyPair (CLIENT client, int seed)
{
    setClientPrivateKey(client, seed);
}

/* --- */
int __ste_Client_max     = 3;
int __ste_Client_counter = 0;

int initClient() {
  if (__ste_Client_counter < __ste_Client_max) {
    return ++__ste_Client_counter;
  } else {
    return -1;
  }
}

char __ste_client_names[3][50];
//char* __ste_client_name0 = 0;
//char* __ste_client_name1 = 0;
//char* __ste_client_name2 = 0;

char* getClientName(int handle) {
  char *res = NULL;
  if (handle >= 0 && handle < __ste_Client_max) {
    res = __ste_client_names[handle];
  }
  return res;
}
void setClientName(int handle, char* value) {
  if (handle >= 0 && handle < __ste_Client_max) {
    //__ste_client_names[handle] = value;
    strcpy(__ste_client_names[handle], value);
  }
}

int __ste_client_outbuffers[3];
//int __ste_client_outbuffer0 = 0;
//int __ste_client_outbuffer1 = 0;
//int __ste_client_outbuffer2 = 0;
//int __ste_client_outbuffer3 = 0;

int getClientOutbuffer(int handle) {
  int res = 0;
  if (handle >= 0 && handle < __ste_Client_max) {
    res = __ste_client_outbuffers[handle];
  }
  return res;
}
void setClientOutbuffer(int handle, int value) {
  if (handle >= 0 && handle < __ste_Client_max) {
    __ste_client_outbuffers[handle] = value;
  }
}

int __ste_ClientAddressBook_sizes[3] = {0, 0, 0};
//int __ste_ClientAddressBook_size0 = 0;
//int __ste_ClientAddressBook_size1 = 0;
//int __ste_ClientAddressBook_size2 = 0;

int getClientAddressBookSize(int handle){
  int res = 0;
  if (handle >= 0 && handle < __ste_Client_max) {
    res = __ste_ClientAddressBook_sizes[handle];
  }
  return res;
}

void setClientAddressBookSize(int handle, int value) {
  if (handle >= 0 && handle < __ste_Client_max) {
    __ste_ClientAddressBook_sizes[handle] = value;
  }
}

int createClientAddressBookEntry(int handle){
    int size = getClientAddressBookSize(handle);
  if (size < 3) {
      setClientAddressBookSize(handle, size + 1);
    return size + 1;
  } else return -1;
}

int __ste_Client_AddressBook_Alias[3][3];
//int __ste_Client_AddressBook0_Alias0 = 0;
//int __ste_Client_AddressBook0_Alias1 = 0;
//int __ste_Client_AddressBook0_Alias2 = 0;
//int __ste_Client_AddressBook1_Alias0 = 0;
//int __ste_Client_AddressBook1_Alias1 = 0;
//int __ste_Client_AddressBook1_Alias2 = 0;
//int __ste_Client_AddressBook2_Alias0 = 0;
//int __ste_Client_AddressBook2_Alias1 = 0;
//int __ste_Client_AddressBook2_Alias2 = 0;

int getClientAddressBookAlias(int handle, int index) {
  int res = 0;
  if (handle < 0 || handle >= __ste_Client_max) {
    return 0;
  }

  if (index < 0 || index >= 3) {
    return 0;
  }

  res = __ste_Client_AddressBook_Alias[handle][index];
  return res;
}

int findClientAddressBookAlias(int handle, int userid) {
  int res = -1;
  if (handle < 0 || handle >= __ste_Client_max) {
    return -1;
  }

  int i;
  for (i = 0; i < 3; i++) {
    if (userid == __ste_Client_AddressBook_Alias[handle][i]) {
      res = i;
    }
  }

  return res;
}

void setClientAddressBookAlias(int handle, int index, int value) {
  if (handle >= 0 && handle < __ste_Client_max) {
    int *aux = __ste_Client_AddressBook_Alias[handle];
    if (index >= 0 && index < 3) {
      aux[index] = value;
    }
  }
}

/*
int __ste_Client_AddressBook0_Address0 = 0;
int __ste_Client_AddressBook0_Address1 = 0;
int __ste_Client_AddressBook0_Address2 = 0;
int __ste_Client_AddressBook1_Address0 = 0;
int __ste_Client_AddressBook1_Address1 = 0;
int __ste_Client_AddressBook1_Address2 = 0;
int __ste_Client_AddressBook2_Address0 = 0;
int __ste_Client_AddressBook2_Address1 = 0;
int __ste_Client_AddressBook2_Address2 = 0;

int getClientAddressBookAddress(int handle, int index) {
  if (handle == 1) {
    if (index == 0) {
      return __ste_Client_AddressBook0_Address0;
    } else if (index == 1) {
      return __ste_Client_AddressBook0_Address1;
    } else if (index == 2) {
      return __ste_Client_AddressBook0_Address2;
    } else {
      return 0;
    }
  } else if (handle == 2) {
    if (index == 0) {
      return __ste_Client_AddressBook1_Address0;
    } else if (index == 1) {
      return __ste_Client_AddressBook1_Address1;
    } else if (index == 2) {
      return __ste_Client_AddressBook1_Address2;
    } else {
      return 0;
    }
  } else if (handle == 3) {
    if (index == 0) {
      return __ste_Client_AddressBook2_Address0;
    } else if (index == 1) {
      return __ste_Client_AddressBook2_Address1;
    } else if (index == 2) {
      return __ste_Client_AddressBook2_Address2;
    } else {
      return 0;
    }
  } else {
    return 0;
  }
}
void setClientAddressBookAddress(int handle, int index, int value) {
  if (handle == 1) {
    if (index == 0) {
      __ste_Client_AddressBook0_Address0 = value;
    } else if (index == 1) {
      __ste_Client_AddressBook0_Address1 = value;
    } else if (index == 2) {
      __ste_Client_AddressBook0_Address2 = value;
    }
  } else if (handle == 2) {
    if (index == 0) {
      __ste_Client_AddressBook1_Address0 = value;
    } else if (index == 1) {
      __ste_Client_AddressBook1_Address1 = value;
    } else if (index == 2) {
      __ste_Client_AddressBook1_Address2 = value;
    }
  } else if (handle == 3) {
    if (index == 0) {
      __ste_Client_AddressBook2_Address0 = value;
    } else if (index == 1) {
      __ste_Client_AddressBook2_Address1 = value;
    } else if (index == 2) {
      __ste_Client_AddressBook2_Address2 = value;
    }
  } 
}
*/
int __ste_client_autoResponses[3];
//int __ste_client_autoResponse0 = 0;
//int __ste_client_autoResponse1 = 0;
//int __ste_client_autoResponse2 = 0;

int getClientAutoResponse(int handle) {
  int res = -1;
  if (handle >= 0 && handle < __ste_Client_max) {
    res = __ste_client_autoResponses[handle];
  }
  return res;
}

void setClientAutoResponse(int handle, int value) {
  if (handle >= 0 && handle < __ste_Client_max) {
    __ste_client_autoResponses[handle] = value;
  } 
}
int __ste_client_privateKeys[3];
//int __ste_client_privateKey0 = 0;
//int __ste_client_privateKey1 = 0;
//int __ste_client_privateKey2 = 0;

int getClientPrivateKey(int handle) {
  int res = 0;
  if (handle >= 0 && handle < __ste_Client_max) {
    res = __ste_client_privateKeys[handle];
  }
  return res;
}
void setClientPrivateKey(int handle, int value) {
  if (handle >= 0 && handle < __ste_Client_max) {
    __ste_client_privateKeys[handle] = value;
  }
}
int __ste_ClientKeyring_sizes[3];
//int __ste_ClientKeyring_size0 = 0;
//int __ste_ClientKeyring_size1 = 0;
//int __ste_ClientKeyring_size2 = 0;

int getClientKeyringSize(int handle){
  int res = 0;
  if (handle >= 0 && handle < __ste_Client_max) {
    res = __ste_ClientKeyring_sizes[handle];
  }
  return res;
}
void setClientKeyringSize(int handle, int value) {
  if (handle >= 0 && handle < __ste_Client_max) {
    __ste_ClientKeyring_sizes[handle] = value;
  }
}
int createClientKeyringEntry(int handle){
    int size = getClientKeyringSize(handle);
  if (size < 3) {
      setClientKeyringSize(handle, size + 1);
    return size + 1;
  } else return -1;
}
int __ste_Client_Keyring_Users[3][3];
//int __ste_Client_Keyring0_User0 = 0;
//int __ste_Client_Keyring0_User1 = 0;
//int __ste_Client_Keyring0_User2 = 0;
//int __ste_Client_Keyring1_User0 = 0;
//int __ste_Client_Keyring1_User1 = 0;
//int __ste_Client_Keyring1_User2 = 0;
//int __ste_Client_Keyring2_User0 = 0;
//int __ste_Client_Keyring2_User1 = 0;
//int __ste_Client_Keyring2_User2 = 0;

int getClientKeyringUser(int handle, int index) {
  int res = 0;
  if (handle >= 0 && handle < __ste_Client_max) {
    int* keys = __ste_Client_Keyring_Users[handle];
    if (index >= 0 && index < 3) {
      res = keys[index];
    }
  }

  return res;
}


void setClientKeyringUser(int handle, int index, int value) {
  if (handle >= 0 && handle < __ste_Client_max) {
    int* keys = __ste_Client_Keyring_Users[handle];
    if (index >= 0 && index < 3) {
      keys[index] = value;
    }
  }
}

int __ste_Client_Keyring_PublicKeys[3][3];
//int __ste_Client_Keyring0_PublicKey0 = 0;
//int __ste_Client_Keyring0_PublicKey1 = 0;
//int __ste_Client_Keyring0_PublicKey2 = 0;
//int __ste_Client_Keyring1_PublicKey0 = 0;
//int __ste_Client_Keyring1_PublicKey1 = 0;
//int __ste_Client_Keyring1_PublicKey2 = 0;
//int __ste_Client_Keyring2_PublicKey0 = 0;
//int __ste_Client_Keyring2_PublicKey1 = 0;
//int __ste_Client_Keyring2_PublicKey2 = 0;

int getClientKeyringPublicKey(int handle, int index) {
  int res = 0;
  if (handle >= 0 && handle < __ste_Client_max) {
    int* keys = __ste_Client_Keyring_PublicKeys[handle];
    if (index >= 0 && index < __ste_Client_max) {
      res = keys[index];
    }
  }
  return res;
}

int findPublicKey(int handle, int userid) {
  int res = 0;
  if (handle >= 0 && handle < __ste_Client_max) {
    int* keyring = __ste_Client_Keyring_Users[handle];
    int i;
    for (i = 0; i < (__ste_Client_max - 1); i++) {  // last position is reserved to the user's own key.
      if (userid == keyring[i]) {
        res = __ste_Client_Keyring_PublicKeys[handle][userid];
      }
    }
  }
  return res;
}

void setClientKeyringPublicKey(int handle, int index, int value) {
  if (handle >= 0 && handle < __ste_Client_max) {
    int* pubkeys = __ste_Client_Keyring_PublicKeys[handle];
    if (index >= 0 && index < __ste_Client_max) {
      pubkeys[index] = value;
    }
  }
}

int __ste_client_forwardReceivers[3];
//int __ste_client_forwardReceiver0 =0;
//int __ste_client_forwardReceiver1 =0;
//int __ste_client_forwardReceiver2 =0;
//int __ste_client_forwardReceiver3 =0;
int getClientForwardReceiver(int handle) {
  int res = -1;
  if (handle >= 0 && handle < __ste_Client_max) {
    res = __ste_client_forwardReceivers[handle];
  }
  return res;
}

void setClientForwardReceiver(int handle, int value) {
  if (handle >= 0 && handle < __ste_Client_max) {
    __ste_client_forwardReceivers[handle] = value;
  }
}
int __ste_client_idCounters[5];
//int __ste_client_idCounter0 = 0;
//int __ste_client_idCounter1 = 0;
//int __ste_client_idCounter2 = 0;

int getClientId(int handle) {
  int res = 0;
  if (handle >= 0 && handle < __ste_Client_max) {
    res = __ste_client_idCounters[handle];
  }
  return res;
}
void setClientId(int handle, int value) {
  if (handle >= 0 && handle < __ste_Client_max) {
    __ste_client_idCounters[handle] = value;
  }
}


// Email
void
printMail (EMAIL msg, int buffer)
{
  FILE* fp = outBuffer(buffer);

  fprintf (fp, "-----------------------\n");
  fprintf (fp, "ID:\n  %i\n", getEmailId(msg));
  fprintf (fp, "FROM:\n  %i\n", getEmailFrom(msg));
  fprintf (fp, "TO:\n  %i\n", getEmailTo(msg));
  fprintf (fp, "IS_READABLE\n  %i\n", isReadable(msg));
  fprintf (fp, ">>>\n  %s\n. . . . . .\n  %s\n<<<\n", getEmailSubject(msg), getEmailBody(msg));

  #ifdef ENCRYPT
    fprintf (fp, "ENCRYPTED\n  %d\n", isEncrypted(msg));
    fprintf (fp, "ENCRYPTION KEY\n  %d\n", getEmailEncryptionKey(msg));
  #endif

  #ifdef SIGN
    fprintf (fp, "SIGNED\n  %i\n", isSigned(msg));
    fprintf (fp, "SIGNATURE\n  %i\n", getEmailSignKey(msg));
  #endif
  fprintf (fp, "-----------------------\n");
}

int
isReadable (EMAIL msg)
{
  #ifdef ENCRYPT
    int encrypted = isEncrypted(msg);
    if (!encrypted) {
      return 1;
    } else {
      return 0;
    }
  #endif

  return 1;
}

EMAIL cloneEmail(EMAIL msg) {
    return msg;
}

EMAIL createEmail (int from, int to, char* subj, char* body) {
  EMAIL msg = initEmail();
  setEmailFrom(msg, from);
  setEmailTo(msg, to);
  setEmailSubject(msg, subj);
  setEmailBody(msg, body);
  return msg;
}

EMAIL getEmail(int from, int to, char* subj) {
  EMAIL msg = findEmail(from, to, subj);

  return msg;
}

/* --- */
int __ste_Email_max     = 10000;
int __ste_Email_counter = 0;
int initEmail() {
  if (__ste_Email_counter < __ste_Email_max) {
    return ++__ste_Email_counter;
  } else {
    return -1;
  }
}

int __ste_email_ids[10000];
//int __ste_email_id0 = 0;
//int __ste_email_id1 = 0;
int getEmailId(int handle) {
  int res = 0;
  if (handle >= 0 && handle < __ste_Email_max) {
    res = __ste_email_ids[handle];
  }
  return res;
}
void setEmailId(int handle, int value) {
  if (handle >= 0 && handle < __ste_Email_max) {
    __ste_email_ids[handle] = value;
  }
}

int __ste_email_from[10000];
//int __ste_email_from0 = 0;
//int __ste_email_from1 = 0;
int getEmailFrom(int handle) {
  int res = 0;
  if (handle >= 0 && handle < __ste_Email_max) {
    res = __ste_email_from[handle];
  }
  return res;
}
void setEmailFrom(int handle, int value) {
  if (handle >= 0 && handle < __ste_Email_max) {
    __ste_email_from[handle] = value;
  }
}

int __ste_email_to[10000];
//int __ste_email_to0 = 0;
//int __ste_email_to1 = 0;
int getEmailTo(int handle) {
  int res = 0;
  if (handle >= 0 && handle < __ste_Email_max) {
    res = __ste_email_to[handle];
  }
  return res;
}
void setEmailTo(int handle, int value) {
  if (handle >= 0 && handle < __ste_Email_max) {
    __ste_email_to[handle] = value;
  }
}

char __ste_email_subject[10000][200];
//char* __ste_email_subject0;
//char* __ste_email_subject1;
char* getEmailSubject(int handle) {
  char* res = NULL;
  if (handle >= 0 && handle < __ste_Email_max) {
    res = __ste_email_subject[handle];
  }
  return res;
}
void setEmailSubject(int handle, char* value) {
  if (handle >= 0 && handle < __ste_Email_max) {
    strcpy(__ste_email_subject[handle], value);
  }
}

char __ste_email_body[10000][200];
//char* __ste_email_body0 = 0;
//char* __ste_email_body1 = 0;
char* getEmailBody(int handle) {
  char* res = NULL;
  if (handle >= 0 && handle < __ste_Email_max) {
    res = __ste_email_body[handle];
  }
  return res;
}
void setEmailBody(int handle, char* value) {
  if (handle >= 0 && handle < __ste_Email_max) {
    strcpy(__ste_email_body[handle], value);
  }
}

int __ste_email_isEncrypted[10000];
//int __ste_email_isEncrypted0 = 0;
//int __ste_email_isEncrypted1 = 0;
int isEncrypted(int handle) {
  int res = 0;
  if (handle >= 0 && handle < __ste_Email_max) {
    res = __ste_email_isEncrypted[handle];
  }
  return res;
}

void setEmailIsEncrypted(int handle, int value) {
  if (handle >= 0 && handle < __ste_Email_max) {
    __ste_email_isEncrypted[handle] = value;
  }
}

int __ste_email_encryptionKey[10000];
//int __ste_email_encryptionKey0 = 0;
//int __ste_email_encryptionKey1 = 0;
int getEmailEncryptionKey(int handle) {
  int res = 0;
  if (handle >= 0 && handle < __ste_Email_max) {
    res = __ste_email_encryptionKey[handle];
  }
  return res;
}

void setEmailEncryptionKey(int handle, int value) {
  if (handle >= 0 && handle < __ste_Email_max) {
    __ste_email_encryptionKey[handle] = value;
  }
}

int __ste_email_isSigned[10000];
//int __ste_email_isSigned0 = 0;
//int __ste_email_isSigned1 = 0;
int isSigned(int handle) {
  int res = 0;
  if (handle >= 0 && handle < __ste_Email_max) {
    res = __ste_email_isSigned[handle];
  }
  return res;
}
void setEmailIsSigned(int handle, int value) {
  if (handle >= 0 && handle < __ste_Email_max) {
    __ste_email_isSigned[handle] = value;
  }
}

int __ste_email_signKey[10000];
//int __ste_email_signKey0 = 0;
//int __ste_email_signKey1 = 0;
int getEmailSignKey(int handle) {
  int res = 0;
  if (handle >= 0 && handle < __ste_Email_max) {
    res = __ste_email_signKey[handle];
  }
  return res;
}

void setEmailSignKey(int handle, int value) {
  if (handle >= 0 && handle < __ste_Email_max) {
    __ste_email_signKey[handle] = value;
  }
}

int __ste_email_isSignatureVerified[10000];
//int __ste_email_isSignatureVerified0;
//int __ste_email_isSignatureVerified1;
int isVerified(int handle) {
  int res = 0;
  if (handle >= 0 && handle < __ste_Email_max) {
    res = __ste_email_isSignatureVerified[handle];
  }
  return res;
}
void setEmailIsSignatureVerified(int handle, int value) {
  if (handle >= 0 && handle < __ste_Email_max) {
    __ste_email_isSignatureVerified[handle] = value;
  }
}

// Custom
int findEmail(int from, int to, char* subject) {
  int i;
  int res = -1;
  for (i = 0; (i < 10000 && res < 0); i++) {
    int i_to     = __ste_email_to[i];
    int i_from   = __ste_email_from[i];
    char* i_subj = __ste_email_subject[i];
    int checkStr = strcmp(i_subj, subject);
    if ((from < 0 || i_from == from) && (to < 0 || i_to == to) && (subject == NULL || checkStr == 0 )) {
      res = i;
    }
  }
  return res;
}


// Util
int 
prompt(char* msg) 
{
    printf("%s\n", msg);
    int retval;
    scanf("%i", &retval);
    return retval; 
}

FILE* outBuffer(int b) {
  if (b == 1) {
    return stdout;
  } else {
    return stderr;
  }
}


//setup hooks

void 
setup_bob(CLIENT bob) 
{
    setClientId(bob, bob);
    setClientName(bob, "bob");
    setClientOutbuffer(bob, 1);

    #ifdef ADDRESS_BOOK
      bobSetAddressBook();
    #endif

    #ifdef KEYS
      setClientPrivateKey(bob, 123);
      bobKeyAddRjh();
      bobKeyAddChuck();
    #endif
}


void
rjhEnableForwarding() 
{
  setClientForwardReceiver(rjh, chuck);
}

void
rjhSetAutoRespond()
{
  setClientAutoResponse(rjh, 1);
}

void 
setup_rjh(CLIENT rjh)
{    
    setClientId(rjh, rjh);
    setClientName(rjh, "rjh");
    setClientOutbuffer(rjh, 2);

    #ifdef AUTO_RESPOND
      rjhSetAutoRespond();
    #endif

    #ifdef FORWARD
      rjhEnableForwarding();
    #endif

    #ifdef KEYS
      setClientPrivateKey(rjh, 456);
      rjhKeyAddBob();
      rjhKeyAddChuck();
    #endif
}

void 
setup_chuck(CLIENT chuck)
{
    setClientId(chuck, chuck);
    setClientName(chuck, "chuck");
    setClientOutbuffer(chuck, 2);
    
    #ifdef KEYS
      setClientPrivateKey(chuck, 789);
      chuckKeyAddBob();
      chuckKeyAddRjh();
    #endif
}


// KEYS
void
bobKeyAddRjh()
{ 
    createClientKeyringEntry(bob);    
    setClientKeyringUser(bob, 0, 1);
    setClientKeyringPublicKey(bob, 0, 456);
    /*
    puts("bob added rjhs key");
    printf("%d\n",getClientKeyringUser(bob, 0));
    printf("%d\n",getClientKeyringPublicKey(bob, 0));
    */
}

void
bobKeyAddChuck()
{
    createClientKeyringEntry(bob);
    setClientKeyringUser(bob, 1, 3);
    setClientKeyringPublicKey(bob, 1, 789);
}


void
rjhKeyAddBob()
{
    createClientKeyringEntry(rjh);
    setClientKeyringUser(rjh, 0, 0);
    setClientKeyringPublicKey(rjh, 0, 123);
}

void
rjhKeyAddChuck()
{
    createClientKeyringEntry(rjh);
    setClientKeyringUser(rjh, 1, 2);
    setClientKeyringPublicKey(rjh, 1, 789);
}

void
chuckKeyAddBob()
{
    createClientKeyringEntry(chuck);
    setClientKeyringUser(chuck, 0, 1);
    setClientKeyringPublicKey(chuck, 0, 123);
}

void
chuckKeyAddRjh()
{
    createClientKeyringEntry(chuck);
    setClientKeyringUser(chuck, 1, 2);
    setClientKeyringPublicKey(chuck, 1, 456);
}


void
bobKeyChange()
{
  generateKeyPair(bob, 777);
}

void
rjhKeyChange()
{
  generateKeyPair(rjh, 666);
}


//actions

void
bobToRjh(char *arg)
{

  char subj[15] = "Hi, RJH [";
  strcat(subj, arg);
  strcat(subj, "]");
  char* body = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer pretium risus vitae enim porta, non eleifend lectus luctus. Fusce vitae commodo arcu. Ut tempor elit ut purus vulputate, vel porta.";
  sendEmail(bob, rjh, subj, body);
  if (!is_queue_empty()) {
    outgoing(get_queued_client(), get_queued_email());
  }
}

void
rjhToBob(char *arg)
{

  char subj[15] = "Hi, Bob [";
  strcat(subj, arg);
  strcat(subj, "]");
  char* body = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer pretium risus vitae enim porta, non eleifend lectus luctus. Fusce vitae commodo arcu. Ut tempor elit ut purus vulputate, vel porta.";
  sendEmail(rjh, bob, subj, body);
}

void
bobSetAddressBook()
{
    setClientAddressBookSize(bob, 1);
    setClientAddressBookAlias(bob, 0, rjh);
}

void setup() {
  bob = 0;
  setup_bob(bob);
  printf("bob: %d\n",bob);
  //rjh = createClient("rjh");
  rjh = 1;
  setup_rjh(rjh);
  printf("rjh: %d\n",rjh);  
  //chuck = createClient("chuck");
  chuck = 2;
  setup_chuck(chuck);
  printf("chuck: %d\n",chuck);
}

void test() {
  int i = 0;
  int j = 0;
  int n = 10000;
  char buff_i[3];
  char buff_j[3];
  while (n > (i+j)) {
    sprintf(buff_i, "%d", i);
    sprintf(buff_j, "%d", j);
    if (i > j) {
      j++;
      rjhToBob(buff_j);
    } else {
      i++;
      bobToRjh(buff_i);
    }
  }
  int m = getEmail(rjh, bob, "Hi, Bob [4999]");
  if (m >= 0) {
    printf("\n[EMAIL FOUND]\n::::\n");
    printMail(m, 1);
  }
}

int
main (void)
{ 
  setup();
  test();

}
