#include "base/Client.h"

CLIENT bob;

CLIENT rjh;

CLIENT chuck;

void setup_bob(CLIENT bob);

void setup_rjh(CLIENT rjh);

void setup_chuck(CLIENT chuck);

void before();

void bobToRjh();

void rjhToBob();

void test();

void setup();

int main();

// ADDRESS_BOOK
void bobSetAddressBook();

// AUTO_RESPOND
void rjhSetAutoRespond();

// FORWARD
void rjhEnableForwarding();

// KEYS
void bobKeyAddRjh();

void bobKeyAddChuck();

void rjhKeyAddBob();

void rjhKeyAddChuck();

void chuckKeyAddBob();

void chuckKeyAddRjh();

void bobKeyChange();

void rjhKeyChange();
