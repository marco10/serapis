#include <stdio.h>
#include "Email.h"
#include "Util.h"


void
printMail (EMAIL msg, int buffer)
{
  FILE* fp = outBuffer(buffer);

  fprintf (fp, "-----------------------\n");
  fprintf (fp, "ID:\n  %i\n", getEmailId(msg));
  fprintf (fp, "FROM:\n  %i\n", getEmailFrom(msg));
  fprintf (fp, "TO:\n  %i\n", getEmailTo(msg));
  fprintf (fp, "IS_READABLE\n  %i\n", isReadable(msg));
  fprintf (fp, ">>>\n  %s\n. . . . . .\n  %s\n<<<\n", getEmailSubject(msg), getEmailBody(msg));

  #ifdef ENCRYPT
    fprintf (fp, "ENCRYPTED\n  %d\n", isEncrypted(msg));
    fprintf (fp, "ENCRYPTION KEY\n  %d\n", getEmailEncryptionKey(msg));
  #endif

  #ifdef SIGN
    fprintf (fp, "SIGNED\n  %i\n", isSigned(msg));
    fprintf (fp, "SIGNATURE\n  %i\n", getEmailSignKey(msg));
  #endif
  fprintf (fp, "-----------------------\n");
}

int
isReadable (EMAIL msg)
{
  #ifdef ENCRYPT
    int encrypted = isEncrypted(msg);
    if (!encrypted) {
      return 1;
    } else {
      return 0;
    }
  #endif

  return 1;
}

EMAIL cloneEmail(EMAIL msg) {
    return msg;
}

EMAIL createEmail (int from, int to, char* subj, char* body) {
  EMAIL msg = initEmail();
  setEmailFrom(msg, from);
  setEmailTo(msg, to);
  setEmailSubject(msg, subj);
  setEmailBody(msg, body);
  return msg;
}

EMAIL getEmail(int from, int to, char* subj) {
  EMAIL msg = findEmail(from, to, subj);

  return msg;
}