#include <stdio.h>
#include "Client.h"
#include "Util.h"

int queue_empty = 1;

EMAIL queued_message;

CLIENT queued_client;

// ADDRESS_BOOK
void
sendToAddressBook (CLIENT client, EMAIL msg)
{

}

// AUTO_RESPOND
void
autoRespond (CLIENT client, EMAIL msg)
{
  #ifdef AUTO_RESPOND
    //puts("sending autoresponse\n");
    int sender = getEmailFrom(msg);
    setEmailTo(msg, sender);
    queue(client, msg);
  #endif
}

// FORWARD
void
forward (CLIENT client, EMAIL msg)
{  
  //puts("Forwarding message.\n");  
  //printMail(msg);
  queue(client, msg);
 
}

// outgoing emails leave the client at this point. here they are put in an outgoing queue instead.
void
mail (CLIENT client, EMAIL msg)
{
  int buffer = getClientOutbuffer(client);
  FILE* fp = outBuffer(buffer);
  fputs("mail sent!\n", fp);
  incoming (getEmailTo(msg), msg);
}

// emails to be sent are processed by this method before beeing mailed.
void
outgoing (CLIENT client, EMAIL msg)
{
  #ifdef SIGN
    sign (client, msg);
  #endif

  #ifdef ENCRYPT
    int receiver = getEmailTo(msg);
    int pubkey = findPublicKey(client, receiver);
    if (pubkey) {
        setEmailEncryptionKey(msg, pubkey);
        setEmailIsEncrypted(msg, 1);
    }
  #endif

  #ifdef ADDRESS_BOOK
    int size = getClientAddressBookSize(client);
    if (size) {
      sendToAddressBook(client, msg);
      //puts("sending to alias in address book\n");
      int receiver = getEmailTo(msg);

      //puts("sending to second receipient\n");
      int second = getClientAddressBookAlias(client, 1);
      setEmailTo(msg, getClientAddressBookAlias(client, 0));
      setEmailFrom(msg, getClientId(client));
      mail(client, msg);

      setEmailTo(msg, getClientAddressBookAlias(client, 0));
    }

  #endif

  setEmailFrom(msg, getClientId(client));
  mail(client, msg);
}

// incoming emails reach the user at this point. here they are put in a mailbox.
void
deliver (CLIENT client, EMAIL msg)
{
   int buffer = getClientOutbuffer(client);
   FILE* fp = outBuffer(buffer);
   fputs("mail received!\n", fp);
   printMail(msg, buffer);
}

void
verify (CLIENT client, EMAIL msg)
{

  if (!isReadable (msg) || !isSigned(msg))
    return;
    
  int pubkey = findPublicKey(client, getEmailFrom(msg));
  if (pubkey && isKeyPairValid(getEmailSignKey(msg), pubkey)) {
    setEmailIsSignatureVerified(msg, 1);
  }

}

// incoming emails are processed by this method before delivery.
void
incoming (CLIENT client, EMAIL msg)
{
  #ifdef DECRYPT
    int privkey = getClientPrivateKey(client);
    if (privkey) {
      
        if (isEncrypted(msg)
            && isKeyPairValid(getEmailEncryptionKey(msg), privkey))
          {
            setEmailIsEncrypted(msg, 0);
            setEmailEncryptionKey(msg, 0);
          }
    }
  #endif

  #ifdef VERIFY
    verify (client, msg);
  #endif

  deliver (client, msg);

  #ifdef AUTO_RESPOND
    if (getClientAutoResponse(client)) {
      autoRespond (client, msg);
    }
  #endif

  #ifdef FORWARD
    int fwreceiver = getClientForwardReceiver(client);
    if (fwreceiver >= 0) {
      
      setEmailTo(msg, fwreceiver);  
      forward(client, msg);
      
    }
  #endif
}

CLIENT createClient(char *name) {
    CLIENT client = initClient();
    return client;
}

void
sign (CLIENT client, EMAIL msg)
{
  int privkey = getClientPrivateKey(client);
  if (!privkey)
    return;
  setEmailIsSigned(msg, 1);
  setEmailSignKey(msg, privkey);
}

void
sendEmail (CLIENT sender, CLIENT receiver, char* subj, char* body) 
{
  EMAIL email = createEmail (sender, receiver, subj, body);
  outgoing (sender, email);
  //incoming (receiver, email);
  
}

void
queue (CLIENT client, EMAIL msg)
{
    queue_empty = 0;
    queued_message = msg;
    queued_client = client;
}

int
is_queue_empty ()
{
    return queue_empty;
}

CLIENT
get_queued_client ()
{
    return queued_client;
}

EMAIL
get_queued_email ()
{
    return queued_message;
}

int
isKeyPairValid (int publicKey, int privateKey)
{
    printf("keypair valid %d %d", publicKey, privateKey);
  if (!publicKey || !privateKey)
    return 0;
  return privateKey == publicKey;
}

void
generateKeyPair (CLIENT client, int seed)
{
    setClientPrivateKey(client, seed);
}
