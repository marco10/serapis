#define CLIENT  int
#define EMAIL int

CLIENT bob;

CLIENT rjh;

CLIENT chuck;

void setup_bob(CLIENT bob);

void setup_rjh(CLIENT rjh);

void setup_chuck(CLIENT chuck);

void before();

void bobToRjh();

void rjhToBob();

void test();

void setup();

int main();

// ADDRESS_BOOK
void bobSetAddressBook();

// AUTO_RESPOND
void rjhSetAutoRespond();

// FORWARD
void rjhEnableForwarding();

// KEYS
void bobKeyAddRjh();

void bobKeyAddChuck();

void rjhKeyAddBob();

void rjhKeyAddChuck();

void chuckKeyAddBob();

void chuckKeyAddRjh();

void bobKeyChange();

void rjhKeyChange();


// Client
void queue (CLIENT client, EMAIL msg);

int is_queue_empty ();
CLIENT get_queued_client ();
EMAIL get_queued_email ();

void mail (CLIENT client, EMAIL msg);
void outgoing (CLIENT client, EMAIL msg);
void deliver (CLIENT client, EMAIL msg);
void incoming (CLIENT client, EMAIL msg);
CLIENT createClient(char *name);

void sendEmail (CLIENT sender, CLIENT receiver, char* subj, char* body);

// ADDRESS_BOOK
void sendToAddressBook (CLIENT client, EMAIL msg);

// AUTO_RESPOND
void autoRespond (int client, int msg);

// FORWARD
void forward (int client, int msg);

// KEYS
int isKeyPairValid (int publicKey, int privateKey);

void generateKeyPair (CLIENT client, int seed);

// SIGN
void sign (CLIENT client, EMAIL msg);

// VERIFY
void verify (CLIENT client, EMAIL msg);

/* --- */

int initClient();
char* getClientName(int handle);
void setClientName(int handle, char* value);
int getClientOutbuffer(int handle);
void setClientOutbuffer(int handle, int value);
int getClientAddressBookSize(int handle);
void setClientAddressBookSize(int handle, int value);
int createClientAddressBookEntry(int handle);
int getClientAddressBookAlias(int handle, int index);
void setClientAddressBookAlias(int handle, int index, int value);
int getClientAddressBookAddress(int handle, int index);
void setClientAddressBookAddress(int handle, int index, int value);

int getClientAutoResponse(int handle);
void setClientAutoResponse(int handle, int value);
int getClientPrivateKey(int handle);
void setClientPrivateKey(int handle, int value);
int getClientKeyringSize(int handle);
int createClientKeyringEntry(int handle);
int getClientKeyringUser(int handle, int index);
void setClientKeyringUser(int handle, int index, int value);
int getClientKeyringPublicKey(int handle, int index);
void setClientKeyringPublicKey(int handle, int index, int value);
int getClientForwardReceiver(int handle);
void setClientForwardReceiver(int handle, int value);
int getClientId(int handle);
void setClientId(int handle, int value);
int findPublicKey(int handle, int userid);
int findClientAddressBookAlias(int handle, int userid);


// Email
void printMail (EMAIL msg, int buffer);

int isReadable (EMAIL msg);

EMAIL createEmail (int from, int to, char* subj, char* body);

EMAIL cloneEmail(EMAIL msg);

// custom
EMAIL getEmail(int from, int to, char* subj);

/* --- */
int initEmail();
int getEmailId(int handle);
void setEmailId(int handle, int value);
int getEmailFrom(int handle);
void setEmailFrom(int handle, int value);
int getEmailTo(int handle);
void setEmailTo(int handle, int value);
char* getEmailSubject(int handle);
void setEmailSubject(int handle, char* value);
char* getEmailBody(int handle);
void setEmailBody(int handle, char* value);
int isEncrypted(int handle);
void setEmailIsEncrypted(int handle, int value);
int getEmailEncryptionKey(int handle);
void setEmailEncryptionKey(int handle, int value);
int isSigned(int handle);
void setEmailIsSigned(int handle, int value);
int getEmailSignKey(int handle);
void setEmailSignKey(int handle, int value);
int isVerified(int handle);
void setEmailIsSignatureVerified(int handle, int value);

// custom
int findEmail(int from, int to, char* subj);


// Util
int prompt(char* msg);

FILE* outBuffer(int b);
