#include <stdlib.h>
#include <string.h>
#include <stdio.h>


//Floor.c

int calls[5];

int personOnFloor[6][5];

void initFloors() {
	calls[0] = 0;
	calls[1] = 0;
	calls[2] = 0;
	calls[3] = 0;
	calls[4] = 0;
	personOnFloor[0][0] = 0;
	personOnFloor[0][1] = 0;
	personOnFloor[0][2] = 0;
	personOnFloor[0][3] = 0;
	personOnFloor[0][4] = 0;
	personOnFloor[1][0] = 0;
	personOnFloor[1][1] = 0;
	personOnFloor[1][2] = 0;
	personOnFloor[1][3] = 0;
	personOnFloor[1][4] = 0;
	personOnFloor[2][0] = 0;
	personOnFloor[2][1] = 0;
	personOnFloor[2][2] = 0;
	personOnFloor[2][3] = 0;
	personOnFloor[2][4] = 0;
	personOnFloor[3][0] = 0;
	personOnFloor[3][1] = 0;
	personOnFloor[3][2] = 0;
	personOnFloor[3][3] = 0;
	personOnFloor[3][4] = 0;
	personOnFloor[4][0] = 0;
	personOnFloor[4][1] = 0;
	personOnFloor[4][2] = 0;
	personOnFloor[4][3] = 0;
	personOnFloor[4][4] = 0;
	personOnFloor[5][0] = 0;
	personOnFloor[5][1] = 0;
	personOnFloor[5][2] = 0;
	personOnFloor[5][3] = 0;
	personOnFloor[5][4] = 0;
}

int isFloorCalling(int floorID) {
	int res = 0;
	if (floorID >= 0 && floorID < 5) {
		res = calls[floorID];
	}
	return res;
}

void resetCallOnFloor(int floorID) {
	if (floorID >= 0 && floorID < 5) {
		calls[floorID] = 0;
	}

}

void callOnFloor(int floorID) {
	if (floorID >= 0 && floorID < 5) {
		calls[floorID] = 1;
	}
}

int isPersonOnFloor(int person, int floor) {
	int res =0;
	if (person >= 0 && person < 6) {
		int *floors = personOnFloor[person];
		if (floor >= 0 && floor < 5) {
			res = floors[floor];
		}
	}
	return res;
}

void initPersonOnFloor(int person, int floor) {
	if (person >= 0 && person < 6) {
		int *floors = personOnFloor[person];
		if (floor >= 0 && floor < 5) {
			floors[floor] = 1;
		}
	}
	callOnFloor(floor);
}

void removePersonFromFloor(int person, int floor) {
	if (person >= 0 && person < 6) {
		int *floors = personOnFloor[person];
		if (floor >= 0 && floor < 5) {
			floors[floor] = 0;
		}
	}
	resetCallOnFloor(floor);
}
int isTopFloor(int floorID) {
	return floorID == 4;
}


//Person.c

/* Persons:
 * bob : 0
 * alice : 1
 * angelina : 2
 * chuck : 3
 * monica : 4
 * bigMac : 5
 *
 */

int weights[6] = {40, 40, 40, 40, 30, 150};

int getWeight(int person) {
	int res = 0;
	if (person >= 0 && person < 6) {
		res = weights[person];
	}
	return res;
}

int origins[6] = {4, 3, 2, 1, 0, 1};

int getOrigin(int person) {
	int res = 0;
	if (person >= 0 && person < 6) {
		res = origins[person];
	}
	return res;
}

int destinations[6] = {0, 0, 1, 3, 1, 3};

int getDestination(int person) {
	int res = 0;
	if (person >= 0 && person < 6) {
		res = destinations[person];
	}
	return res;
}

// Elevator.c
void __feature_EXECUTIVE_FLOOR;
	int executiveFloor = 4;
void __endfeature_;

void __feature_WEIGHT;
	int weight = 0;
	int maximumWeight = 100;
void __endfeature_;

void __feature_OVERLOADED;
	int blocked = 0;
void __endfeature_;

int currentHeading = 1; // 1 = up, 0 = down
int currentFloorID = 0;

// is the person 0-5 inside the elevator?
int persons[6];
/*
int persons_0;
int persons_1;
int persons_2;
int persons_3;
int persons_4;
int persons_5;
*/
int doorState = 1; // 1 = open, 0 = closed

int floorButtons[5];
/*
int floorButtons_0;
int floorButtons_1;
int floorButtons_2;
int floorButtons_3;
int floorButtons_4; // 5 floors
*/

/**
* Init the elevator, putting him on the last floor and heading down.
*/
void initTopDown() {
	currentFloorID = 4;
	currentHeading = 0;
	floorButtons[0] = 0;
	floorButtons[1] = 0;
	floorButtons[2] = 0;
	floorButtons[3] = 0;
	floorButtons[4] = 0;
	persons[0] = 0;
	persons[1] = 0;
	persons[2] = 0;
	persons[3] = 0;
	persons[4] = 0;
	persons[5] = 0;
	initFloors();
}

/**
* Init the elevator, putting him on the first floor and heading up.
*/
void initBottomUp() {
	currentFloorID = 0;
	currentHeading = 1;
	floorButtons[0] = 0;
	floorButtons[1] = 0;
	floorButtons[2] = 0;
	floorButtons[3] = 0;
	floorButtons[4] = 0;
	persons[0] = 0;
	persons[1] = 0;
	persons[2] = 0;
	persons[3] = 0;
	persons[4] = 0;
	persons[5] = 0;
	initFloors();
}

int isBlocked() {
	int b = 0;

	void __feature_OVERLOADED;
		b = blocked;
	void __endfeature_;

	return b;
}

/**
* Puts person with id `p` inside the elvator.
*/
void enterElevator(int p) {
	if (p >= 0 && p < 6) {
		persons[p] = 1;
	}
	void __feature_WEIGHT;
		weight = weight + getWeight(p);
	void __endfeature_;
}

/**
* Is the elevator empty?
* Returns 1 if it is, and 0 if not.
*/
int isEmpty() {
	int res = 1;
	int i = 0;
	for (i = 0; i < 6; i++) {
		if (persons[i] == 1) {
			res = 0;
		}
	}
	return res;
}

/**
* Removes person with id `p` from the elvator.
*/
void leaveElevator(int p) {
	if (p >= 0 && p < 6) {
		persons[p] = 0;
	}

	void __feature_EMPTY;
		if (isEmpty()) {
			floorButtons[0] = 0;
			floorButtons[1] = 0;
			floorButtons[2] = 0;
			floorButtons[3] = 0;
			floorButtons[4] = 0;
		}
	void __endfeature_;

	void __feature_WEIGHT;
		weight = weight - getWeight(p);
	void __endfeature_;
	// "p left the elevator"
}

/**
* Presses the button for the floor `floorID`.
*/
void pressInLiftFloorButton(int floorID) {
	if (floorID >= 0 && floorID < 5) {
		floorButtons[floorID] = 1;
	}
}

/**
* After reaching floor `floorID`, resets the button.
*/
void resetFloorButton(int floorID) {
	if (floorID >= 0 && floorID < 5) {
		floorButtons[floorID] = 0;
	}
}

/**
* Returns the ID of the floor on which the elevator is.
*/
int getCurrentFloorID() {
	return currentFloorID;
}

/**
* Checks if the doors are open.
*/
int areDoorsOpen() {
	return doorState;
}

/**
* Checks if the button for `floorID` is pressed.
*/
int buttonForFloorIsPressed(int floorID) {
	int res = 0;
	if (floorID >= 0 && floorID < 5) {
		res = floorButtons[floorID];
	}
	return res;
}

/**
* Is the elevator heading up or down?
* Returns 1 for up, and 0 for down.
*/
int getCurrentHeading() {
	return currentHeading;
}

/**
* Is there any floor requesting a stop?
* Returns 0 if not, and 1 if it is.
*/
int anyStopRequested () {
	int res = 0;
	int i, c;
	for (i = 0; i < 5; i++) {
		c = isFloorCalling(i);
		if (floorButtons[i] == 1 || c == 1){
			res = 1;
		}
	}
	return res;
}

/**
* Checks if the elevator is idle, i.e. no stop was requested.
*/
int isIdle() {
	return (anyStopRequested() == 0);
}

/**
* Checks if any of the elevator buttons is pressed.
*/
int isAnyLiftButtonPressed() {
	int res = 0;
	int i;
	for (i = 0; i < 5; i++) {
		if (floorButtons[i] == 1){
			res = 1;
		}
	}
	return res;
}

int isExecutiveFloor(int floorID) {
	int res = 0;
	void __feature_EXECUTIVE_FLOOR;
		res = (executiveFloor == floorID);
	void __endfeature_;
	return res;
}

int isExecutiveFloorCalling() {
	int res = 0;
	void __feature_EXECUTIVE_FLOOR;
		res = isFloorCalling(executiveFloor);
	void __endfeature_;
	return res;
}

/**
* Considering the elevator's direction `dir`, checks if there 
* are any requests to stop in floors that will appear in that 
* direction.
* Returns 1 for true, 0 for false.
*/
int stopRequestedInDirection (int dir, int respectFloorCalls, int respectInLiftCalls) {
	int res = 0;
	int rFloorCalls = respectFloorCalls;
	void __feature_TWO_THIRDS_FULL;
		int overload = weight > maximumWeight*2/3;
		int buttonPressed = isAnyLiftButtonPressed();
		if (overload && buttonPressed) {
			rFloorCalls = 0;
		}
	void __endfeature_;

	void __feature_EXECUTIVE_FLOOR;
		if (isExecutiveFloorCalling()) {
			return ((getCurrentFloorID() < executiveFloor) == (dir == 1));
		}
	void __endfeature_;

	if (dir == 1) {
		if (isTopFloor(currentFloorID)) {return 0;}
		// Good Implementation:
		int i = 0;
		for (i = currentFloorID+1; i < 5; i++) {
			int call = isFloorCalling(i);
			if (rFloorCalls && call){
				res = 1;
			}
			int button = floorButtons[i];
			if (respectInLiftCalls && button) {
				res = 1;
			}
		}
		
	} else {
		if (currentFloorID == 0) {return 0;}
		// Good Implementation:
		int j = 0;
		for (j = currentFloorID-1; j >= 0; j--) {
			int call = isFloorCalling(j);
			if (rFloorCalls && call){ 
				res = 1; 
			}
			int button = floorButtons[j];
			if (respectInLiftCalls && button){ 
				res = 1; 
			}
		}
		
	}
	return res;
}

/**
* .
*/
void continueInDirection(int dir) {
	currentHeading = dir;
	if (currentHeading == 1) {
		int top = isTopFloor(currentFloorID);
		if (top) {
			//System.out.println("Reversing at Top Floor");
			currentHeading = 0;
		}
	} else {
		if (currentFloorID == 0) {
			//System.out.println("Reversing at Basement Floor");
			currentHeading = 1;
		}
	}
	if (currentHeading == 1) {
		currentFloorID = currentFloorID + 1;
	} else {
		currentFloorID = currentFloorID - 1;
	}
}

/**
* Is there a request to stop at the current floor?
*/
int stopRequestedAtCurrentFloor() {
	int res = 0;
	void __feature_TWO_THIRDS_FULL;
		if (weight > maximumWeight*2/3) {
			int isPressed = buttonForFloorIsPressed(getCurrentFloorID());
			res = isPressed == 1;
			return res;
		}
	void __endfeature_;

	void __feature_EXECUTIVE_FLOOR;
		int execCalling = isExecutiveFloorCalling();
		int currFloor   = getCurrentFloorID();
		if (execCalling && ! (executiveFloor == currFloor)) {
			res = 0;
			return res;
		}
	void __endfeature_;

	int floorCalling = isFloorCalling(currentFloorID);
	int buttonPressed = buttonForFloorIsPressed(currentFloorID);
	if (floorCalling) {
		res = 1;
	} else if (buttonPressed) {
		res = 1;
	} else {
		res = 0;
	}

	return res;
}

int getReverseHeading(int ofHeading) {
	if (ofHeading==0) {
		return 1;
	} else {
		return 0;
	}
}

/**
* .
*/
void processWaitingOnFloor(int floorID) {
	if (isPersonOnFloor(0,floorID)) {
		removePersonFromFloor(0, floorID);
		pressInLiftFloorButton(getDestination(0));
		enterElevator(0);
	}
	if (isPersonOnFloor(1,floorID)) {
		removePersonFromFloor(1, floorID);
		pressInLiftFloorButton(getDestination(1));
		enterElevator(1);
	}
	if (isPersonOnFloor(2,floorID)) {
		removePersonFromFloor(2, floorID);
		pressInLiftFloorButton(getDestination(2));
		enterElevator(2);
	}
	if (isPersonOnFloor(3,floorID)) {
		removePersonFromFloor(3, floorID);
		pressInLiftFloorButton(getDestination(3));
		enterElevator(3);
	}
	if (isPersonOnFloor(4,floorID)) {
		removePersonFromFloor(4, floorID);
		pressInLiftFloorButton(getDestination(4));
		enterElevator(4);
	}
	if (isPersonOnFloor(5,floorID)) {
		removePersonFromFloor(5, floorID);
		pressInLiftFloorButton(getDestination(5));
		enterElevator(5);
	}
	resetCallOnFloor(floorID);
}

// pre: elevator arrived at the current floor, next actions to be done
void timeShift() {
	//System.out.println("--");
	void __feature_OVERLOADED;
		blocked = 0;
		if (areDoorsOpen() && weight > maximumWeight) {
			blocked = 1;
			return;
		}
	void __endfeature_;

	int stopRequested = stopRequestedAtCurrentFloor();
	if (stopRequested) {
		//System.out.println("Arriving at " +  currentFloorID + ", Doors opening");
		doorState = 1;
		// iterate over a copy of the original list, avoids concurrent modification exception
		int i;
		for (i = 0; i < 6; i++) {
			int personDest = getDestination(i);
			if (persons[i] && personDest == currentFloorID) {
				leaveElevator(i);
			}
		}
		processWaitingOnFloor(currentFloorID);
		resetFloorButton(currentFloorID);
	} else {
		if (doorState == 1)  {
			doorState = 0;
			//System.out.println("Doors Closing");
		}
		int stopRequestedCurrentDir = stopRequestedInDirection(currentHeading, 1, 1);
		int stopRequestedReverseDir = stopRequestedInDirection(getReverseHeading(currentHeading), 1, 1);
		if (stopRequestedCurrentDir) {
			//System.out.println("Arriving at " + currentFloorID + ", continuing");
			// continue
			continueInDirection(currentHeading);
		} else if (stopRequestedReverseDir) {
			//System.out.println("Arriving at " + currentFloorID + ", reversing direction because of call in other direction");
			// revert direction
			continueInDirection(getReverseHeading(currentHeading));
		} else {
			//idle
			//System.out.println("Arriving at " + currentFloorID + ", idle->continuing");
			continueInDirection(currentHeading);
		}
	}
}
void printState() {
	printf("Elevator ");

	void __feature_OVERLOADED;
		if (isBlocked()) {printf("Blocked ");}
	void __endfeature_;

	if (doorState) {printf("[_]");}
	else printf("[] ");
	printf(" at ");
	printf("%i",currentFloorID);
	printf(" heading ");
	if (currentHeading)	{printf("up");}
	else printf("down");

	printf(" IL_p:");
	int i;
	for (i = 0; i < 5; i++) {
		if (floorButtons[i]) {
			printf(" %i", i);
		}
	}

	printf(" F_p:");
	int f;
	for (f = 0; f < 5; f++) {
		int floorCalling = isFloorCalling(f);
		if (floorCalling) {
			printf(" %i", f);
		}
	}
	
	printf(" People:");
	int p;
	for (p = 0; p < 6; p++) {
		if (persons[p]) {
			printf(" %i", p);
		}
	}
	void __feature_WEIGHT ;
		printf(" (%d Kg)", weight);
	void __endfeature_;
	//
	printf("\n");
}

int existInLiftCallsInDirection(int d) {
	int res = 0;
	 if (d == 1) {
	 	 int i = 0;
		 for (i =  currentFloorID + 1; i < 5; i++) {
		 	int pressed = buttonForFloorIsPressed(i);
		 	if (pressed) {
		 		res = 1;
		 	}
		 }
	 } else if (d == 0) {
		int i = 0;
		for (i =  currentFloorID - 1; i >= 0; i--){
			for (i =  currentFloorID + 1; i < 5; i++) {
				int pressed = buttonForFloorIsPressed(i);
			 	if (pressed) {
					res = 1;
				}
			 }
		 }
	 }
	 return res;		 
}



int cleanupTimeShifts = 12;


void bobCall()      { initPersonOnFloor(0, getOrigin(0)); }
void aliceCall()    { initPersonOnFloor(1, getOrigin(1)); }
void angelinaCall() { initPersonOnFloor(2, getOrigin(2)); }
void chuckCall()    { initPersonOnFloor(3, getOrigin(3)); }
void monicaCall()   { initPersonOnFloor(4, getOrigin(4)); }
void bigMacCall()   { initPersonOnFloor(5, getOrigin(5)); }
void threeTS()      { timeShift(); timeShift(); timeShift(); }
void cleanup() {
	// minimum 1 timeShift(), maximum cleanupTimeShifts.
	// (1 is needed for certain scnearios)
	timeShift();
	int i;
	int is_blocked = isBlocked();
	for (i = 0; (i < (cleanupTimeShifts - 1)) && (is_blocked != 1); i++){
		if (isIdle()) {
			return;
		}
		else{
			timeShift();
		}
		is_blocked = isBlocked();
	}
}

void runTest_Simple() {
	
	initBottomUp();

	chuckCall();
	angelinaCall();
	timeShift();
	monicaCall();
	timeShift();
	monicaCall();
	bobCall();
	chuckCall();
	chuckCall();
	timeShift();
	timeShift();
	timeShift();
	aliceCall();
	timeShift();
	timeShift();
	timeShift();
	monicaCall();
	aliceCall();
	timeShift();
	timeShift();
	bobCall();
	chuckCall();
	bobCall();
	angelinaCall();
	timeShift();
	timeShift();
	timeShift();
	timeShift();
	timeShift();
	timeShift();
	
	printState();
}

void setup() {
}

// this function is the hook for the specifications
// the generated scenarios do not work if the generated test()-function is called outside of this method!
void runTest() {
	runTest_Simple();
	//randomSequenceOfActions();
	//test();
}

int main (void) {	
	setup();
	runTest();
	return 0;
}
