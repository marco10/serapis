int getWeight(int person);
int getOrigin(int person);
int getDestination(int person);
void enterElevator(int person);
int isFloorCalling(int floorID);
void resetCallOnFloor(int floorID);
void callOnFloor(int floorID);
int isPersonOnFloor(int person, int floor);
void initPersonOnFloor(int person, int floor);
void removePersonFromFloor(int person, int floor);
int isTopFloor(int floorID);
void processWaitingPersons(int floorID);
void initFloors();

void timeShift();
int isBlocked();
void printState();
int isEmpty();
int isAnyLiftButtonPressed();
int buttonForFloorIsPressed(int floorID);

void initTopDown();
void initBottomUp();

int areDoorsOpen();
int getCurrentFloorID();
int isIdle();

void __feature_EXECUTIVE_FLOOR;
 int executiveFloor = 4;
 int isExecutiveFloorCalling();
 int isExecutiveFloor(int floorID);
void __endfeature_;

void __feature_WEIGHT;
 int weight = 0;
 int maximumWeight = 100;
void __endfeature_;

void __feature_OVERLOADED;
 int blocked = 0;
void __endfeature_;
