int getWeight(int person);
int getOrigin(int person);
int getDestination(int person);
void enterElevator(int person);
int isFloorCalling(int floorID);
void resetCallOnFloor(int floorID);
void callOnFloor(int floorID);
int isPersonOnFloor(int person, int floor);
void initPersonOnFloor(int person, int floor);
void removePersonFromFloor(int person, int floor);
int isTopFloor(int floorID);
void processWaitingPersons(int floorID);
void initFloors();

void timeShift();
int isBlocked();
void printState();
int isEmpty();
int isAnyLiftButtonPressed();
int buttonForFloorIsPressed(int floorID);

void initTopDown();
void initBottomUp();

int areDoorsOpen();
int getCurrentFloorID();
int isIdle();

void __feature_EXECUTIVE_FLOOR;
 int executiveFloor = 4;
 int isExecutiveFloorCalling();
 int isExecutiveFloor(int floorID);
void __endfeature_;

void __feature_WEIGHT;
 int weight = 0;
 int maximumWeight = 100;
void __endfeature_;

void __feature_OVERLOADED;
 int blocked = 0;
void __endfeature_;
int getWeight(int person);
int getOrigin(int person);
int getDestination(int person);
void enterElevator(int person);
int isFloorCalling(int floorID);
void resetCallOnFloor(int floorID);
void callOnFloor(int floorID);
int isPersonOnFloor(int person, int floor);
void initPersonOnFloor(int person, int floor);
void removePersonFromFloor(int person, int floor);
int isTopFloor(int floorID);
void processWaitingPersons(int floorID);
void initFloors();



int cleanupTimeShifts = 12;


int get_nondet() {
    int nd = 1;
    return nd;
}

int get_nondetMinMax07() {
    int nd = 1;

    if (nd==0) {
 return 0;
    } else if (nd==1) {
 return 1;
    } else if (nd==2) {
 return 2;
    } else if (nd==3) {
 return 3;
    } else if (nd==4) {
 return 4;
    } else if (nd==5) {
 return 5;
    } else if (nd==6) {
 return 6;
    } else if (nd==7) {
 return 7;
    } else {
     exit(0);
    }
}

void bobCall() { initPersonOnFloor(0, getOrigin(0)); }
void aliceCall() { initPersonOnFloor(1, getOrigin(1)); }
void angelinaCall() { initPersonOnFloor(2, getOrigin(2)); }
void chuckCall() { initPersonOnFloor(3, getOrigin(3)); }
void monicaCall() { initPersonOnFloor(4, getOrigin(4)); }
void bigMacCall() { initPersonOnFloor(5, getOrigin(5)); }
void threeTS() { timeShift(); timeShift(); timeShift(); }
void cleanup() {


 timeShift();
 int i;
 for (i = 0; i < cleanupTimeShifts-1 && isBlocked()!=1; i++) {
  if (isIdle())
   return;
  else
   timeShift();

 }
}

void randomSequenceOfActions() {
  int maxLength = 4;
  if (get_nondet()) {

   initTopDown();


  } else {
   initBottomUp();



  }
  int counter = 0;
  while (counter < maxLength) {
   counter++;
   int action = get_nondetMinMax07();





   if (action < 6) {
    int origin = getOrigin(action);
    initPersonOnFloor(action, origin);
   } else if (action == 6) {
    timeShift();
   } else if (action == 7) {

    timeShift();
    timeShift();
    timeShift();

   }


   if (isBlocked()) {
    return;
   }
  }
  cleanup();
 }


void runTest_Simple() {
 bigMacCall();
 angelinaCall();
 printState();
 cleanup();
}
void setup() {
}


void runTest() {
 runTest_Simple();


}

int main (void) {

 setup();
 runTest();
 return 0;

}
