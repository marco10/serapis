/*
 * Elevator.h
 *
 *  Created on: 28.02.2011
 *      Author: rhein
 */
#include "Person.h"
#include "Floor.h"

void timeShift();
int isBlocked();
void printState();
int isEmpty();
int isAnyLiftButtonPressed();
int buttonForFloorIsPressed(int floorID);

void initTopDown();
void initBottomUp();

int areDoorsOpen();
int getCurrentFloorID();
int isIdle();

#ifdef EXECUTIVE_FLOOR
	int executiveFloor = 4;
	int isExecutiveFloorCalling();
	int isExecutiveFloor(int floorID);
#endif

#ifdef WEIGHT
	int weight = 0;
	int maximumWeight = 100;
#endif

#ifdef OVERLOADED
	int blocked = 0;
#endif
