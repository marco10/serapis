#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "Main.h"

#define N_ACTIONS 1000000

int cleanupTimeShifts  = 4;


// -> MinePump.c <-

int pumpRunning = 0;
int systemActive = 1;

void timeShift() {
	if (pumpRunning)
		lowerWaterLevel();
	if (systemActive)
		processEnvironment();
}

void processEnvironment() {
	#ifdef HIGH_WATER_SENSOR
		int highWater = isHighWaterLevel();
		if (!pumpRunning && highWater) {
			activatePump();
			return;
		}
	#endif

	#ifdef LOW_WATER_SENSOR
		int lowWater = isLowWaterLevel();
		if (!pumpRunning && lowWater) {
			deactivatePump();
			return;
		}
	#endif

	#ifdef METHANE_ALARM
		int methaneAlarm = isMethaneAlarm();
		if (!pumpRunning && methaneAlarm) {
			deactivatePump();
			return;
		}
	#endif
}

void activatePump() {
	#ifdef METHANE_ALARM
		int methaneAlarm = isMethaneAlarm();
		if (methaneAlarm) {
			return;
		}
	#endif
	pumpRunning = 1;
}

void deactivatePump() {
	pumpRunning = 0;
}

int isMethaneAlarm() {
	return isMethaneLevelCritical();
}

int isPumpRunning() {
	return pumpRunning;
}

void printPump() {
	printf("Pump(System:");
	if (systemActive)
		printf("On");
	else
		printf("Off");
	printf(",Pump:");
	if (pumpRunning)
		printf("On");
	else
		printf("Off");
	printf(") ");
	printEnvironment();
	printf("\n");
}

// HIGH_WATER_SENSOR
int isHighWaterLevel() {
	return ! isHighWaterSensorDry();
}

// LOW_WATER_SENSOR
int isLowWaterLevel() {
	return ! isLowWaterSensorDry();
}

// START_COMMAND
void startSystem() {
	#ifdef START_COMMAND
		systemActive = 1;
	#endif
}

// STOP_SYSTEM
void stopSystem() {
	#ifdef STOP_SYSTEM
		if (pumpRunning) {
			deactivatePump();
		}
		systemActive = 0;
	#endif
}



// -> Environment.c <-

/*
 * Waterlevels:
 * 0: below the low sensor
 * 1: between low and high sensors
 * 2: above high sensor
 */
 int waterLevel = 1;

 int methaneLevelCritical = 0;

void lowerWaterLevel() {
	if (waterLevel > 0) {
		waterLevel = waterLevel-1;
	}
}

void waterRise() {
	if (waterLevel < 2) {
		waterLevel = waterLevel+1;
	}
}

void changeMethaneLevel() {
	if (methaneLevelCritical) {
		methaneLevelCritical = 0;
	} else {
		methaneLevelCritical = 1;
	}
}

int isMethaneLevelCritical() {
	return methaneLevelCritical;
}


void printEnvironment() {
	printf("Env(Water:%i", waterLevel);
	printf(",Meth:");
	if (methaneLevelCritical)
		printf("CRIT");
	else
		printf("OK");
	printf(")");
}

int getWaterLevel() {
	return waterLevel;
}


// HIGH_WATER_SENSOR
int isHighWaterSensorDry() {
	// cpachecker bug
	//return waterLevel < 2;
	if (waterLevel < 2) {
		return 1;
	} else {
		return 0;
	}
}

// LOW_WATER_SENSOR
int isLowWaterSensorDry() {
	return waterLevel == 0;
}

// -> Test.c <-

void cleanup() {
	// minimum 1 timeShift(), maximum cleanupTimeShifts.
	// (1 is needed for certain scenarios)
	timeShift();
	int i;
	for (i = 0; i < cleanupTimeShifts - 1; i++) {
		timeShift();
	}
}

void doAction() {
	startSystem();

	waterRise();
	waterRise();
	timeShift();
	waterRise();
	timeShift();
	printPump();
	changeMethaneLevel();
	lowerWaterLevel();
	timeShift();
	waterRise();
	timeShift();
	printPump();
	changeMethaneLevel();
	lowerWaterLevel();
	timeShift();
	timeShift();
	lowerWaterLevel();
	printPump();
	waterRise();
	timeShift();

	stopSystem();
	printPump();

}

void test() {

	for (int i = 0; i < N_ACTIONS; i++) {
		doAction(i);
	}

	printPump();
	cleanup();
}

void setup() {

}

// this function is the hook for the specifications
// the generated scenarios do not work if the generated test()-function is called outside of this method!
void runTest() {
	//runTest_Simple();
	//randomSequenceOfActions();
	test();
}
int
main (void)
{ 

  setup();
  runTest();

  return 0;

}

