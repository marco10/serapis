#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "Test.h"
#include "base/Environment.h"
#include "base/MinePump.h"

#define N_ACTIONS 1000000

int cleanupTimeShifts  = 4;

void cleanup() {
	// minimum 1 timeShift(), maximum cleanupTimeShifts.
	// (1 is needed for certain scenarios)
	timeShift();
	int i;
	for (i = 0; i < cleanupTimeShifts - 1; i++) {
		timeShift();
	}
}

void doAction() {
	startSystem();

	waterRise();
	waterRise();
	timeShift();
	waterRise();
	timeShift();
	printPump();
	changeMethaneLevel();
	lowerWaterLevel();
	timeShift();
	waterRise();
	timeShift();
	printPump();
	changeMethaneLevel();
	lowerWaterLevel();
	timeShift();
	timeShift();
	lowerWaterLevel();
	printPump();
	waterRise();
	timeShift();

	stopSystem();
	printPump();

}

void test() {
	/*
	timeShift();printPump();
	timeShift();printPump();
	timeShift();printPump();
	waterRise();printPump();
	timeShift();printPump();
	changeMethaneLevel();printPump();
	timeShift();
	*/
	for (int i = 0; i < N_ACTIONS; i++) {
		doAction();
	}

	printPump();
	cleanup();
}

void setup() {

}

// this function is the hook for the specifications
// the generated scenarios do not work if the generated test()-function is called outside of this method!
void runTest() {
	//runTest_Simple();
	//randomSequenceOfActions();
	test();
}
int
main (void)
{ 

  setup();
  runTest();

  return 0;

}
