

void timeShift();
void activatePump();
void deactivatePump();
int isPumpRunning();
void processEnvironment();
int isMethaneAlarm();

void printPump();

// HIGH_WATER_SENSOR
int isHighWaterLevel();

// LOW_WATER_SENSOR
int isLowWaterLevel();

// START_COMMAND
void startSystem();

// STOP_COMMAND
void stopSystem();



void lowerWaterLevel() ;
void waterRise();
void changeMethaneLevel();
int isMethaneLevelCritical();
int getWaterLevel();

void printEnvironment();

// HIGH_WATER_SENSOR
int isHighWaterSensorDry();

// LOW_WATER_SENSOR
int isLowWaterSensorDry();