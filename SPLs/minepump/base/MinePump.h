
void timeShift();
void activatePump();
void deactivatePump();
int isPumpRunning();
void processEnvironment();

void printPump();

// HIGH_WATER_SENSOR
int isHighWaterLevel();

// LOW_WATER_SENSOR
int isLowWaterLevel();

// START_COMMAND
void startSystem();

// STOP_COMMAND
void stopSystem();
