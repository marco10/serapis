mkdir vectorizer-log
gcc -O3 -fopt-info-vec-optimized -c src\0A-SanDiego\integralImage2D2D.c 2> vectorizer-log\0A-integralImage2D2D.txt
gcc -O3 -fopt-info-vec-optimized -c src\0A-SanDiego\finalSAD.c 2> vectorizer-log\0A-finalSAD.txt
gcc -O3 -fopt-info-vec-optimized -c src\0A-SanDiego\computeSAD.c 2> vectorizer-log\0A-computeSAD.txt
gcc -O3 -fopt-info-vec-optimized -c src\0B-SanDiego-Double\integralImage2D2D.c 2> vectorizer-log\0B-integralImage2D2D.txt
gcc -O3 -fopt-info-vec-optimized -c src\0B-SanDiego-Double\finalSAD.c 2> vectorizer-log\0B-finalSAD.txt
gcc -O3 -fopt-info-vec-optimized -c src\0B-SanDiego-Double\computeSAD.c 2> vectorizer-log\0B-computeSAD.txt
gcc -O3 -fopt-info-vec-optimized -c src\4-Modified1\getDisparity.c 2> vectorizer-log\4-getDisparity.txt
gcc -O3 -fopt-info-vec-optimized -c src\5-Modified2\getDisparity.c 2> vectorizer-log\5-getDisparity.txt
gcc -O3 -fopt-info-vec-optimized -c src\6-ModifiedC-NoUnnecessaryIntermediates\getDisparity.c 2> vectorizer-log\6-getDisparity.txt
gcc -O3 -fopt-info-vec-optimized -c src\7-ModifiedC-ManualInterchange\getDisparity.c 2> vectorizer-log\7-getDisparity.txt