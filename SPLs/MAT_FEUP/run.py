import os
import subprocess
import shutil

build_path = 'build'
result_path = 'result'
iterations = 30

if os.path.exists(result_path):
    shutil.rmtree(result_path)

os.makedirs(result_path)

output_file_name = result_path + os.sep + 'disparity.csv'
with open(output_file_name, 'wb+') as output_file:
    output_file.write('sep=,\n'.encode('utf-8'))
    for flag_name in next(os.walk(build_path))[1]:
        flag_path_name = build_path + os.sep + flag_name
        for version in next(os.walk(flag_path_name))[1]:
            if version.startswith('_'):
                continue
            executable_name = flag_path_name + os.sep + version + os.sep + 'disparity.exe'
            print('Running ' + executable_name)
            output_file.write(executable_name.encode('utf-8'))
            output_file.write(','.encode('utf-8'))
            for i in range(iterations):
                if i != 0:
                    output_file.write(','.encode('utf-8'))
                process = subprocess.Popen([executable_name], stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
                output = process.communicate()[0]
                output_file.write(output)
            output_file.write('\n'.encode('utf-8'))