#!/usr/bin/python3

import os
import shutil
import subprocess

src_path = 'src'
dest_path = 'build'
make = 'mingw32-make'

sep = os.sep

versions = next(os.walk(src_path))[1]
flag_combinations = [('O2', '-O2'), ('O3', '-O3')]
iterations = 40

if os.path.exists(dest_path):
    shutil.rmtree(dest_path)

for flag_combination in flag_combinations:
    for version in versions:
        if version.startswith('_'):
            continue
        new_path = src_path + sep + version
        cfiles = []
        datafiles = []
        for r, p, f in os.walk(new_path):
            for file in f:
                if file.endswith('.c'):
                    cfiles.append(r + sep + file)
                if r.endswith('data'):
                    datafiles.append((r + sep + file, file))
        path = dest_path + sep + flag_combination[0] + sep + version
        os.makedirs(path)
        generated_makefile = path + sep + 'Makefile'
        with open('Makefile', 'r') as makefile_read:
            with open(generated_makefile, 'w+') as makefile:
                makefile.write('OPTFLAGS=' + flag_combination[1] + '\nCFILES=' + ' '.join(cfiles) + '\nBASEPATH=' + new_path + '\nBINNAME=' + path + sep + 'disparity.exe\n')
                makefile.write(makefile_read.read())
        datadir = path + sep + 'data'
        os.mkdir(datadir)
        for data in datafiles:
            with open(data[0], 'rb') as data_read:
                with open(datadir + sep + data[1], 'wb+') as data_write:
                    data_write.write(data_read.read())
        subprocess.call([make, '-f', generated_makefile])
