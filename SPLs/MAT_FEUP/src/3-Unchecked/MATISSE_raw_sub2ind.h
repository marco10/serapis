/* Header file for MATISSE_raw_sub2ind */

#ifndef MATISSE_RAW_SUB2IND_H
#define MATISSE_RAW_SUB2IND_H

#include "lib/tensor_struct.h"
#include <stdlib.h>

/**
 */
double MATISSE_raw_sub2ind_titd_row_row_1(tensor_i* sizes, tensor_d* indices);

#endif
