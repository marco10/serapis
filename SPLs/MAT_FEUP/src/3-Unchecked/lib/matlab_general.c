/* Implementation file for lib/matlab_general */

#include "matlab_general.h"


/**
 *  min implementation for Y = min(X,Z), when X and Z are both scalars 
 */
int min_scalars_dec_ii(int scalar1, int scalar2)
{

	/* return the correct element */
	if( scalar1 < scalar2 )

		return scalar1;

	return scalar2;
}

/**
 *  max implementation for Y = max(X,Z), when X and Z are both scalars 
 */
int max_scalars_dec_ii(int scalar1, int scalar2)
{

	/* return the correct element */
	if( scalar1 > scalar2 )

		return scalar1;

	return scalar2;
}
