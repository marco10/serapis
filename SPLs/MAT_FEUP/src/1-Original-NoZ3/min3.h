/* Header file for min3 */

/**#ifndef MIN3_H**/
#define MIN3_H

#include "lib/matisse.h"
#include "lib/tensor_struct.h"
#include <stdlib.h>

/**
 */
void min3_tddi_undef_2(tensor_d* A, double B[1], int DIM, tensor_d** restrict y, tensor_d** restrict positions);

/**#endif**/
