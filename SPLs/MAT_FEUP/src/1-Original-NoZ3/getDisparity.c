/* Implementation file for getDisparity */

#include "getDisparity.h"
#include "lib/array_creators_alloc.h"
#include "lib/array_creators_dec.h"
#include "lib/general_matrix.h"
#include "lib/matisse.h"
#include "lib/matlab_general.h"
#include "lib/matrix.h"
#include "lib/tensor.h"
#include "lib/tensor_struct.h"
#include "min3.h"
#include "padarray_specialized_pre_post_tdd1x2_2.h"
#include "padarray_specialized_pre_tdi1x2s_2.h"
#include <stdlib.h>


/**
 */
void getDisparity_tdtdii_2d_2d_3(tensor_d* Ileft, tensor_d* Iright, int win_sz, int max_shift, tensor_d** restrict retDisparity, tensor_d** restrict retSAD, tensor_d** restrict minSAD)
{
   tensor_d* Ileft_1 = NULL;
   int numel_result;
   int iter_4;
   tensor_d* Iright_1 = NULL;
   int numel_result_1;
   int iter_3;
   int nr;
   int nc;
   int nb;
   double half_win_sz;
   double matrix_1[2];
   tensor_d* IleftPadded = NULL;
   double matrix[2];
   tensor_d* IrightPadded = NULL;
   int i;
   tensor_d* correlateSAD = NULL;
   int retSAD_end_1;
   int retSAD_end;
   int matrix_2[3];
   tensor_i* correlateSAD_size = NULL;
   int left_index_1;
   int right_index_1;
   int left_value_1;
   int right_value_1;
   int iter_6;
   int iter_5;
   int retSAD_size1;
   int retSAD_size2;
   int retSAD_size3;
   int result_dim3;
   int iter_2;
   int iter_1;
   int iter;
   double retSAD_value;
   int is_less;
   tensor_d* IleftPadded_1 = NULL;
   tensor_d* IrightPadded_1 = NULL;
   double min_arg2[1];

   new_array_helper_d(Ileft->shape, Ileft->dims, &Ileft_1);
   numel_result = Ileft->length;
   for(iter_4 = 0; iter_4 < numel_result; ++iter_4){
      Ileft_1->data[iter_4] = (double) Ileft->data[iter_4];
   }
   
   new_array_helper_d(Iright->shape, Iright->dims, &Iright_1);
   numel_result_1 = Iright->length;
   for(iter_3 = 0; iter_3 < numel_result_1; ++iter_3){
      Iright_1->data[iter_3] = (double) Iright->data[iter_3];
   }
   
   size_multiargs_td_3_generic(Ileft_1, &nr, &nc, &nb);
   zeros_d3(nr, nc, max_shift, retSAD);
   if(win_sz > 1){
      half_win_sz = (double) win_sz / 2.0;
      // Removed no-op function call: matisse_new_array_from_dims
      matrix_1[0] = half_win_sz;
      matrix_1[1] = half_win_sz;
      padarray_specialized_pre_post_tdd1x2_2_tdd1x2_2d_1(Ileft_1, matrix_1, &IleftPadded);
      // Removed no-op function call: matisse_new_array_from_dims
      matrix[0] = half_win_sz;
      matrix[1] = half_win_sz;
      padarray_specialized_pre_post_tdd1x2_2_tdd1x2_2d_1(Iright_1, matrix, &IrightPadded);
      for(i = 0; i < max_shift; ++i){
         correlateSAD_tdtdii_undef_undef_1(IleftPadded, IrightPadded, win_sz, i, &correlateSAD);
         retSAD_end_1 = (*retSAD)->shape[0];
         retSAD_end = (*retSAD)->shape[1];
         // Removed no-op function call: matisse_new_array_from_dims
         matrix_2[0] = retSAD_end_1;
         matrix_2[1] = retSAD_end;
         matrix_2[2] = 1;
         size_d(correlateSAD, &correlateSAD_size);
         left_index_1 = 0;
         right_index_1 = 0;
         while(left_index_1 < (3)){
            left_value_1 = matrix_2[left_index_1];
            ++left_index_1;
            if(left_value_1 == 1){
               continue;
            }
            
            if(right_index_1 >= correlateSAD_size->length){
               abort();
            }
            
            right_value_1 = correlateSAD_size->data[right_index_1];
            ++right_index_1;
            if(right_value_1 == 1){
               continue;
            }
            
            if(left_value_1 != right_value_1){
               abort();
            }
            
         }
         
         while(right_index_1 < correlateSAD_size->length){
            if(correlateSAD_size->data[right_index_1] != 1){
               abort();
            }
            
            ++right_index_1;
         }
         
         for(iter_6 = 0; iter_6 < retSAD_end; ++iter_6){
            for(iter_5 = 0; iter_5 < retSAD_end_1; ++iter_5){
               (*retSAD)->data[(iter_5) + (iter_6) * (*retSAD)->shape[0] + (i) * (*retSAD)->shape[0]*(*retSAD)->shape[1]] = correlateSAD->data[correlateSAD->length == 1 ? 0 : iter_5 + iter_6 * (matrix_2[0]) + (1 - 1) * (matrix_2[0]) * (matrix_2[1])];
            }
            
         }
         
      }
      
      retSAD_size1 = (*retSAD)->shape[0];
      retSAD_size2 = (*retSAD)->shape[1];
      retSAD_size3 = (*retSAD)->dims < 3 ? 1 : (*retSAD)->shape[2];
      result_dim3 = min_scalars_dec_ii(1, retSAD_size3);
      new_array_d_3(retSAD_size1, retSAD_size2, result_dim3, minSAD);
      new_array_d_3(retSAD_size1, retSAD_size2, result_dim3, retDisparity);
      for(iter_2 = 1; iter_2 <= retSAD_size3; ++iter_2){
         for(iter_1 = 0; iter_1 < retSAD_size2; ++iter_1){
            for(iter = 0; iter < retSAD_size1; ++iter){
               retSAD_value = (*retSAD)->data[(iter) + (iter_1) * (*retSAD)->shape[0] + (iter_2 - 1) * (*retSAD)->shape[0]*(*retSAD)->shape[1]];
               is_less = iter_2 == 1 || retSAD_value < (*minSAD)->data[(iter) + (iter_1) * (*minSAD)->shape[0]];
               if(is_less){
                  (*retDisparity)->data[(iter) + (iter_1) * (*retDisparity)->shape[0]] = (double) iter_2;
                  (*minSAD)->data[(iter) + (iter_1) * (*minSAD)->shape[0]] = retSAD_value;
               }
               
            }
            
         }
         
      }
      
   }else{
      if(Ileft_1 != NULL){
         new_array_helper_d(Ileft_1->shape, Ileft_1->dims, &IleftPadded_1);
         copy_td_ptd(Ileft_1, &IleftPadded_1);
      }
      
      if(Iright_1 != NULL){
         new_array_helper_d(Iright_1->shape, Iright_1->dims, &IrightPadded_1);
         copy_td_ptd(Iright_1, &IrightPadded_1);
      }
      
      correlateSAD_tdtdii_2d_2d_1(IleftPadded_1, IrightPadded_1, win_sz, 0, retSAD);
      new_col_d(min_arg2);
      min3_tddi_undef_2(*retSAD, min_arg2, 3, minSAD, retDisparity);
   }
   
   tensor_free_d(&IleftPadded);
   tensor_free_d(&IleftPadded_1);
   tensor_free_d(&Ileft_1);
   tensor_free_d(&IrightPadded);
   tensor_free_d(&IrightPadded_1);
   tensor_free_d(&Iright_1);
   tensor_free_d(&correlateSAD);
   tensor_free_i(&correlateSAD_size);
}

/**
 */
tensor_d* correlateSAD_tdtdii_2d_2d_1(tensor_d* Ileft, tensor_d* Iright, int win_sz, int disparity, tensor_d** restrict retSAD)
{
   int matrix[2];
   tensor_d* Iright_moved = NULL;
   int colon_arg2_8;
   int Iright_moved_end;
   int range_size_5;
   int ndims_result;
   int size;
   int iter_14;
   tensor_d* Iright_moved_1 = NULL;
   int iter_6;
   int iter_5;
   int Iright_moved_index;
   int iter_4;
   int rows;
   int cols;
   tensor_d* SAD = NULL;
   int i;
   int j;
   double diff;
   tensor_d* integralImg = NULL;
   int colon_arg1_8;
   int colon_arg2_6;
   int colon_arg1_5;
   int colon_arg2_7;
   int range_size_6;
   int range_size_7;
   tensor_d* plus_arg1_4 = NULL;
   int iter_3;
   int integralImg_index_4;
   int iter_2;
   int colon_arg2_4;
   int colon_arg2_5;
   int range_size_8;
   int range_size_1;
   tensor_d* plus_arg2 = NULL;
   int iter_1;
   int integralImg_index_1;
   int iter;
   tensor_i* size_result = NULL;
   tensor_i* size_2 = NULL;
   int dim_13;
   tensor_d* minus_arg1_3 = NULL;
   int numel_result_4;
   int iter_10;
   int colon_arg2_2;
   int colon_arg1_1;
   int colon_arg2_3;
   int range_size_2;
   int range_size_3;
   tensor_d* minus_arg2 = NULL;
   int iter_12;
   int integralImg_index_3;
   int iter_13;
   tensor_i* size_result_1 = NULL;
   tensor_i* size_3 = NULL;
   tensor_d* minus_arg1_6 = NULL;
   int numel_result_5;
   int iter_7;
   int colon_arg1_2;
   int colon_arg2;
   int colon_arg2_1;
   int range_size_4;
   int range_size;
   tensor_d* minus_arg2_1 = NULL;
   int iter_11;
   int integralImg_index_6;
   int iter_9;
   tensor_i* size_result_2 = NULL;
   tensor_i* size_1 = NULL;
   int numel_result_6;
   int iter_8;

   // Removed no-op function call: matisse_new_array_from_dims
   matrix[0] = 0;
   matrix[1] = disparity;
   padarray_specialized_pre_tdi1x2s_2_tdi1x2_2d_1(Iright, matrix, &Iright_moved);
   colon_arg2_8 = Iright_moved->shape[1] - disparity;
   Iright_moved_end = Iright_moved->shape[0];
   range_size_5 = colon_arg2_8 - 1 + 1;
   if(!(colon_arg2_8 <= Iright_moved->shape[1])){
      abort();
   }
   
   ndims_result = ndims_alloc_d(Iright_moved);
   size = 1;
   for(iter_14 = 3; iter_14 <= ndims_result; ++iter_14){
      size *= Iright_moved->dims < iter_14 ? 1 : Iright_moved->shape[iter_14 - 1];
   }
   
   new_array_d_3(Iright_moved_end, range_size_5, size, &Iright_moved_1);
   for(iter_6 = 0; iter_6 < size; ++iter_6){
      for(iter_5 = 1; iter_5 <= range_size_5; ++iter_5){
         Iright_moved_index = iter_5 + 1 - 1;
         for(iter_4 = 0; iter_4 < Iright_moved_end; ++iter_4){
            Iright_moved_1->data[(iter_4) + (iter_5 - 1) * Iright_moved_1->shape[0] + (iter_6) * Iright_moved_1->shape[0]*Iright_moved_1->shape[1]] = Iright_moved->data[(iter_4) + (Iright_moved_index - 1) * Iright_moved->shape[0] + (iter_6) * Iright_moved->shape[0]*Iright_moved->shape[1]];
         }
         
      }
      
   }
   
   size_multiargs_td_2_of_2(Ileft, &rows, &cols);
   new_array_d_2(rows, cols, &SAD);
   for(i = 0; i < rows; ++i){
      for(j = 0; j < cols; ++j){
         diff = Ileft->data[(i) + (j) * Ileft->shape[0]] - Iright_moved_1->data[(i) + (j) * Iright_moved_1->shape[0]];
         SAD->data[(i) + (j) * SAD->shape[0]] = diff * diff;
      }
      
   }
   
   // 2D scan.
   integralImage2D_td_2d_1(SAD, &integralImg);
   colon_arg1_8 = win_sz + 1;
   colon_arg2_6 = integralImg->shape[0];
   colon_arg1_5 = win_sz + 1;
   colon_arg2_7 = integralImg->shape[1];
   range_size_6 = colon_arg2_6 - colon_arg1_8 + 1;
   if(!(colon_arg2_6 <= integralImg->shape[0])){
      abort();
   }
   
   range_size_7 = colon_arg2_7 - colon_arg1_5 + 1;
   if(!(colon_arg2_7 <= integralImg->shape[1])){
      abort();
   }
   
   new_array_d_3(range_size_6, range_size_7, 1, &plus_arg1_4);
   for(iter_3 = 1; iter_3 <= range_size_7; ++iter_3){
      integralImg_index_4 = iter_3 + colon_arg1_5 - 1;
      for(iter_2 = 1; iter_2 <= range_size_6; ++iter_2){
         plus_arg1_4->data[(iter_2 - 1) + (iter_3 - 1) * plus_arg1_4->shape[0]] = integralImg->data[(iter_2 + colon_arg1_8 - 1 - 1) + (integralImg_index_4 - 1) * integralImg->shape[0]];
      }
      
   }
   
   colon_arg2_4 = integralImg->shape[0] - win_sz + 1;
   colon_arg2_5 = integralImg->shape[1] - win_sz + 1;
   range_size_8 = colon_arg2_4 - 2 + 1;
   if(!(colon_arg2_4 <= integralImg->shape[0])){
      abort();
   }
   
   range_size_1 = colon_arg2_5 - 2 + 1;
   if(!(colon_arg2_5 <= integralImg->shape[1])){
      abort();
   }
   
   new_array_d_3(range_size_8, range_size_1, 1, &plus_arg2);
   for(iter_1 = 1; iter_1 <= range_size_1; ++iter_1){
      integralImg_index_1 = iter_1 + 2 - 1;
      for(iter = 1; iter <= range_size_8; ++iter){
         plus_arg2->data[(iter - 1) + (iter_1 - 1) * plus_arg2->shape[0]] = integralImg->data[(iter + 2 - 1 - 1) + (integralImg_index_1 - 1) * integralImg->shape[0]];
      }
      
   }
   
   size_d(plus_arg1_4, &size_result);
   size_d(plus_arg2, &size_2);
   if(size_result->length == 2 && size_result->data[0] == 1 && size_result->data[1] == 1){
   }else{
      if(size_2->length == 2 && size_2->data[0] == 1 && size_2->data[1] == 1){
      }else{
         if((2) != (ndims_alloc_i(size_2))){
            abort();
         }
         
         for(dim_13 = 1; dim_13 <= (ndims_alloc_i(size_2)); ++dim_13){
            if((size_result->dims < dim_13 ? 1 : size_result->shape[dim_13 - 1]) != (size_2->dims < dim_13 ? 1 : size_2->shape[dim_13 - 1])){
               abort();
            }
            
         }
         
      }
      
      if(size_result != NULL){
         new_array_helper_i(size_result->shape, size_result->dims, &size_2);
         copy_ti_pti(size_result, &size_2);
      }
      
   }
   
   new_array_ti_d(size_2, &minus_arg1_3);
   numel_result_4 = minus_arg1_3->length;
   for(iter_10 = 0; iter_10 < numel_result_4; ++iter_10){
      minus_arg1_3->data[iter_10] = plus_arg1_4->data[plus_arg1_4->length == 1 ? 0 : iter_10] + plus_arg2->data[plus_arg2->length == 1 ? 0 : iter_10];
   }
   
   colon_arg2_2 = integralImg->shape[0] - win_sz + 1;
   colon_arg1_1 = win_sz + 1;
   colon_arg2_3 = integralImg->shape[1];
   range_size_2 = colon_arg2_2 - 2 + 1;
   if(!(colon_arg2_2 <= integralImg->shape[0])){
      abort();
   }
   
   range_size_3 = colon_arg2_3 - colon_arg1_1 + 1;
   if(!(colon_arg2_3 <= integralImg->shape[1])){
      abort();
   }
   
   new_array_d_3(range_size_2, range_size_3, 1, &minus_arg2);
   for(iter_12 = 1; iter_12 <= range_size_3; ++iter_12){
      integralImg_index_3 = iter_12 + colon_arg1_1 - 1;
      for(iter_13 = 1; iter_13 <= range_size_2; ++iter_13){
         minus_arg2->data[(iter_13 - 1) + (iter_12 - 1) * minus_arg2->shape[0]] = integralImg->data[(iter_13 + 2 - 1 - 1) + (integralImg_index_3 - 1) * integralImg->shape[0]];
      }
      
   }
   
   size_d(minus_arg1_3, &size_result_1);
   size_d(minus_arg2, &size_3);
   if(size_result_1->length == 2 && size_result_1->data[0] == 1 && size_result_1->data[1] == 1){
   }else{
      if(size_3->length == 2 && size_3->data[0] == 1 && size_3->data[1] == 1){
      }else{
         if((2) != (ndims_alloc_i(size_3))){
            abort();
         }
         
         for(dim_13 = 1; dim_13 <= (ndims_alloc_i(size_3)); ++dim_13){
            if((size_result_1->dims < dim_13 ? 1 : size_result_1->shape[dim_13 - 1]) != (size_3->dims < dim_13 ? 1 : size_3->shape[dim_13 - 1])){
               abort();
            }
            
         }
         
      }
      
      if(size_result_1 != NULL){
         new_array_helper_i(size_result_1->shape, size_result_1->dims, &size_3);
         copy_ti_pti(size_result_1, &size_3);
      }
      
   }
   
   new_array_ti_d(size_3, &minus_arg1_6);
   numel_result_5 = minus_arg1_6->length;
   for(iter_7 = 0; iter_7 < numel_result_5; ++iter_7){
      minus_arg1_6->data[iter_7] = minus_arg1_3->data[minus_arg1_3->length == 1 ? 0 : iter_7] - minus_arg2->data[minus_arg2->length == 1 ? 0 : iter_7];
   }
   
   colon_arg1_2 = win_sz + 1;
   colon_arg2 = integralImg->shape[0];
   colon_arg2_1 = integralImg->shape[1] - win_sz + 1;
   range_size_4 = colon_arg2 - colon_arg1_2 + 1;
   if(!(colon_arg2 <= integralImg->shape[0])){
      abort();
   }
   
   range_size = colon_arg2_1 - 2 + 1;
   if(!(colon_arg2_1 <= integralImg->shape[1])){
      abort();
   }
   
   new_array_d_3(range_size_4, range_size, 1, &minus_arg2_1);
   for(iter_11 = 1; iter_11 <= range_size; ++iter_11){
      integralImg_index_6 = iter_11 + 2 - 1;
      for(iter_9 = 1; iter_9 <= range_size_4; ++iter_9){
         minus_arg2_1->data[(iter_9 - 1) + (iter_11 - 1) * minus_arg2_1->shape[0]] = integralImg->data[(iter_9 + colon_arg1_2 - 1 - 1) + (integralImg_index_6 - 1) * integralImg->shape[0]];
      }
      
   }
   
   size_d(minus_arg1_6, &size_result_2);
   size_d(minus_arg2_1, &size_1);
   if(size_result_2->length == 2 && size_result_2->data[0] == 1 && size_result_2->data[1] == 1){
   }else{
      if(size_1->length == 2 && size_1->data[0] == 1 && size_1->data[1] == 1){
      }else{
         if((2) != (ndims_alloc_i(size_1))){
            abort();
         }
         
         for(dim_13 = 1; dim_13 <= (ndims_alloc_i(size_1)); ++dim_13){
            if((size_result_2->dims < dim_13 ? 1 : size_result_2->shape[dim_13 - 1]) != (size_1->dims < dim_13 ? 1 : size_1->shape[dim_13 - 1])){
               abort();
            }
            
         }
         
      }
      
      if(size_result_2 != NULL){
         new_array_helper_i(size_result_2->shape, size_result_2->dims, &size_1);
         copy_ti_pti(size_result_2, &size_1);
      }
      
   }
   
   new_array_ti_d(size_1, retSAD);
   numel_result_6 = (*retSAD)->length;
   for(iter_8 = 0; iter_8 < numel_result_6; ++iter_8){
      (*retSAD)->data[iter_8] = minus_arg1_6->data[minus_arg1_6->length == 1 ? 0 : iter_8] - minus_arg2_1->data[minus_arg2_1->length == 1 ? 0 : iter_8];
   }
   
   tensor_free_d(&Iright_moved);
   tensor_free_d(&Iright_moved_1);
   tensor_free_d(&SAD);
   tensor_free_d(&integralImg);
   tensor_free_d(&minus_arg1_3);
   tensor_free_d(&minus_arg1_6);
   tensor_free_d(&minus_arg2);
   tensor_free_d(&minus_arg2_1);
   tensor_free_d(&plus_arg1_4);
   tensor_free_d(&plus_arg2);
   tensor_free_i(&size_1);
   tensor_free_i(&size_2);
   tensor_free_i(&size_3);
   tensor_free_i(&size_result);
   tensor_free_i(&size_result_1);
   tensor_free_i(&size_result_2);
   
   return *retSAD;
}

/**
 */
tensor_d* integralImage2D_td_2d_1(tensor_d* I, tensor_d** restrict retImg)
{
   int nr;
   int nc;
   int nb;
   int I_end;
   int iter_2;
   int i;
   int retImg_arg1;
   int retImg_end;
   int iter_1;
   int j;
   int retImg_arg2;
   int retImg_end_1;
   int iter;

   size_multiargs_td_3_generic(I, &nr, &nc, &nb);
   zeros_d3(nr, nc, nb, retImg);
   if(!(1 <= I->shape[0])){
      abort();
   }
   
   I_end = I->shape[1];
   for(iter_2 = 0; iter_2 < I_end; ++iter_2){
      (*retImg)->data[iter_2] = I->data[iter_2];
   }
   
   for(i = 2; i <= nr; ++i){
      retImg_arg1 = i - 1;
      if(!(retImg_arg1 <= (*retImg)->shape[0])){
         abort();
      }
      
      retImg_end = (*retImg)->shape[1];
      if(!(i <= I->shape[0])){
         abort();
      }
      
      for(iter_1 = 0; iter_1 < retImg_end; ++iter_1){
         (*retImg)->data[(i - 1) + (iter_1) * (*retImg)->shape[0]] = (*retImg)->data[(retImg_arg1 - 1) + (iter_1) * (*retImg)->shape[0]] + I->data[(i - 1) + (iter_1) * I->shape[0]];
      }
      
   }
   
   // vtuneResumeMex;
   for(j = 2; j <= nc; ++j){
      retImg_arg2 = j - 1;
      retImg_end_1 = (*retImg)->shape[0];
      if(!(retImg_arg2 <= (*retImg)->shape[1])){
         abort();
      }
      
      if(!(j <= (*retImg)->shape[1])){
         abort();
      }
      
      for(iter = 0; iter < retImg_end_1; ++iter){
         (*retImg)->data[(iter) + (j - 1) * (*retImg)->shape[0]] = (*retImg)->data[(iter) + (retImg_arg2 - 1) * (*retImg)->shape[0]] + (*retImg)->data[(iter) + (j - 1) * (*retImg)->shape[0]];
      }
      
   }
   
   // vtunePauseMex;
   
   return *retImg;
}

/**
 */
tensor_d* correlateSAD_tdtdii_undef_undef_1(tensor_d* Ileft, tensor_d* Iright, int win_sz, int disparity, tensor_d** restrict retSAD)
{
   int matrix[2];
   tensor_d* Iright_moved = NULL;
   int colon_arg2_8;
   int Iright_moved_end;
   int range_size_5;
   int ndims_result;
   int size;
   int iter_14;
   tensor_d* Iright_moved_1 = NULL;
   int iter_6;
   int iter_5;
   int Iright_moved_index;
   int iter_4;
   int rows;
   int cols;
   tensor_d* SAD = NULL;
   int i;
   int j;
   double diff;
   tensor_d* integralImg = NULL;
   int colon_arg1_8;
   int colon_arg2_6;
   int colon_arg1_5;
   int colon_arg2_7;
   int range_size_6;
   int range_size_7;
   tensor_d* plus_arg1_4 = NULL;
   int iter_3;
   int integralImg_index_4;
   int iter_2;
   int colon_arg2_4;
   int colon_arg2_5;
   int range_size_8;
   int range_size_1;
   tensor_d* plus_arg2 = NULL;
   int iter_1;
   int integralImg_index_1;
   int iter;
   tensor_i* size_result = NULL;
   tensor_i* size_2 = NULL;
   int dim_13;
   tensor_d* minus_arg1_3 = NULL;
   int numel_result_4;
   int iter_10;
   int colon_arg2_2;
   int colon_arg1_1;
   int colon_arg2_3;
   int range_size_2;
   int range_size_3;
   tensor_d* minus_arg2 = NULL;
   int iter_12;
   int integralImg_index_3;
   int iter_13;
   tensor_i* size_result_1 = NULL;
   tensor_i* size_3 = NULL;
   tensor_d* minus_arg1_6 = NULL;
   int numel_result_5;
   int iter_7;
   int colon_arg1_2;
   int colon_arg2;
   int colon_arg2_1;
   int range_size_4;
   int range_size;
   tensor_d* minus_arg2_1 = NULL;
   int iter_11;
   int integralImg_index_6;
   int iter_9;
   tensor_i* size_result_2 = NULL;
   tensor_i* size_1 = NULL;
   int numel_result_6;
   int iter_8;

   // Removed no-op function call: matisse_new_array_from_dims
   matrix[0] = 0;
   matrix[1] = disparity;
   padarray_specialized_pre_tdi1x2s_2_tdi1x2_undef_1(Iright, matrix, &Iright_moved);
   colon_arg2_8 = Iright_moved->shape[1] - disparity;
   Iright_moved_end = Iright_moved->shape[0];
   range_size_5 = colon_arg2_8 - 1 + 1;
   if(!(colon_arg2_8 <= Iright_moved->shape[1])){
      abort();
   }
   
   ndims_result = ndims_alloc_d(Iright_moved);
   size = 1;
   for(iter_14 = 3; iter_14 <= ndims_result; ++iter_14){
      size *= Iright_moved->dims < iter_14 ? 1 : Iright_moved->shape[iter_14 - 1];
   }
   
   new_array_d_3(Iright_moved_end, range_size_5, size, &Iright_moved_1);
   for(iter_6 = 0; iter_6 < size; ++iter_6){
      for(iter_5 = 1; iter_5 <= range_size_5; ++iter_5){
         Iright_moved_index = iter_5 + 1 - 1;
         for(iter_4 = 0; iter_4 < Iright_moved_end; ++iter_4){
            Iright_moved_1->data[(iter_4) + (iter_5 - 1) * Iright_moved_1->shape[0] + (iter_6) * Iright_moved_1->shape[0]*Iright_moved_1->shape[1]] = Iright_moved->data[(iter_4) + (Iright_moved_index - 1) * Iright_moved->shape[0] + (iter_6) * Iright_moved->shape[0]*Iright_moved->shape[1]];
         }
         
      }
      
   }
   
   size_multiargs_td_2_generic(Ileft, &rows, &cols);
   new_array_d_2(rows, cols, &SAD);
   for(i = 0; i < rows; ++i){
      for(j = 0; j < cols; ++j){
         diff = Ileft->data[(i) + (j) * Ileft->shape[0]] - Iright_moved_1->data[(i) + (j) * Iright_moved_1->shape[0]];
         SAD->data[(i) + (j) * SAD->shape[0]] = diff * diff;
      }
      
   }
   
   // 2D scan.
   integralImage2D_td_2d_1(SAD, &integralImg);
   colon_arg1_8 = win_sz + 1;
   colon_arg2_6 = integralImg->shape[0];
   colon_arg1_5 = win_sz + 1;
   colon_arg2_7 = integralImg->shape[1];
   range_size_6 = colon_arg2_6 - colon_arg1_8 + 1;
   if(!(colon_arg2_6 <= integralImg->shape[0])){
      abort();
   }
   
   range_size_7 = colon_arg2_7 - colon_arg1_5 + 1;
   if(!(colon_arg2_7 <= integralImg->shape[1])){
      abort();
   }
   
   new_array_d_3(range_size_6, range_size_7, 1, &plus_arg1_4);
   for(iter_3 = 1; iter_3 <= range_size_7; ++iter_3){
      integralImg_index_4 = iter_3 + colon_arg1_5 - 1;
      for(iter_2 = 1; iter_2 <= range_size_6; ++iter_2){
         plus_arg1_4->data[(iter_2 - 1) + (iter_3 - 1) * plus_arg1_4->shape[0]] = integralImg->data[(iter_2 + colon_arg1_8 - 1 - 1) + (integralImg_index_4 - 1) * integralImg->shape[0]];
      }
      
   }
   
   colon_arg2_4 = integralImg->shape[0] - win_sz + 1;
   colon_arg2_5 = integralImg->shape[1] - win_sz + 1;
   range_size_8 = colon_arg2_4 - 2 + 1;
   if(!(colon_arg2_4 <= integralImg->shape[0])){
      abort();
   }
   
   range_size_1 = colon_arg2_5 - 2 + 1;
   if(!(colon_arg2_5 <= integralImg->shape[1])){
      abort();
   }
   
   new_array_d_3(range_size_8, range_size_1, 1, &plus_arg2);
   for(iter_1 = 1; iter_1 <= range_size_1; ++iter_1){
      integralImg_index_1 = iter_1 + 2 - 1;
      for(iter = 1; iter <= range_size_8; ++iter){
         plus_arg2->data[(iter - 1) + (iter_1 - 1) * plus_arg2->shape[0]] = integralImg->data[(iter + 2 - 1 - 1) + (integralImg_index_1 - 1) * integralImg->shape[0]];
      }
      
   }
   
   size_d(plus_arg1_4, &size_result);
   size_d(plus_arg2, &size_2);
   if(size_result->length == 2 && size_result->data[0] == 1 && size_result->data[1] == 1){
   }else{
      if(size_2->length == 2 && size_2->data[0] == 1 && size_2->data[1] == 1){
      }else{
         if((2) != (ndims_alloc_i(size_2))){
            abort();
         }
         
         for(dim_13 = 1; dim_13 <= (ndims_alloc_i(size_2)); ++dim_13){
            if((size_result->dims < dim_13 ? 1 : size_result->shape[dim_13 - 1]) != (size_2->dims < dim_13 ? 1 : size_2->shape[dim_13 - 1])){
               abort();
            }
            
         }
         
      }
      
      if(size_result != NULL){
         new_array_helper_i(size_result->shape, size_result->dims, &size_2);
         copy_ti_pti(size_result, &size_2);
      }
      
   }
   
   new_array_ti_d(size_2, &minus_arg1_3);
   numel_result_4 = minus_arg1_3->length;
   for(iter_10 = 0; iter_10 < numel_result_4; ++iter_10){
      minus_arg1_3->data[iter_10] = plus_arg1_4->data[plus_arg1_4->length == 1 ? 0 : iter_10] + plus_arg2->data[plus_arg2->length == 1 ? 0 : iter_10];
   }
   
   colon_arg2_2 = integralImg->shape[0] - win_sz + 1;
   colon_arg1_1 = win_sz + 1;
   colon_arg2_3 = integralImg->shape[1];
   range_size_2 = colon_arg2_2 - 2 + 1;
   if(!(colon_arg2_2 <= integralImg->shape[0])){
      abort();
   }
   
   range_size_3 = colon_arg2_3 - colon_arg1_1 + 1;
   if(!(colon_arg2_3 <= integralImg->shape[1])){
      abort();
   }
   
   new_array_d_3(range_size_2, range_size_3, 1, &minus_arg2);
   for(iter_12 = 1; iter_12 <= range_size_3; ++iter_12){
      integralImg_index_3 = iter_12 + colon_arg1_1 - 1;
      for(iter_13 = 1; iter_13 <= range_size_2; ++iter_13){
         minus_arg2->data[(iter_13 - 1) + (iter_12 - 1) * minus_arg2->shape[0]] = integralImg->data[(iter_13 + 2 - 1 - 1) + (integralImg_index_3 - 1) * integralImg->shape[0]];
      }
      
   }
   
   size_d(minus_arg1_3, &size_result_1);
   size_d(minus_arg2, &size_3);
   if(size_result_1->length == 2 && size_result_1->data[0] == 1 && size_result_1->data[1] == 1){
   }else{
      if(size_3->length == 2 && size_3->data[0] == 1 && size_3->data[1] == 1){
      }else{
         if((2) != (ndims_alloc_i(size_3))){
            abort();
         }
         
         for(dim_13 = 1; dim_13 <= (ndims_alloc_i(size_3)); ++dim_13){
            if((size_result_1->dims < dim_13 ? 1 : size_result_1->shape[dim_13 - 1]) != (size_3->dims < dim_13 ? 1 : size_3->shape[dim_13 - 1])){
               abort();
            }
            
         }
         
      }
      
      if(size_result_1 != NULL){
         new_array_helper_i(size_result_1->shape, size_result_1->dims, &size_3);
         copy_ti_pti(size_result_1, &size_3);
      }
      
   }
   
   new_array_ti_d(size_3, &minus_arg1_6);
   numel_result_5 = minus_arg1_6->length;
   for(iter_7 = 0; iter_7 < numel_result_5; ++iter_7){
      minus_arg1_6->data[iter_7] = minus_arg1_3->data[minus_arg1_3->length == 1 ? 0 : iter_7] - minus_arg2->data[minus_arg2->length == 1 ? 0 : iter_7];
   }
   
   colon_arg1_2 = win_sz + 1;
   colon_arg2 = integralImg->shape[0];
   colon_arg2_1 = integralImg->shape[1] - win_sz + 1;
   range_size_4 = colon_arg2 - colon_arg1_2 + 1;
   if(!(colon_arg2 <= integralImg->shape[0])){
      abort();
   }
   
   range_size = colon_arg2_1 - 2 + 1;
   if(!(colon_arg2_1 <= integralImg->shape[1])){
      abort();
   }
   
   new_array_d_3(range_size_4, range_size, 1, &minus_arg2_1);
   for(iter_11 = 1; iter_11 <= range_size; ++iter_11){
      integralImg_index_6 = iter_11 + 2 - 1;
      for(iter_9 = 1; iter_9 <= range_size_4; ++iter_9){
         minus_arg2_1->data[(iter_9 - 1) + (iter_11 - 1) * minus_arg2_1->shape[0]] = integralImg->data[(iter_9 + colon_arg1_2 - 1 - 1) + (integralImg_index_6 - 1) * integralImg->shape[0]];
      }
      
   }
   
   size_d(minus_arg1_6, &size_result_2);
   size_d(minus_arg2_1, &size_1);
   if(size_result_2->length == 2 && size_result_2->data[0] == 1 && size_result_2->data[1] == 1){
   }else{
      if(size_1->length == 2 && size_1->data[0] == 1 && size_1->data[1] == 1){
      }else{
         if((2) != (ndims_alloc_i(size_1))){
            abort();
         }
         
         for(dim_13 = 1; dim_13 <= (ndims_alloc_i(size_1)); ++dim_13){
            if((size_result_2->dims < dim_13 ? 1 : size_result_2->shape[dim_13 - 1]) != (size_1->dims < dim_13 ? 1 : size_1->shape[dim_13 - 1])){
               abort();
            }
            
         }
         
      }
      
      if(size_result_2 != NULL){
         new_array_helper_i(size_result_2->shape, size_result_2->dims, &size_1);
         copy_ti_pti(size_result_2, &size_1);
      }
      
   }
   
   new_array_ti_d(size_1, retSAD);
   numel_result_6 = (*retSAD)->length;
   for(iter_8 = 0; iter_8 < numel_result_6; ++iter_8){
      (*retSAD)->data[iter_8] = minus_arg1_6->data[minus_arg1_6->length == 1 ? 0 : iter_8] - minus_arg2_1->data[minus_arg2_1->length == 1 ? 0 : iter_8];
   }
   
   tensor_free_d(&Iright_moved);
   tensor_free_d(&Iright_moved_1);
   tensor_free_d(&SAD);
   tensor_free_d(&integralImg);
   tensor_free_d(&minus_arg1_3);
   tensor_free_d(&minus_arg1_6);
   tensor_free_d(&minus_arg2);
   tensor_free_d(&minus_arg2_1);
   tensor_free_d(&plus_arg1_4);
   tensor_free_d(&plus_arg2);
   tensor_free_i(&size_1);
   tensor_free_i(&size_2);
   tensor_free_i(&size_3);
   tensor_free_i(&size_result);
   tensor_free_i(&size_result_1);
   tensor_free_i(&size_result_2);
   
   return *retSAD;
}
