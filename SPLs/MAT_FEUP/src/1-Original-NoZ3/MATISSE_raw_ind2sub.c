/* Implementation file for MATISSE_raw_ind2sub */

#include "MATISSE_raw_ind2sub.h"
#include "lib/array_creators_alloc.h"
#include "lib/matisse.h"
#include "lib/tensor.h"
#include "lib/tensor_struct.h"
#include <math.h>
#include <stdlib.h>


/**
 */
tensor_d* MATISSE_raw_ind2sub_tii_row_1(tensor_i* sizes, int ind, tensor_d** restrict s)
{
   tensor_d* dims_to = NULL;
   int end_1;
   int i;
   int end;
   int i_1;

   zeros_d2(1, sizes->length + 1, &dims_to);
   dims_to->data[0] = 1.0;
   end_1 = sizes->length;
   for(i = 1; i <= end_1; ++i){
      dims_to->data[i + 1 - 1] = dims_to->data[i - 1] * (double) sizes->data[i - 1];
   }
   
   zeros_d2(1, sizes->length, s);
   end = sizes->length;
   for(i_1 = 1; i_1 <= end; ++i_1){
      (*s)->data[i_1 - 1] = floor(fmod((double) (ind - 1), dims_to->data[i_1 + 1 - 1]) / dims_to->data[i_1 - 1]) + 1.0;
   }
   
   tensor_free_d(&dims_to);
   
   return *s;
}
