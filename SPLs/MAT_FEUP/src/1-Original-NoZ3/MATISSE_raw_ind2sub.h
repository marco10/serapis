/* Header file for MATISSE_raw_ind2sub */

/**#ifndef MATISSE_RAW_IND2SUB_H**/
#define MATISSE_RAW_IND2SUB_H

#include "lib/matisse.h"
#include "lib/tensor_struct.h"
#include <stdlib.h>

/**
 */
tensor_d* MATISSE_raw_ind2sub_tii_row_1(tensor_i* sizes, int ind, tensor_d** restrict s);

/**#endif**/
