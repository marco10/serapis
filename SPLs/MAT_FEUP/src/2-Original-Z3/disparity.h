/* Header file for disparity */

#ifndef DISPARITY_H
#define DISPARITY_H

#include "lib/matisse.h"
#include "lib/tensor_struct.h"
#include <stdlib.h>

/**
 */
tensor_d* disparity_tctdtd_row_2d_2d_1(tensor_c* type, tensor_d* imleft, tensor_d* imright, tensor_d** restrict imDispOwn);

#endif
