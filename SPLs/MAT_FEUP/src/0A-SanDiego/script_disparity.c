/********************************
Author: Sravanthi Kota Venkata
********************************/

#include <stdio.h>
#include <stdlib.h>
#include "disparity.h"

#include <time.h>

#ifdef _WIN32
#include <windows.h>

double get_time_in_seconds() {
	LARGE_INTEGER t, f;
	QueryPerformanceCounter(&t);
	QueryPerformanceFrequency(&f);
	return (double)t.QuadPart/(double)f.QuadPart;
}

#endif
#define MULTI_EXEC 1

char * get_absolute_filename(char * filename)
{
//char* get_absolute_filename(const char* filename) {

#ifdef _WIN32
   HMODULE module = GetModuleHandleA(NULL);
   char* file_path = NULL;
   size_t size = 32;
   do {
      size *= 2;
      file_path = realloc(file_path, size);
   } while (GetModuleFileNameA(module, file_path, size) >= size);
   
   // file_path has the path including the executable name now.
   
   size_t index = strlen(file_path) - 2;
   while (file_path[index] != '\\' && file_path[index] != '/') {
      if (--index <= 0) {
         fprintf(stderr, "Could not get executable path");
         exit(1);
      }
   }
   
   file_path[index + 1] = '\0';
#else
// disabled warning: #warning Fallback absolute path - returning relative path
   char* file_path = malloc(3);
   strcpy(file_path, "./");
#endif

   if (file_path == NULL) {
      fprintf(stderr, "Could not get executable folder\n");
      exit(1);
   }
   char* absolute_filename = malloc(strlen(file_path) + strlen(filename) + 1);
   if (absolute_filename == NULL) {
      fprintf(stderr, "Could not allocate memory for absolute filename\n");
      exit(1);
   }
   strcpy(absolute_filename, file_path);
   strcat(absolute_filename, filename);
   free(file_path);

   return absolute_filename;}

int main(int argc, char* argv[])
{
    int rows = 32;
    int cols = 32;
    I2D *imleft, *imright, *retDisparity;
    
    int i, j;
    const char *im1, *im2;
    char timFile[100];
    int WIN_SZ=8, SHIFT=64;
    FILE* fp;
   double timeElapsed;
   double start, end;
   #if MULTI_EXEC
   int iterations_idx;   
   int iterations = 1;
   #endif
    
    im1 = get_absolute_filename("data/1.bmp");
    im2 = get_absolute_filename("data/2.bmp");
    
    imleft = readImage(im1);
    imright = readImage(im2);

    rows = imleft->height;
    cols = imleft->width;

#ifdef test
    WIN_SZ = 2;
    SHIFT = 1;
#endif
#ifdef sim_fast
    WIN_SZ = 4;
    SHIFT = 4;
#endif
#ifdef sim
    WIN_SZ = 4;
    SHIFT = 8;
#endif

   // Start measuring
   start = get_time_in_seconds();
   
   #if MULTI_EXEC
   for(iterations_idx = 0; iterations_idx<iterations; iterations_idx++)
   {
   #endif
    retDisparity = getDisparity(imleft, imright, WIN_SZ, SHIFT);
   #if MULTI_EXEC
   }
   #endif
   
   // Stop measuring
   end = get_time_in_seconds();
   timeElapsed = end - start;
   printf("%e", timeElapsed);

    //printf("Input size\t\t- (%dx%d)\n", rows, cols);
#ifdef CHECK   
    /** Self checking - use expected.txt from data directory  **/
    {
        int tol, ret=0;
        tol = 2;
#ifdef GENERATE_OUTPUT
        writeMatrix(retDisparity, argv[1]);
#endif
        ret = selfCheck(retDisparity, argv[1], tol);
        if (ret == -1)
            printf("Error in Disparity Map\n");
    }
    /** Self checking done **/
#endif
    
    iFreeHandle(imleft);
    iFreeHandle(imright);
    iFreeHandle(retDisparity);

    return 0;
}
