/* Implementation file for getDisparity */

#include "getDisparity.h"
#include "lib/general_matrix.h"
#include "lib/matisse.h"
#include "lib/matrix.h"
#include "lib/tensor.h"
#include "lib/tensor_struct.h"
#include "padarray_specialized_pre_post_tdd1x2_2.h"
#include "padarray_specialized_pre_tdi1x2s_2.h"
#include <stdlib.h>


/**
 */
tensor_d* getDisparity_tdtdii_2d_2d_1(tensor_d* Ileft, tensor_d* Iright, int win_sz, int max_shift, tensor_d** restrict retDisparity)
{
   int numel_result;
   int iter_1;
   int numel_result_1;
   int iter;
   int nr;
   int nc;
   int nb;
   double half_win_sz;
   double matrix_1[2];
   tensor_d* IleftPadded = NULL;
   double matrix[2];
   tensor_d* IrightPadded = NULL;
   tensor_d* minSAD = NULL;
   int mtimes;
   int minSAD_numel;
   int iter_3;
   int retDisparity_numel;
   int iter_2;
   tensor_d* SAD = NULL;
   tensor_d* retSAD = NULL;
   int k;
   int j;
   int i;
   double a;
   double b;
   
   size_multiargs_td_3_generic(Ileft, &nr, &nc, &nb);
   if(win_sz > 1){
      half_win_sz = (double) win_sz / 2.0;
      // Removed no-op function call: matisse_new_array_from_dims
      matrix_1[0] = half_win_sz;
      matrix_1[1] = half_win_sz;
      padarray_specialized_pre_post_tdd1x2_2_tdd1x2_2d_1(Ileft, matrix_1, &IleftPadded);
      // Removed no-op function call: matisse_new_array_from_dims
      matrix[0] = half_win_sz;
      matrix[1] = half_win_sz;
      padarray_specialized_pre_post_tdd1x2_2_tdd1x2_2d_1(Iright, matrix, &IrightPadded);
   }else{
      if(Ileft != NULL){
         new_array_helper_d(Ileft->shape, Ileft->dims, &IleftPadded);
         copy_td_ptd(Ileft, &IleftPadded);
      }
      
      if(Iright != NULL){
         new_array_helper_d(Iright->shape, Iright->dims, &IrightPadded);
         copy_td_ptd(Iright, &IrightPadded);
      }
      
   }
   
   new_array_d_2(nr, nc, &minSAD);
   mtimes = 255 * 255 * 255;
   minSAD_numel = minSAD->length;
   for(iter_3 = 0; iter_3 < minSAD_numel; ++iter_3){
      minSAD->data[iter_3] = (double) mtimes;
   }
   
   new_array_d_2(nr, nc, retDisparity);
   retDisparity_numel = (*retDisparity)->length;
   for(iter_2 = 0; iter_2 < retDisparity_numel; ++iter_2){
      (*retDisparity)->data[iter_2] = (double) max_shift;
   }
   
   new_array_d_2(IleftPadded->shape[0], IleftPadded->shape[1], &SAD);
   new_array_d_2(nr, nc, &retSAD);
   for(k = 1; k <= max_shift; ++k){
      correlateSAD_tdtdiitdtd_undef_undef_2d_2d_2(IleftPadded, IrightPadded, win_sz, k - 1, &retSAD, &SAD);
      //  Find Disparity
      //  findDisparity(retSAD, minSAD, retDisp, k, nr, nc)
      for(j = 0; j < nc; ++j){
         for(i = 0; i < nr; ++i){
            a = retSAD->data[(i) + (j) * retSAD->shape[0]];
            b = minSAD->data[(i) + (j) * minSAD->shape[0]];
            if(a < b){
               minSAD->data[(i) + (j) * minSAD->shape[0]] = a;
               (*retDisparity)->data[(i) + (j) * (*retDisparity)->shape[0]] = (double) k;
            }
            
         }
         
      }
      
   }
   
   tensor_free_d(&IleftPadded);
   tensor_free_d(&IrightPadded);
   tensor_free_d(&SAD);
   tensor_free_d(&minSAD);
   tensor_free_d(&retSAD);
   
   return *retDisparity;
}

/**
 */
void correlateSAD_tdtdiitdtd_undef_undef_2d_2d_2(tensor_d* Ileft, tensor_d* Iright, int win_sz, int disparity, tensor_d** restrict retSAD, tensor_d** restrict SAD)
{
   int matrix_1[2];
   tensor_d* Iright_moved = NULL;
   int end;
   int i;
   double diff;
   int colon_arg1_6;
   int colon_arg1_7;
   int range_size;
   int range_size_1;
   int colon_arg2_1;
   int colon_arg1_3;
   int colon_arg1;
   tensor_i* size = NULL;
   tensor_d* minus = NULL;
   int iter_3;
   int SAD_index_3;
   int SAD_index_5;
   int SAD_index_7;
   int SAD_index_1;
   int iter_2;
   int retSAD_end_1;
   int retSAD_end;
   int matrix[2];
   int iter;
   int iter_1;

   // Removed no-op function call: matisse_new_array_from_dims
   matrix_1[0] = 0;
   matrix_1[1] = disparity;
   padarray_specialized_pre_tdi1x2s_2_tdi1x2_undef_1(Iright, matrix_1, &Iright_moved);
   end = Ileft->length;
   for(i = 0; i < end; ++i){
      diff = Ileft->data[i] - Iright_moved->data[i];
      (*SAD)->data[i] = diff * diff;
   }
   
   // 2D scan.
   integralImage2D_td_2d_1(SAD);
   colon_arg1_6 = win_sz + 1;
   colon_arg1_7 = win_sz + 1;
   range_size = (*SAD)->shape[0] - colon_arg1_6 + 1;
   range_size_1 = (*SAD)->shape[1] - colon_arg1_7 + 1;
   colon_arg2_1 = (*SAD)->shape[1] - win_sz + 1;
   if(!((*SAD)->shape[0] - win_sz + 1 <= (*SAD)->shape[0])){
      abort();
   }
   
   if(!(colon_arg2_1 <= (*SAD)->shape[1])){
      abort();
   }
   
   colon_arg1_3 = win_sz + 1;
   if(!((*SAD)->shape[0] - win_sz + 1 <= (*SAD)->shape[0])){
      abort();
   }
   
   colon_arg1 = win_sz + 1;
   if(!((*SAD)->shape[1] - win_sz + 1 <= (*SAD)->shape[1])){
      abort();
   }
   for(iter_3 = 1; iter_3 <= range_size_1; ++iter_3){
      SAD_index_3 = iter_3 + colon_arg1_7 - 1;
      SAD_index_5 = iter_3 + 2 - 1;
      SAD_index_7 = iter_3 + colon_arg1_3 - 1;
      SAD_index_1 = iter_3 + 2 - 1;
      for(iter_2 = 1; iter_2 <= range_size; ++iter_2){
         (*retSAD)->data[(iter_2 - 1) + (iter_3 - 1) * (*retSAD)->shape[0]] = (*SAD)->data[(iter_2 + colon_arg1_6 - 1 - 1) + (SAD_index_3 - 1) * (*SAD)->shape[0]] + (*SAD)->data[(iter_2 + 2 - 1 - 1) + (SAD_index_5 - 1) * (*SAD)->shape[0]] - (*SAD)->data[(iter_2 + 2 - 1 - 1) + (SAD_index_7 - 1) * (*SAD)->shape[0]] - (*SAD)->data[(iter_2 + colon_arg1 - 1 - 1) + (SAD_index_1 - 1) * (*SAD)->shape[0]];
      }
      
   }
   
   tensor_free_d(&Iright_moved);
}

/**
 */
tensor_d* integralImage2D_td_2d_1(tensor_d** restrict I)
{
   int nr;
   int nc;
   int nb;
   int i;
   int I_arg1;
   int I_end_1;
   int iter_1;
   int j;
   int I_arg2;
   int I_end;
   int iter;

   size_multiargs_td_3_generic(*I, &nr, &nc, &nb);
   for(iter_1 = 0; iter_1 < I_end_1; ++iter_1){
       for(i = 2; i <= nr; ++i){
         I_arg1 = i - 1;
         I_end_1 = (*I)->shape[1];
         (*I)->data[(i - 1) + (iter_1) * (*I)->shape[0]] = (*I)->data[(I_arg1 - 1) + (iter_1) * (*I)->shape[0]] + (*I)->data[(i - 1) + (iter_1) * (*I)->shape[0]];
      }
      
   }
   
   // vtuneResumeMex;
   for(j = 2; j <= nc; ++j){
      I_arg2 = j - 1;
      I_end = (*I)->shape[0];
      for(iter = 0; iter < I_end; ++iter){
         (*I)->data[(iter) + (j - 1) * (*I)->shape[0]] = (*I)->data[(iter) + (I_arg2 - 1) * (*I)->shape[0]] + (*I)->data[(iter) + (j - 1) * (*I)->shape[0]];
      }
      
   }
   
   // vtunePauseMex;
   
   return *I;
}
