#!/usr/bin/python

import re, sys, os
import fileinput as fin
from pycparser import c_parser, c_ast, parse_file, c_generator
from subprocess import call, check_output

from lazyme.string import color_print

generator = c_generator.CGenerator()
ignore_features = 0
phases = [True, True, True, True, False, True]

class FuncDefVisitor(c_ast.NodeVisitor):
  def __init__(self, funcname):
    self.funcname = funcname
    self.node = None

  def visit_FuncDef(self, node):
    if self.funcname == node.decl.name:  #&
      self.node = node

class FuncDeclVisitor(c_ast.NodeVisitor):
  def __init__(self, ast):
    self.ast = ast

  def visit_Decl(self, node):
    if isinstance(node.type, c_ast.FuncDecl):
      v = FuncDefVisitor(node.type.type.declname)
      v.visit(self.ast)
      if v.node != None:
        node = v.node

def clean_files(files):
  # switch all kind of tokens that cilly or CPP can't deal with
  for f in files:
    with fin.FileInput(f, inplace=True, backup='.bak') as file:
      for line in file:
        new_line = re.sub(r"size_t", r"int", line)
        new_line = re.sub(r"NULL", r"0", new_line)
        new_line = re.sub(r"bool ", r"int ", new_line)
        new_line = re.sub(r"true", r"1", new_line)
        new_line = re.sub(r"false", r"0", new_line)
        new_line = re.sub(r"uint8_t", r"int", new_line)
        new_line = re.sub(r"uint16_t", r"int", new_line)
        new_line = re.sub(r"uint64_t", r"int", new_line)
        new_line = re.sub(r"int8_t", r"int", new_line)
        new_line = re.sub(r"int16_t", r"int", new_line)
        new_line = re.sub(r"int64_t", r"int", new_line)
        new_line = re.sub(r"__m128d", r"int", new_line)
        print(new_line, end="")

    p = call(['rm', '-r', f+".bak"])


def getFuncSig(file, new_ext, new_names):
  ast = parse_file(file, use_cpp=True, cpp_path='cpp', cpp_args='')
  for n in ast.ext:
    if isinstance(n, c_ast.Decl):
      fd = n.type
      if ('extern' in n.storage) | (n.name in new_names):
        pass
      elif isinstance(fd, c_ast.FuncDecl):
        new_names.add(n.name)
        new_ext.append(n)
  #return generator.visit(c_ast.FileAST(new_ext))
  return new_ext, new_names

def fetchFunctions(mainFile, cFiles, imports, signatures, fetch=False):
  ast = parse_file(mainFile, use_cpp=True,
            cpp_path='cpp',
            cpp_args='')

  if fetch == False:
    return ast
  i=0
  already = []
  #first of all, put the signatures in the right place
  for n in ast.ext:
    if isinstance(n, c_ast.Decl):
      if isinstance(n.type, c_ast.FuncDecl):
        already.append(n.name)
    if isinstance(n, c_ast.FuncDef):
      name = n.decl.name
      if name == 'main':
        break
    i += 1
  
  signatures = list(filter(lambda x: not(any(i == x.name for i in already)), signatures))
  #signatures = list(filter(lambda x: len(  list(filter(lambda y: y in x.name, already))  ) == 0, signatures))
  
  left = ast.ext[:i]
  right = ast.ext[i:]
  ast.ext = left + signatures + right

  i=0
  for n in ast.ext:
    if isinstance(n, c_ast.Decl):
      fd = n.type
      name = n.name
      if name is None:
        name = ''
      lastFeature = ''
      if name.startswith('__feature_') | name.startswith('__endfeature_'):
        if name.startswith('__feature_'):
          lastFeature = name.replace('__feature_', '')
        else:
          lastFeature = ''
        #ident = c_ast.IdentifierType(['void'], n.coord)
        #tp = c_ast.TypeDecl(name, None, ident, n.coord)
        #ast.ext[i] = c_ast.Decl(name, [], [], [], tp, None, None, n.coord)
      if isinstance(fd, c_ast.FuncDecl):
        if 'extern' in n.storage:
          #del ast.ext[i]
          continue
        node, j = None, 0
        if lastFeature != '':
          for c in imports:
            if c['feature'] == lastFeature:
              sources = c['includes']
              break
        else:
          sources = cFiles
        while (node == None) & (j < len(sources)):
          #print(sources[j])
          ast_aux = parse_file(sources[j], use_cpp=True)
          vis = FuncDefVisitor(name)
          vis.visit(ast_aux)
          if vis.node != None:
            node = vis.node
          j+=1
        if node != None:
          ast.ext[i] = node
        else:
          ast.ext[i] = c_ast.FuncDef(n, [], c_ast.Compound([], n.coord), n.coord)
    i+=1
  #ast.show()
  #print(generator.visit(ast))
  
  return ast

def expandFeatures(ast):
  i=0
  feats = Stack()
  for n in ast.ext:
    if isinstance(n, c_ast.Decl):
      name = n.name
      if name.startswith('__feature_'):
        tag = re.sub(r'__feature_(.+)', r'\1', name)
        feats.push(tag)
      elif name.startswith('__endfeature_'):
        feats.pop()
    elif isinstance(n, c_ast.FuncDef) & (not feats.isEmpty()):
      name = n.decl.type
      tags = ''
      for e in feats.getAll():
        tags += e + "&&"
      tags = tags[:-2]
      new_name = n.decl.name + '__feature_' + tags
      ast.ext[i].decl.name = new_name
      ast.ext[i].decl.type.type.declname = new_name
    i+=1
  return ast

class Stack(object):
    def __init__(self):
      self.items = []

    def push(self, item):
      self.items.append(item)

    def pop(self):
      return self.items.pop()

    def peek(self):
      return self.items[-1]

    def getAll(self):
      return self.items

    def isEmpty(self):
      return len(self.items) == 0
    
    def empty(self):
      del self.items[:]


def featureList(includes, excludes):
  feats = ''
  for i in includes.getAll():
    feats += i + "&&"
  for e in excludes.getAll():
    feats += "not_" + e + "&&"
  feats = feats[:-3]    #remove last occurence of '&&'
  return feats

def getValueType(val):
  if val.startswith("{"):
    x = val.replace("{","").split(",")[0]
    tp = getValueType(x)
    return tp + '[' + str(len(x)) + ']'
  if val.startswith("\'"):
    return 'char'
  if val.startswith("\""):
    return 'char [' + str(len(val)-2) + "]"
  try:
    a = float(val)
  except ValueError:
    try:
      a = int(val)
    except ValueError:
      return 'null'
    else:
      return 'int'
  else:
    return 'float'

def updateDefinesDict(dicList, tag, feat, val):
  i = 0
  found = False
  for d in dicList:
    if tag in d['tag']:
      dicList[i]['defs'].append((feat, val))
      found = True
      break
    i+=1
  if not found:
    dicList.append({'tag' : tag, 'defs' : [(feat, val)]})
  return dicList

def transformFeatures(lines):
  #dictonary for #define replacemente: [{'tag': tag, 'defs' : [('feature', 'value')]}]
  definesByFeats = []
  newContent = []
  imports = []
  includes = Stack()
  excludes = Stack()
  control = Stack()
  begins, ends = 0, 0
  for l in lines:
    if re.match(r'^[ \t]*# *define ', l):
      #check if it is inside of any kind of '#if'
      if includes.isEmpty() & excludes.isEmpty():
        pass
      else:
        lst = re.split(r' +', (re.sub(r'^[ \t]*# *define +(.+)', r'\1', l)))
        if len(lst) == 2:
          tag = lst[0]
          val = lst[1].replace('\n','')
          tp = getValueType(val)
          if tp != 'null':
            l = tp + " " + tag + " = " + val + ";\n"
          else:
            feats = featureList(includes, excludes)
            tag = lst[0]
            val = ' '.join(lst[1:])
            definesByFeats = updateDefinesDict(definesByFeats, tag, feats, val)
        elif len(lst) == 1:
          tag = lst[0]
          val = '1'
          tp = 'int'
          l = tp + " " + tag + " = " + val + ";\n"
        else:
          lst = re.split(r' +', (re.sub(r'^[ \t]*# *define +(.+)', r'\1', l)))
          feats = featureList(includes, excludes)
          tag = lst[0]
          val = ' '.join(lst[1:]).replace('\n','')
          definesByFeats = updateDefinesDict(definesByFeats, tag, feats, val)
    elif re.match(r'^[ \t]*# *ifdef', l): #l.startswith("#ifdef "):
      s = re.sub(r'[ \t]*# *ifdef *', '', l) #l.replace("#ifdef ", "")
      includes.push(s)
      feats = featureList(includes, excludes)
      l = "void __feature_" + feats + ';\n'
      begins = begins + 1
      control.push("i")
    elif re.match(r'^[ \t]*# *ifndef', l): #l.startswith("#ifndef "):
      s = l.replace("#ifndef ", "")
      excludes.push(s)
      feats = featureList(includes, excludes)
      l = "void __feature_" + feats + ";\n"
      begins = begins + 1
      control.push("e")
    elif re.match(r'^[ \t]*# *if', l): #l.startswith("#if "):
      regex = r'^[ \t]*# *if +(!?)defined? *\(([_a-zA-Z0-9]+)\)'
      aux = re.sub(regex, r'\1\2', l)
      su = re.sub(r'\(.+\)', 'AAA', aux)
      if '||' in su:
        s = aux.replace(" ", "").replace("&&", "><")
      else:
        s = aux.replace(" ", "")
      includes.push(s)
      feats = featureList(includes, excludes)
      l = "void __feature_" + feats + ";\n"
      begins = begins + 1
      control.push("i")
    elif re.match(r'^[ \t]*# *else', l): #l.startswith("#else "):
      excludes.push(includes.pop())
      control.pop()
      control.push("e")
      feats = featureList(includes, excludes)
      l = "void __endfeature_;\n" + "void __feature_" + feats + ';\n'
      ends = ends + 1
      begins = begins + 1
    elif re.match(r'^[ \t]*# *endif', l): #l.startswith("#endif "):
      if begins > ends:
        l = "void __endfeature_;\n"
        ends = ends + 1
      c = control.pop()
      if c == "i":
        includes.pop()
      else:
        excludes.pop()
      if (begins == ends) & control.isEmpty() & (not includes.isEmpty()) & (not excludes.isEmpty()):
        control.empty()
        includes.empty()
        excludes.empty()
    elif re.match(r'^[ \t]*# *elif', l): #l.startswith("#elif "):
      excludes.push(includes.pop())
      regex = r'^[ \t]*# *if +(!?)defined? *\(([_a-zA-Z0-9]+)\)'
      aux = re.sub(regex, r'\1\2', l)
      su = re.sub(r'\(.+\)', 'AAA', aux)
      if '||' in su:
        s = aux.replace(" ", "").replace("&&", "><")
      else:
        s = aux.replace(" ", "")
      includes.push(s)
      feats = featureList(includes, excludes)
      l = "void __endfeature_;\n" + "void __feature_" + feats + ';\n'
      ends = ends + 1
      begins = begins + 1
    elif re.match(r'^[ \t]*# *include', l): 
      if (not includes.isEmpty()) & (not excludes.isEmpty()):
        feats = featureList(includes, excludes)
        regex = r'^[ \t]*# *include +\"(.+)\"'
        s = re.sub(regex, r'\1', l)
        found = False
        for imp in imports:
          if imp['feature'] == feats:
            imp['includes'].append(s)
            found = True
        if not found:
          imports.append({"feature" : feats, "includes" : [s.replace('.h','.c')]})
    else:
      pass
    newContent.append(l)
  return imports, newContent, definesByFeats

def replaceDefines(lines, defDict, file):
  newContent = []
  j = 0
  for l in lines:
    newL = []
    if not re.match(r'^.*# *define ', l):
      for df in defDict:
        orig = df['tag']
        for d in df['defs']:
          feat = d[0]
          new = d[1]
          if '(' in orig:
            tag = re.sub(r'(.+)\(.+\)', r'\1', orig)
            newTag = re.sub(r'(.+)\(.+\)', r'\1', new)
            args = re.sub(r'.+\((.+)\)', r'\1', (orig.replace(' ', '')))
            newArgs = re.sub(r'.+\((.+)\)', r'\1', (new.replace(' ', '')))
            lst = args.split(',')
            i = 2
            for x in lst:
              args = args.replace(x,'(.+)')
              newArgs = newArgs.replace(x,'\\'+str(i))
              i+=1
            x = '(^|[^a-zA-Z0-9]+)' + tag + '\(' + args + '\)'
            y = '\\1' + newTag + '(' + newArgs + ')'
            z = re.sub(r''+x, r''+y, l)
            if z != l:
              newL += ['void __feature_' + feat + ';\n' , z , 'void __endfeature_' + feat + ';\n']
          else:
            rexp = r'(^|[^a-zA-Z0-9]+)' + orig + r'([^a-zA-Z0-9]+)'
            su = r'\1 ' + new + r'\2'
            x = re.sub(r''+rexp, r''+su, l)
            if x != l:
              newL += ['void __feature_' + feat + ';\n' , x , 'void __endfeature_' + feat + ';\n']
      if newL == []:
        newL = [l]
      #lines[j] = newL
      newContent += newL
      #if file.endswith('pb.c'):
        #print(newL)
      j += 1
  return newContent

def switchConditions(content):
  x = re.sub(r'\|\|', '|', content)
  y = re.sub(r'typedef ([a-zA-Z0-9 _]+) (uint32_t|__uint32_t);', '',x)
  
  return y

def main(dir, root, files, headers, mainFile, ignore_includes = True):
  allFiles = files + headers
  color_print('#First Step: #if* --> __feature_', color='green', bold=True)
  sigs, includes = [], []
  if phases[1]:
    for f in allFiles:
      file = root + f
      file_tmp = file + ".tmp.c"

      if not os.path.exists(os.path.dirname(file)):
        try:
          os.makedirs(os.path.dirname(file))
        except OSError as exc: # Guard against race condition
          if exc.errno != errno.EEXIST:
            raise

      if ignore_includes:
        outp = check_output('sed -r "s/#include *<.+>//g" ' + dir + f + " > " + file_tmp, shell=True).decode("utf-8")
        
      else:
        outp = check_output('cp ' + dir+f + " " + file_tmp, shell=True).decode("utf-8")

      if outp != '':
        print('ERROR! ' + str(outp))
        return

      outp = check_output('gcc -fpreprocessed -dD -E ' + file_tmp + ' > '+ file  + "; rm " + file_tmp, shell=True).decode("utf-8")
      
      with open(file) as fp:
        content = fp.readlines()
        includes, lines, featureDefines = transformFeatures(content)
        lines = replaceDefines(lines, featureDefines, file)
        
        #write transformed source code, with system libraries ignored
        new_content = ''
        if ignore_features == 1:
          for l in content:
            new_content +=  re.sub(r'(# *include *< *stdio.h *>)', r'//\1', l)
        else:
          for l in lines:
            new_content +=  re.sub(r'(# *include *< *stdio.h *>)', r'//\1', l)
        fp = open(file, 'w+')
        fp.write(new_content)
        fp.close()
    
  color_print('#Second Step: CPP on all files', color='green', bold=True)    # this can't work as it is in a stable version for variability management (see paper note)
  if phases[2]:
    allFiles = list(map(lambda x: root + '/' + x, allFiles))
    clean_files(allFiles)
    for f in allFiles:
      tmp_name = re.sub(r'(.+)\..+', r'\1.scc1', f)
      outp = check_output('cpp ' + f + ' > ' + tmp_name + '; cp ' + tmp_name + ' ' + f + '; rm ' + tmp_name , shell=True).decode("utf-8")
      if outp != '':
        print('ERROR! ' + str(outp))
        return
      #remove unparsable keywords
      reg = r'^#.+\n|__asm__ *\(.+\)|__extension__ *|__inline *|__restrict *|__attribute__ *\(\(.+\)\)'
      #reg2 = r'^#.+\n|__asm__ *\(.+\)|__extension__ *|__inline *|__restrict *|__attribute__ *\(\(__\w+__ *(\([_, a-zA-Z0-9]+\))?( *, *__\w+__ *(\([_, a-zA-Z0-9]+\))?)*\)\)'
      with open(f, "r") as sources:
        lines = sources.readlines()
      with open(f, "w") as sources:
        last_line = ""
        for line in lines:
          if last_line != "":
            line = last_line + line
            last_line = ""
          if line.endswith(",\n"):
            last_line = line.replace("\n", "")
            continue
          x = re.sub(reg, '', line)
          sources.write(x) # re.sub(reg2, r'\1\2\3 var\4', x))
    
    #Get ALL the function signatures from ALL the headers
    #this can't work as it is in a stable version for variability management (see paper note)
    sigs = []
    new_names = set([])
    for h in headers:
      sigs, new_names = getFuncSig(root + '/' + h, sigs, new_names)

  color_print('#Third Step: Find function source', color='green', bold=True)
  ##ast = fetchFunctions(root + '/' + new_name, files, includes)
  ast = None
  ast = fetchFunctions(root + '/' + mainFile, allFiles, includes, sigs, phases[3])
  #ast.show()
    
  color_print('#Fourth Step: Expand \'__feature_\' within function', color='green', bold=True)
  if phases[4]:
    ast = expandFeatures(ast)
    ast.show()
    print(generator.visit(ast))
  
  color_print('#Fifth Step: Use cilly to reduce C to CIL', color='green', bold=True)
  if phases[5]:
    strg = '#define __sigset_t int\n#define uint32_t int\n#define stderr 2\n#define strerror(x) \"error\"\n' + switchConditions(generator.visit(ast))
  
    with open(root+'/'+mainFile, 'w') as fp:
      fp.write(strg)
    
    final = root + '/' + re.sub(r'(.+)\.c', r'\1.scc', mainFile)
    tmp_name = final + '.tmp'
    
    outp = check_output('cpp ' + root + '/' + mainFile + ' > ' + tmp_name + ' ; cilly.native --noPrintLn --keepunused ' + tmp_name + ' --out ' + final + '; rm ' + tmp_name, shell=True).decode("utf-8")
    if outp != '':
      print('ERROR! ' + str(outp))
      return

    #At last! Let's clean the comments generated by CIL
    reg = r' ?/\* .+ \*/ ?'
    with open(final, "r") as sources:
      lines = sources.readlines()
      with open(final, "w") as sources:
        for line in lines:
          x = re.sub(reg, '', line)
          sources.write(x)
    #-#
  return

def filesByTypeInDir(path, suffix):
  lst = []
  for root, dirs, files in os.walk(path):
    for file in files:
      if file.endswith(suffix):
        lst.append(re.sub(path, '', os.path.join(root, file)))
  return lst


if __name__ == '__main__':
  #this script is expecting: 
  #  (1) a dir path (including the source files for the SPL),
  #  (2) a filename, where the main function is declared, withing the source in (1).
  mainFile = ""
  if len(sys.argv) >= 3:
    directory = str(sys.argv[1])
    mainFile = str(sys.argv[2])
    root = directory + '/' + '_joined_'
    if not os.path.exists(root):
      os.makedirs(root)
    else:
      color_print('#Initial Step: Cleaning Previous Files', color='green', bold=True)
      if phases[0]:
        for f in os.listdir(root):
          p = call(['rm', '-r', root+'/'+f])
    files = filesByTypeInDir(directory, '.c')
    headers = filesByTypeInDir(directory, '.h')
    main(directory, root, files, headers, mainFile)
  print("finished!")

