{-# LANGUAGE DeriveGeneric, DeriveAnyClass #-}

module Analysis.Flow
(
  join, 
  preBB, 
  initVal, 
  expVal, 
  xor, 
  execW, 
  eval, 
  Value(..),
  MapM,
  Visited,
  isAllFalse, 
  getMaxIter,
  loopNodes, 
  allLoopBounds, 
  allLoopBounds', 
  checkCFG, 
  checkCFG'
) where

import Safe
import Data.List
import Data.Function
import Data.Maybe
import Control.Arrow ((***))
import Control.DeepSeq
import Control.Applicative ((<|>))

import qualified Data.String.Utils as SU
import Language.CIL.Types
import Language.CIL.StmtCore
import Analysis.CFG
import Data.Char
import Text.Regex (mkRegex, splitRegex, subRegex, matchRegex)
import Language.C.Data.Position (nopos)
import GHC.Generics (Generic)

import Analysis.Bounds
----
--The analysis abstract datatype
data Value = Unknown                        -- Undetermined Value
           | NULL                           -- 'null' Value
           | ValI !Int                       -- 'int' Value
           | ValD !Double                    -- 'double | float' Value
           | ValC !Char                      -- 'char' Value
           | ValS String                    -- 'String <=> char[]' Value
           | Pointer Name                   -- Pointer to other var
           | ValArr [Value]                 -- Array
           | ValStruct [(Name, [Value])]    -- Here, a struct can have several values for each tag. This happens to make it easier to update struct values later on.
    deriving (Show, Eq, Read, NFData, Generic)

-- Comparison operators

(|<|) :: Value -> Value -> Bool
(ValI a) |<| (ValI b) = a < b
(ValD a) |<| (ValD b) = a < b
(ValI a) |<| (ValD b) = fromIntegral a < b
(ValD a) |<| (ValI b) = a < fromIntegral b
(ValC a) |<| (ValC b) = a < b
(ValC a) |<| (ValI b) = a < chr b
(ValI a) |<| (ValC b) = chr a < b
Unknown |<| _ = True
_ |<| Unknown = True
_ |<| _ = False

(|>|) :: Value -> Value -> Bool
(ValI a) |>| (ValI b) = a > b
(ValD a) |>| (ValD b) = a > b
(ValI a) |>| (ValD b) = fromIntegral a > b
(ValD a) |>| (ValI b) = a < fromIntegral b
(ValC a) |>| (ValC b) = a > b
(ValC a) |>| (ValI b) = a > chr b
(ValI a) |>| (ValC b) = chr a > b
Unknown |>| _ = True
_ |>| Unknown = True
_ |>| _ = False

(|<=|) :: Value -> Value -> Bool
(ValI a) |<=| (ValI b) = a <= b
(ValD a) |<=| (ValD b) = a <= b
(ValI a) |<=| (ValD b) = fromIntegral a <= b
(ValD a) |<=| (ValI b) = a <= fromIntegral b
(ValC a) |<=| (ValC b) = a <= b
(ValC a) |<=| (ValI b) = a <= chr b
(ValI a) |<=| (ValC b) = chr a <= b
Unknown |<=| _ = True
_ |<=| Unknown = True
_ |<=| _ = False

(|>=|) :: Value -> Value -> Bool
(ValI a) |>=| (ValI b) = a >= b
(ValD a) |>=| (ValD b) = a >= b
(ValI a) |>=| (ValD b) = fromIntegral a >= b
(ValD a) |>=| (ValI b) = a >= fromIntegral b
(ValC a) |>=| (ValC b) = a >= b
(ValC a) |>=| (ValI b) = a >= chr b
(ValI a) |>=| (ValC b) = chr a >= b
Unknown |>=| _ = True
_ |>=| Unknown = True
_ |>=| _ = False

(|==|) :: Value -> Value -> Bool
(ValI a) |==| (ValI b) = a == b
(ValD a) |==| (ValD b) = a == b
(ValI a) |==| (ValD b) = fromIntegral a == b
(ValD a) |==| (ValI b) = a == fromIntegral b
(ValC a) |==| (ValC b) = a == b
(ValC a) |==| (ValI b) = a == chr b
(ValI a) |==| (ValC b) = chr a == b
NULL |==| NULL = True
Unknown |==| _ = True
_ |==| Unknown = True
_ |==| _ = False

(|/=|) :: Value -> Value -> Bool
(ValI a) |/=| (ValI b) = a /= b
(ValD a) |/=| (ValD b) = a /= b
(ValI a) |/=| (ValD b) = fromIntegral a /= b
(ValD a) |/=| (ValI b) = a /= fromIntegral b
(ValC a) |/=| (ValC b) = a /= b
(ValC a) |/=| (ValI b) = a /= chr b
(ValI a) |/=| (ValC b) = chr a /= b
Unknown |/=| _ = True
_ |/=| Unknown = True
_ |/=| _ = False

instance Num Value where
  (+) (ValI a) (ValI b) = ValI (a+b)
  (+) (ValD a) (ValD b) = ValD (a+b)
  (+) (ValI a) (ValD b) = ValD (fromIntegral a+b)
  (+) (ValD a) (ValI b) = ValD (a+fromIntegral b)
  (+) (ValC a) (ValC b) = ValC (chr(ord a+ord b))
  (+) (ValC a) (ValI b) = ValC (chr(ord a+b))
  (+) (ValI a) (ValC b) = ValC (chr(a+ord b))
  (+) NULL NULL = NULL
  (+) NULL a = a
  (+) a NULL = a
  (+) _ _ = Unknown
  (*) (ValI a) (ValI b) = ValI (a*b)
  (*) (ValD a) (ValD b) = ValD (a*b)
  (*) (ValI a) (ValD b) = ValD (fromIntegral a*b)
  (*) (ValD a) (ValI b) = ValD (a*fromIntegral b)
  (*) (ValC a) (ValC b) = ValC (chr(ord a*ord b))
  (*) (ValC a) (ValI b) = ValC (chr(ord a*b))
  (*) (ValI a) (ValC b) = ValC (chr(ord b*a))
  (*) NULL NULL = NULL
  (*) NULL a = a
  (*) a NULL = a
  (*) _ _ = Unknown
  abs x = x
  signum (ValI a) = ValI (signum a)
  signum (ValD a) = ValD (signum a)
  signum (ValC a) = ValC (chr 1)
  signum NULL = NULL
  signum _ = Unknown
  fromInteger a = ValI (fromIntegral a)
  negate (ValI a) = ValI (negate a)
  negate (ValD a) = ValD (negate a)
  negate NULL = NULL
  negate _ = Unknown

type Var = (String, [Value])

-- first version of struct M
type MapM = [Var]

type Visited = [(Node, Double, [Var])]

maxiter = 200000000
valImLeft = [NULL]
valImRight = [NULL]

join :: (Eq a, Ord a) => [[(a,[b])]] -> [(a,[b])]
join = map ((head *** concat) . unzip)
     . groupBy ((==) `on` fst)
     . sortBy (compare `on` fst)
     . concat

ff :: [Var] -> [Expr] -> [Var]
ff vars [] = []
ff vars (v@(Var n _):t) = let vals = snd . (headNote "ff") . filter(\(name, val) -> name == n) $ vars
                              tp = isPointer ((headNote "ff") vals)
                              res = if tp
                                    then resetVar vars n:map (resetVar vars) (filterPointers vals)
                                    else []
                          in res ++ ff vars t
ff vars ((Adr (Var n _) _):t) = (resetVar vars n):ff vars t
ff vars ((Add (Var n _) e _):t) = let vals = snd . (headNote "ff") . filter(\(name,val) -> name == n) $ vars
                                      tp = isPointer ((headNote "ff") vals)
                                      res = if tp
                                            then resetVar vars n:map (resetVar vars) (filterPointers vals)
                                            else []
                                  in res ++ ff vars t
ff vars ((Add (MemInd (Var n _) _ _) e _):t) = let vals = snd . (headNote "ff") . filter(\(name,val) -> name == n) $ vars
                                               in map (resetVar vars) (filterPointers vals) ++ ff vars t
ff vars (_:t) = ff vars t

isPointer :: Value -> Bool
isPointer (ValArr _) = True
isPointer (Pointer _) = True
isPointer _ = False

initArray :: Int -> Value -> [Value]
initArray s tp = [ValArr (replicate s tp)]

initMatrix :: Int -> Int -> Value -> [Value]
initMatrix s1 s2 tp = [ValArr (concat . replicate s1 $ (initArray s2 tp))]

resetVar :: [Var] -> Name -> Var
resetVar vars n = let vals = snd . (headNote ("resetVar: '"++n++"'")) . filter (\(a,b) -> a == n) $ vars
                  in (n, [resetByType (h vals)])
                where h a | a == [] = Unknown
                          | otherwise = (headNote "resetVar") a

resetVars :: [Var] -> [Var]
resetVars vars = map (\(n,v) -> (n, [resetByType (h v)])) vars
               where h a | a == [] = Unknown
                         | otherwise = (headNote "resetVar") a


resetByType :: Value -> Value
resetByType (NULL) = NULL
resetByType (ValC _) = ValC 'a'
resetByType (ValI _) = ValI 0
resetByType (ValD _) = ValD 0.0
resetByType (Pointer p) = Pointer p   --Unknown
resetByType (ValArr l) = ValArr (map resetByType l)
resetByType (ValStruct l) = ValStruct (map (\(x,y) -> (x, [resetByType ((headNote "resetByType") y)])) l)
resetByType _ = Unknown

varType :: [(Name, Type)] -> Type -> Value
varType _ (Void) = NULL
varType l (Ptr t) = Pointer ""
varType l (Array s t) = ValArr [(varType l t)] -- not 100% true
varType _ (Int8) = ValC 'a'
varType _ (Int16) = ValI 0
varType _ (Int32) = ValI 0
varType _ (Float) = ValD 0.0
varType _ (Double) = ValD 0.0
varType l (Struct s) = ValStruct (map(\(a,b) -> (a, [varType l b])) s)
varType l (StructRef n) = varType l (snd . (headNote "varType") . filter (\(a,b) -> a == n) $ l)
varType l (TypedefRef n) = varType l (snd . (headNote "varType") . filter (\(a,b) -> a == n) $ l)
varType _ _ = NULL

fetchTypes :: Gr BB Flow -> [(Name, Type)]
fetchTypes gr = let nodes = labNodes gr
                    select (i,a) = case a of BB (TypeDecl n t _, _, _, _) -> True
                                             _ -> False
                    all = filter select nodes
                    fetch b@(i, bb) = case bb of BB ((TypeDecl n (Typedef (StructRef ref)) _), _, _, _) -> (n, snd . fetch . (headNote "fetchTypes") . filter (\(i, BB((TypeDecl t _ _), _, _, _)) -> t == ref) $ all)
                                                 BB ((TypeDecl n t _), _, _, _) -> (n, t)
                in map (\(i,j) -> fetch (i,j)) all

varInput :: MapM -> Name -> Var
varInput m n = fetch . filter(\(name,val) -> name == n) $ m
             where fetch l | l == [] = (n,[Unknown])
                           | otherwise = (n,snd . (headNote "varInput") $ l)

nubValues :: [Var] -> [Var]
nubValues vars = map (\(a,b) -> (a, nub b)) vars

eval :: Gr BB Flow -> Visited -> MapM -> (Int, [Var]) -> Configuration -> LNode BB -> [Var]
eval gr _ _ _ _ (i, Begin _ ) = []
eval gr visited m lVars conf node@(i, bb@(BB(s, _, c, eb))) = let types = fetchTypes gr
                                                                  r = if chkConfig c conf 
                                                                      then case s of VariableDef name tp (Just init) _ -> let x = join(map (eval gr visited m lVars conf) (preBB gr i))
                                                                                                                          in (name, initVal x init):x
                                                                                     VariableDef name (Array sz1 (Array sz2 t)) (Nothing) _ -> let x = join(map (eval gr visited m lVars conf) (preBB gr i))
                                                                                                                                                   tp = varType types t
                                                                                                                                               in (name, initMatrix sz1 sz2 tp):x
                                                                                     VariableDef name (Array size t) (Nothing) _ -> let x = join(map (eval gr visited m lVars conf) (preBB gr i))
                                                                                                                                        tp = varType types t
                                                                                                                                    in (name, initArray size tp):x
                                                                                     VariableDef name (Ptr t) (Nothing) p -> let x = join(map (eval gr visited m lVars conf) (preBB gr i))
                                                                                                                                 tp = varType types t
                                                                                                                                 ref = [("pointedBy_" ++ name, [resetByType tp]), (name, createPointer [] (Var ("pointedBy_" ++ name) p))]
                                                                                                                             in ref++x
                                                                                     VariableDef name tp (Nothing) _ -> let x = join(map (eval gr visited m lVars conf) (preBB gr i))
                                                                                                                        in (name, [resetByType (varType types tp)]):x
                                                                                     AssignExpr ex1 ex2 _ -> let x = join(map (eval gr visited m lVars conf) (preBB gr i))
                                                                                                             in nubValues . nubBy (\(a,_) (b,_) -> a == b) $ (evalAssign x s ++ x)
                                                                                     StmtApply (Apply (Var n _) el) _ -> let x = join(map (eval gr visited m lVars conf) (preBB gr i))
                                                                                                                             v | matchRegex (mkRegex "srand") n /= Nothing = varInput m "pointedBy_imright" : varInput m "pointedBy_imleft" : (ff x el)
                                                                                                                               | otherwise = (ff x el)
                                                                                                                         in nubValues . nubBy (\(a,_) (b,_) -> a == b) $ (v ++ x)
                                                                                     AssignApply (Var n _) (Apply (Var f _) el)  _-> let x = join(map (eval gr visited m lVars conf) (preBB gr i))
                                                                                                                                         v | matchRegex (mkRegex "fread") f /= Nothing = (n, [ValI 128]) -- used for a specific loop bound
                                                                                                                                           | otherwise = resetVar x n
                                                                                                                                     in nubValues . nubBy (\(a,_) (b,_) -> a == b) $ (v : (ff x el) ++ x)
                                                                                     While exp stm _ -> let vis = dropWhile (\(a,_,_) -> a /= i) visited
                                                                                                            res | vis /= [] = third . head $ vis
                                                                                                                | otherwise = let x = join(map (eval gr visited m lVars conf) (filter (\(x,y) -> x < i) (preBB gr i)))
                                                                                                                                  r = if i == (fst lVars)
                                                                                                                                      then if snd lVars == [] then x else snd lVars
                                                                                                                                      else if not (isNested gr (fst lVars) i)
                                                                                                                                           then snd (execW gr visited m node conf maxiter 0 x False)
                                                                                                                                           else x
                                                                                                                              in r
                                                                                                        in res
                                                                                     _ -> join(map (eval gr visited m lVars conf) (preBB gr i))
                                                                      else join(map (eval gr visited m lVars conf) (preBB gr i))
                                                           in force r

isPredOf :: Gr BB Flow -> Int -> Int -> Bool
isPredOf cfg i j = let p = preBB cfg j
                       r = length (filter (\(a,_) -> a == i) p) > 0
                   in r

--isNested :: Gr BB Flow -> Int -> Int -> Bool
--isNested cfg i j = (j `elem` (reachable i cfg)) && (i > j)

evalAssign :: [Var] -> Stmt -> [Var]
evalAssign lVars (AssignExpr (Var n _) exp _) = let tp = (headNote "evalAssign 1-1") . snd . (headNote ("evalAssign 1-2"++n)) . filter (\(tag, val) -> tag == n) $ lVars
                                                    var = case tp of Pointer _ -> [(n, createPointer lVars exp)]
                                                                     _ -> [(n, expVal lVars exp)]
                                                in var
evalAssign lVars (AssignExpr (Ind (Add v@(Var n _) pos _) _) src _) = evalAssign lVars (AssignExpr (Index v pos nopos) src nopos)
evalAssign lVars (AssignExpr (Index (Var n _) exp _) src _) = let i = map intVal . (expVal lVars) $ exp
                                                                  srcVal = expVal lVars src
                                                                  var = (headNote "evalAssign 2") . filter (\(tag, v) -> tag == n) $ lVars
                                                                  values = snd var
                                                                  ptrs = filterPointers values
                                                                  res1 = if length(ptrs) >= 2
                                                                         then map (\x -> (x, findPointerValues lVars x ++ updateArray(findPointerValues lVars x) i srcVal)) ptrs
                                                                         else map (\x -> (x, updateArray(findPointerValues lVars x) i srcVal)) ptrs
                                                                  res2 = [(n, updateArray values i srcVal)]
                                                              in res1 ++ res2
evalAssign lVars (AssignExpr (Ind (Add (Index v@(Var n _) exp1 _) pos _) _) src _) = evalAssign lVars (AssignExpr (Index (Index v exp1 nopos) pos nopos) src nopos)
evalAssign lVars (AssignExpr (Index (Index (Var n _) exp1 _) exp2 _) src _) = let i = map intVal . (expVal lVars) $ exp1
                                                                                  j = map intVal . (expVal lVars) $ exp2
                                                                                  srcVal = expVal lVars src
                                                                                  var = (headNote "evalAssign 3") . filter (\(tag, v) -> tag == n) $ lVars
                                                                                  values = snd var
                                                                                  ptrs = filterPointers values
                                                                                  res1 = if length(ptrs) >= 2
                                                                                         then map (\x -> (x, findPointerValues lVars x ++ updateMatrix (findPointerValues lVars x) i j srcVal)) ptrs
                                                                                         else map (\x -> (x, updateMatrix(findPointerValues lVars x) i j srcVal)) ptrs
                                                                                  res2 = [(n, updateMatrix values i j srcVal)]
                                                                              in res1 ++ res2
evalAssign lVars (AssignExpr (Ind (Var n _) _) exp _) = let vals = snd . (headNote "evalAssign 4") . filter (\(tag, v) -> tag == n) $ lVars
                                                            srcVal = expVal lVars exp
                                                            ptrs = filterPointers vals
                                                            res1 = if length(ptrs) >= 2
                                                                   then map (\x -> (x, findPointerValues lVars x ++ srcVal)) ptrs
                                                                   else map (\x -> (x, srcVal)) ptrs
                                                            res2 = [(n, updateArray vals [0] srcVal)]
                                                        in res1 ++ res2
evalAssign lVars (AssignExpr (Ind (Index (Var n _) exp1 _) _) exp2 _) = let vals = snd . (headNote "evalAssign 5") . filter (\(tag, v) -> tag == n) $ lVars
                                                                            srcVal = expVal lVars exp2
                                                                            ptrs = filterPointers vals
                                                                            res1 = if length(ptrs) >= 2
                                                                                   then map (\x -> (x, findPointerValues lVars x ++ srcVal)) ptrs
                                                                                   else map (\x -> (x, srcVal)) ptrs
                                                                            res2 = [(n, updateMatrix vals (map intVal (expVal lVars exp1)) [0] srcVal)]
                                                                        in res1 ++ res2
-- every damm combination for composed types...
--arr.val
evalAssign lVars (AssignExpr (Mem (Var n _) tag _) exp2 _) = let srcVal = expVal lVars exp2
                                                                 vals = snd . (headNote "evalAssign 6") . filter (\(name, v) -> name == n) $ lVars
                                                             in [(n, updateStruct vals tag srcVal)]
--arr.val[i]
evalAssign lVars (AssignExpr (Index (Mem (Var n _) tag _) pos1 _) exp _) = let srcVal = expVal lVars exp
                                                                               p = map intVal (expVal lVars pos1)
                                                                               vals = snd . (headNote "evalAssign 7") . filter (\(name, v) -> name == n) $ lVars
                                                                           in [(n, updateArrayinStruct vals tag p srcVal)]
--arr.val[i][j]
evalAssign lVars (AssignExpr (Index (Index (Mem (Var n _) tag _) pos1 _) pos2 _) exp _) = let srcVal = expVal lVars exp
                                                                                              p1 = map intVal (expVal lVars pos1)
                                                                                              p2 = map intVal (expVal lVars pos2)
                                                                                              vals = snd . (headNote "evalAssign 8") . filter (\(name, v) -> name == n) $ lVars
                                                                                          in [(n, updateMatrixinStruct vals tag p1 p2 srcVal)]
--arr[i].val
evalAssign lVars (AssignExpr (Mem (Index (Var n _) pos1 _) tag _) exp _) = let srcVal = expVal lVars exp
                                                                               p = map intVal (expVal lVars pos1)
                                                                               vals = snd . (headNote "evalAssign 9") . filter (\(name, v) -> name == n) $ lVars
                                                                           in [(n, updateStructArray vals p tag srcVal)]
--arr[i].val[j]
evalAssign lVars (AssignExpr (Index (Mem (Index (Var n _) pos1 _) tag _) pos2 _) exp _) = let srcVal = expVal lVars exp
                                                                                              p1 = map intVal (expVal lVars pos1)
                                                                                              p2 = map intVal (expVal lVars pos2)
                                                                                              vals = snd . (headNote "evalAssign 10") . filter (\(name, v) -> name == n) $ lVars
                                                                                              vv = concat . map (\x -> updateArrayinStruct [x] tag p2 srcVal) $ (arrayPosValue lVars vals p1)    --potencial performance problem
                                                                                          in [(n, updateArray vals p1 vv)]
--arr[i].val[j][k]
evalAssign lVars (AssignExpr (Index (Index (Mem (Index (Var n _) pos1 _) tag _) pos2 _) pos3 _) exp _) = let srcVal = expVal lVars exp
                                                                                                             p1 = map intVal (expVal lVars pos1)
                                                                                                             p2 = map intVal (expVal lVars pos2)
                                                                                                             p3 = map intVal (expVal lVars pos3)
                                                                                                             vals = snd . (headNote "evalAssign 11") . filter (\(name, v) -> name == n) $ lVars
                                                                                                             vv = concat . map (\x -> updateMatrixinStruct [x] tag p2 p3 srcVal) $ (arrayPosValue lVars vals p1)    --potencial performance problem
                                                                                                         in [(n, updateArray vals p1 vv)]
--arr[i][j].val
evalAssign lVars (AssignExpr (Mem (Index (Index (Var n _) pos1 _) pos2 _) tag _) exp _) = let srcVal = expVal lVars exp
                                                                                              p1 = map intVal (expVal lVars pos1)
                                                                                              p2 = map intVal (expVal lVars pos2)
                                                                                              vals = snd . (headNote "evalAssign 12") . filter (\(name, v) -> name == n) $ lVars
                                                                                          in [(n, updateStructMatrix vals p1 p2 tag srcVal)]
--arr[i][j].val[k]
evalAssign lVars (AssignExpr (Index (Mem (Index (Index (Var n _) pos1 _) pos2 _) tag _) pos3 _) exp _) = let srcVal = expVal lVars exp
                                                                                                             p1 = map intVal (expVal lVars pos1)
                                                                                                             p2 = map intVal (expVal lVars pos2)
                                                                                                             p3 = map intVal (expVal lVars pos3)
                                                                                                             vals = snd . (headNote "evalAssign 13") . filter (\(name, v) -> name == n) $ lVars
                                                                                                             vv = concat . map (\x -> updateArrayinStruct [x] tag p3 srcVal) $ (arrayPosValue lVars (arrayPosValue lVars vals p1) p2)    --potencial performance problem
                                                                                                         in [(n, updateMatrix vals p1 p2 vv)] 
--arr[i][j].val[k][l]
evalAssign lVars (AssignExpr (Index (Index (Mem (Index (Index (Var n _) pos1 _) pos2 _) tag _) pos3 _) pos4 _) exp _) = let srcVal = expVal lVars exp
                                                                                                                            p1 = map intVal (expVal lVars pos1)
                                                                                                                            p2 = map intVal (expVal lVars pos2)
                                                                                                                            p3 = map intVal (expVal lVars pos3)
                                                                                                                            p4 = map intVal (expVal lVars pos4)
                                                                                                                            vals = snd . (headNote "evalAssign 14") . filter (\(name, v) -> name == n) $ lVars
                                                                                                                            vv = concat . map (\x -> updateMatrixinStruct [x] tag p3 p4 srcVal) $ (arrayPosValue lVars (arrayPosValue lVars vals p1) p2)    --potencial performance problem
                                                                                                                        in [(n, updateMatrix vals p1 p2 vv)] 
--arr -> val
evalAssign lVars (AssignExpr (MemInd (Var n _) tag _) exp _) = let srcVal = expVal lVars exp
                                                                   vals = snd . (headNote "evalAssign 15") . filter (\(name, v) -> name == n) $ lVars
                                                                   ptrs = filterPointers vals
                                                                   res = if length(ptrs) >= 2
                                                                         then map (\x -> (x, updateStruct' (findPointerValues lVars x) tag srcVal)) ptrs
                                                                         else map (\x -> (x, updateStruct (findPointerValues lVars x) tag srcVal)) ptrs
                                                               in res
--arr -> val[i]
evalAssign lVars (AssignExpr (Ind (Add (MemInd (Var n _) tag _) pos1 _) _) exp _) = evalAssign lVars (AssignExpr (Index (MemInd (Var n nopos) tag nopos) pos1 nopos) exp nopos)
evalAssign lVars (AssignExpr (Index (MemInd (Var n _) tag _) pos1 _) exp _) = let srcVal = expVal lVars exp
                                                                                  vals = snd . (headNote "evalAssign 16") . filter (\(name, v) -> name == n) $ lVars
                                                                                  p1 = map intVal (expVal lVars pos1)
                                                                                  ptrs = filterPointers vals
                                                                                  res = if length(ptrs) >= 2
                                                                                        then map (\x -> (x, updateArrayinStruct' (findPointerValues lVars x) tag p1 srcVal)) ptrs
                                                                                        else map (\x -> (x, updateArrayinStruct (findPointerValues lVars x) tag p1 srcVal)) ptrs
                                                                              in res
--arr -> val[i][j]
evalAssign lVars (AssignExpr (Index (Index (MemInd (Var n _) tag _) pos1 _) pos2 _) exp _) = let srcVal = expVal lVars exp
                                                                                                 vals = snd . (headNote "evalAssign 17") . filter (\(name, v) -> name == n) $ lVars
                                                                                                 p1 = map intVal (expVal lVars pos1)
                                                                                                 p2 = map intVal (expVal lVars pos2)
                                                                                                 ptrs = filterPointers vals
                                                                                                 res = if length(ptrs) >= 2
                                                                                                       then map (\x -> (x, updateMatrixinStruct' (findPointerValues lVars x) tag p1 p2 srcVal)) ptrs
                                                                                                       else map (\x -> (x, updateMatrixinStruct (findPointerValues lVars x) tag p1 p2 srcVal)) ptrs
                                                                                             in res
--(*arr)->val
evalAssign lVars (AssignExpr (MemInd (Ind (Var n _) _) tag _) exp _) = let vals = snd . (headNote "evalAssign 18") . filter (\(name, v) -> name == n) $ lVars
                                                                           ptrs = filterPointers vals
                                                                           res = if length(ptrs) >= 2
                                                                                 then map (\x -> (x, findPointerValues lVars x):evalAssign lVars (AssignExpr (MemInd (Var x nopos) tag nopos) exp nopos)) ptrs
                                                                                 else map (\x -> evalAssign lVars (AssignExpr (MemInd (Var x nopos) tag nopos) exp nopos)) ptrs
                                                                       in concat res
--(*arr)->val[i]
evalAssign lVars (AssignExpr (Ind (Add (MemInd (Ind (Var n _) _) tag _) pos1 _) _) exp _) = evalAssign lVars (AssignExpr (Index (MemInd (Ind (Var n nopos) nopos) tag nopos) pos1 nopos) exp nopos)
evalAssign lVars (AssignExpr (Index (MemInd (Ind (Var n _) _) tag _) pos1 _) exp _) = let vals = snd . (headNote "evalAssign 19") . filter (\(name, v) -> name == n) $ lVars
                                                                                          ptrs = filterPointers vals
                                                                                          res = if length(ptrs) >= 2
                                                                                                then map (\x -> (x, findPointerValues lVars x):evalAssign lVars (AssignExpr (Index (MemInd (Var x nopos) tag nopos) pos1 nopos) exp nopos)) ptrs
                                                                                                else map (\x -> evalAssign lVars (AssignExpr (Index (MemInd (Var x nopos) tag nopos) pos1 nopos) exp nopos)) ptrs
                                                                                      in concat res
--(*arr)->val[i][j]
evalAssign lVars (AssignExpr (Index (Index (MemInd (Ind (Var n _) _) tag _) pos1 _) pos2 _) exp _) = let vals = snd . (headNote "evalAssign 20") . filter (\(name, v) -> name == n) $ lVars
                                                                                                         ptrs = filterPointers vals
                                                                                                         res = if length(ptrs) >= 2
                                                                                                               then map (\x -> (x, findPointerValues lVars x):evalAssign lVars (AssignExpr (Index (Index (MemInd (Var x nopos) tag nopos) pos1 nopos) pos2 nopos) exp nopos)) ptrs
                                                                                                               else map (\x -> evalAssign lVars (AssignExpr (Index (Index (MemInd (Var x nopos) tag nopos) pos1 nopos) pos2 nopos) exp nopos)) ptrs
                                                                                                     in concat res

evalAssign lVars _ = []

filterPointers :: [Value] -> [Name]
filterPointers [] = []
filterPointers (x:xs) = case x of Pointer "" -> filterPointers xs
                                  Pointer p -> p:filterPointers xs
                                  _ -> filterPointers xs

--updateStructMatrix :: [Value] -> 

updateStructArray :: [Value] -> [Int] -> Name -> [Value] -> [Value]
updateStructArray [] _ _ _ = []
updateStructArray (v:vals) indx tag newV = case v of ValArr oldV -> ((map (\(i,v) -> ValArr (insertAt (i, (headNote "updateStructArray - var '?'") (updateStruct [(valAt oldV i)] tag [v])) 0 NULL oldV))) $ (cartProd indx newV)) ++ updateStructArray vals indx tag newV
                                                     Pointer _ -> (v:updateStructArray vals indx tag newV)
                                                     Unknown -> updateStructArray (initArray (maximum indx) (resetByType v)) indx tag newV ++ updateStructArray vals indx tag newV
                                                     _ -> (v:vals)

updateStructMatrix :: [Value] -> [Int] -> [Int] -> Name -> [Value] -> [Value]
updateStructMatrix [] _ _ _ _ = []
updateStructMatrix (v:vals) indx1 indx2 tag newV = case v of ValArr oldV -> let posVal = cartProd (cartProd indx1 indx2) newV
                                                                                res = map (\((i,j),v) -> ValArr (insertAt (i, (headNote "updateStuctMatrix - var '?'") (updateStructArray [(valAt oldV i)] [j] tag [v])) 0 NULL oldV)) $ posVal
                                                                            in res ++ updateStructMatrix vals indx1 indx2 tag newV
                                                             Pointer _ -> (v:updateStructMatrix vals indx1 indx2 tag newV)
                                                             Unknown -> updateStructMatrix (initMatrix (maximum indx1) (maximum indx2) (resetByType v)) indx1 indx2 tag newV ++ updateStructMatrix vals indx1 indx2 tag newV
                                                             _ -> (v:vals)

updateMatrixinStruct :: [Value] -> Name -> [Int] -> [Int] -> [Value] -> [Value]
updateMatrixinStruct [] _ _ _ _ = []
updateMatrixinStruct (v:vals) tag pos1 pos2 newVals = case v of ValStruct s -> ValStruct (map (\(x,y) -> if x == tag then (x, updateMatrix y pos1 pos2 newVals) else (x,y)) s) : updateMatrixinStruct vals tag pos1 pos2 newVals
                                                                _ -> v:updateMatrixinStruct vals tag pos1 pos2 newVals

updateMatrixinStruct' :: [Value] -> Name -> [Int] -> [Int] -> [Value] -> [Value]
updateMatrixinStruct' [] _ _ _ _ = []
updateMatrixinStruct' (v:vals) tag pos1 pos2 newVals = case v of ValStruct s -> ValStruct (map (\(x,y) -> if x == tag then (x, y ++ updateMatrix y pos1 pos2 newVals) else (x,y)) s) : updateMatrixinStruct' vals tag pos1 pos2 newVals
                                                                 _ -> v:updateMatrixinStruct' vals tag pos1 pos2 newVals

updateArrayinStruct :: [Value] -> Name -> [Int] -> [Value] -> [Value]
updateArrayinStruct [] _ _ _ = []
updateArrayinStruct (v:vals) tag pos newVals = case v of ValStruct s -> ValStruct (map (\(x,y) -> if x == tag then (x, updateArray y pos newVals) else (x,y)) s) : updateArrayinStruct vals tag pos newVals
                                                         _ -> v:updateArrayinStruct vals tag pos newVals

updateArrayinStruct' :: [Value] -> Name -> [Int] -> [Value] -> [Value]
updateArrayinStruct' [] _ _ _ = []
updateArrayinStruct' (v:vals) tag pos newVals = case v of ValStruct s -> ValStruct (map (\(x,y) -> if x == tag then (x, y ++ updateArray y pos newVals) else (x,y)) s) : updateArrayinStruct' vals tag pos newVals
                                                          _ -> v:updateArrayinStruct' vals tag pos newVals


updateStruct :: [Value] -> Name -> [Value] -> [Value]
updateStruct [] _ _ = []
updateStruct (v:vals) tag newVals = case v of ValStruct s -> ValStruct (map (\(x,y) -> if x == tag then (x, newVals) else (x,y)) s) : updateStruct vals tag newVals
                                              _ -> v:updateStruct vals tag newVals

updateStruct' :: [Value] -> Name -> [Value] -> [Value]
updateStruct' [] _ _ = []
updateStruct' (v:vals) tag newVals = case v of ValStruct s -> ValStruct (map (\(x,y) -> if x == tag then (x, y ++ newVals) else (x,y)) s) : updateStruct' vals tag newVals
                                               _ -> v:updateStruct' vals tag newVals

updateMatrix :: [Value] -> [Int] -> [Int] -> [Value] -> [Value]
updateMatrix [] _ _ _ = []
updateMatrix (v:vals) indx1 indx2 newV = case v of ValArr oldV -> let posVal = cartProd (cartProd indx1 indx2) newV
                                                                      res = map (\((i,j),v) -> ValArr (insertAt (i, (headNote "updateMatrix - var '?'") (updateArray [(valAt oldV i)] [j] [v])) 0 NULL oldV)) $ posVal
                                                                  in res ++ updateMatrix vals indx1 indx2 newV
                                                   Pointer _ -> (v:updateMatrix vals indx1 indx2 newV)
                                                   Unknown -> updateMatrix (initMatrix (maximum indx1) (maximum indx2) (resetByType v)) indx1 indx2 newV ++ updateMatrix vals indx1 indx2 newV
                                                   _ -> (v:vals)

updateArray :: [Value] -> [Int] -> [Value] -> [Value]
updateArray [] _ _ = []
updateArray (v:vals) indx newV = case v of ValArr oldV -> ((map (\(i,v) -> ValArr (insertAt (i,v) 0 NULL oldV))) $ (cartProd indx newV)) ++ updateArray vals indx newV
                                           Pointer _ -> (v:updateArray vals indx newV)
                                           Unknown -> updateArray (initArray (maximum indx) (resetByType v)) indx newV ++ updateArray vals indx newV
                                           _ -> (v:vals)

--updateArray v _ _ = v

preBB :: Gr BB Flow -> Node -> [LNode BB]
preBB gr i = let p = map (\x -> (x,(lab gr x))) (pre gr i)
                 res = map (\(y,(Just l)) -> (y,l)) p
             in res

initVal :: [Var] -> Init -> [Value]
initVal vars (InitList init) = [Unknown]
initVal vars (Init exp) = expVal vars exp

expVal :: [Var] -> Expr -> [Value]
expVal vars (ConstInt i _) = [ValI i]
expVal vars (ConstFloat f _) = [ValD f]
expVal vars (ConstChar c _) = [ValC c]
expVal vars (ConstString s _) = [ValS s]
expVal vars (Var n _) = let x = filter (\(x,y) -> x==n) $ vars
                            y = if length x > 0
                                then snd . (headNote "") $ x
                                else [Unknown]
                        in y
expVal vars (Add exp1 exp2 _) = [a+b | a<-(expVal vars exp1), b<-(expVal vars exp2)]
expVal vars (Sub exp1 exp2 _) = [a-b | a<-(expVal vars exp1), b<-(expVal vars exp2)]
expVal vars (Mul exp1 exp2 _) = [a*b | a<-(expVal vars exp1), b<-(expVal vars exp2)]
expVal vars (Div exp1 exp2 _) = [divValue a b | a<-(expVal vars exp1), b<-(expVal vars exp2)]
expVal vars (Rmd exp1 exp2 _) = [remValue a b | a<-(expVal vars exp1), b<-(expVal vars exp2)]
expVal vars (Shl exp1 exp2 _) = [Unknown]
expVal vars (Shr exp1 exp2 _) = [Unknown]
expVal vars (Lt exp1 exp2 _) = let e1 = (expVal vars exp1)
                                   e2 = (expVal vars exp2)
                                   val = [evalExpr (|<|) a b | a<-e1, b<-e2]
                               in val
expVal vars (Gt exp1 exp2 _) = let e1 = (expVal vars exp1)
                                   e2 = (expVal vars exp2)
                                   val = [evalExpr (|>|) a b | a<-e1, b<-e2]
                               in val
expVal vars (Le exp1 exp2 _) = let e1 = (expVal vars exp1)
                                   e2 = (expVal vars exp2)
                                   val = [evalExpr (|<|) a b | a<-e1, b<-e2]
                               in val
expVal vars (Ge exp1 exp2 _) = let e1 = (expVal vars exp1)
                                   e2 = (expVal vars exp2)
                                   val = [evalExpr (|<|) a b | a<-e1, b<-e2]
                               in val
expVal vars (Eq exp1 exp2 _) = let e1 = (expVal vars exp1)
                                   e2 = (expVal vars exp2) 
                                   val = [evalExpr (|==|) a b | a<-e1, b<-e2]
                               in val
expVal vars (Neq exp1 exp2 _) = let e1 = (expVal vars exp1)
                                    e2 = (expVal vars exp2) 
                                    val = [evalExpr (|/=|) a b | a<-e1, b<-e2]
                                in val
expVal vars (And exp1 exp2 _) = let e1 = (expVal vars exp1)
                                    e2 = (expVal vars exp2)
                                    val = [evalExpr' (&&) a b | a<-e1, b<-e2]
                                in val
expVal vars (Xor exp1 exp2 _) = let e1 = (expVal vars exp1)
                                    e2 = (expVal vars exp2)
                                    val = [evalExpr' xor a b | a<-e1, b<-e2]
                                in val
expVal vars (Or exp1 exp2 _) = let e1 = (expVal vars exp1)
                                   e2 = (expVal vars exp2)
                                   val = [evalExpr' (||) a b | a<-e1, b<-e2]
                               in val
expVal vars (Adr exp1 _) = createPointer vars exp1
expVal vars (Ind (Adr (Var n p) _) _) = expVal vars (Var n p)
expVal vars (Ind v@(Var n _) _) = concat . map (\x -> refValue vars x) $ (expVal vars v)   -- *x
expVal vars (Ind (Index (Var n _) exp _) _) = [Unknown]  -- *var[exp] ; ref a row in a matrix
expVal vars (Ind (Add v@(Var n _) exp _) _) = arrayPosValue vars (expVal vars v) (map ((\x -> x-1) . intVal) (expVal vars exp))  -- *(var + exp)
expVal vars (Ind (Sub v@(Var n _) exp _) _) =  [Unknown] -- *(var - exp)
expVal vars (Ind (Add (Index (Var n _) exp1 _) exp2 _) _) = [Unknown]  -- *(var[exp1] + exp2)
expVal vars (Ind (Sub (Index v@(Var n _) exp1 _) exp2 _) _) = arrayPosValue vars values i  -- *(var[exp1] - exp2)
                                                            where i = map ((\x -> x-1) . intVal) (expVal vars exp2)
                                                                  values = arrayPosValue vars (expVal vars v) j
                                                                  j = map intVal (expVal vars exp1)
expVal vars (Minus exp1 _) = map negate (expVal vars exp1)
expVal vars (Comp exp1 _) = [Unknown]
expVal vars (Neg exp1 _) = map negExpr (expVal vars exp1)
expVal vars (Cast tp exp1 _) = expVal vars exp1
expVal vars (Index v@(Var n _) exp p) = let i = map intVal . (expVal vars) $ exp       -- array value
                                            values = expVal vars v
                                        in arrayPosValue vars values i
expVal vars (Index (Index v@(Var n _) exp1 _) exp2 _) = arrayPosValue vars values i   -- matrix value
                                                        where i = map intVal (expVal vars exp2)
                                                              values = arrayPosValue vars (expVal vars v) j
                                                              j = map intVal (expVal vars exp1)
expVal vars (Mem exp1 name _) = structVal (expVal vars exp1) name
expVal vars (MemInd exp1 name _) = let e = (expVal vars exp1)
                                       vals = concat . map (refValue vars) $ e
                                   in structVal e name
expVal vars (SizeT tp _) = [Unknown] --
expVal vars (SizeE exp1 _) = [Unknown] --
expVal vars _ = [Unknown] -- Just to be sure!

refValue :: [Var] -> Value -> [Value]
refValue vars v = case v of Pointer ref -> findPointerValues vars ref
                            ValArr arr -> arrayPosValue vars [v] [0]
                            _ -> [Unknown]

structVal :: [Value] -> Name -> [Value]
structVal [] _ = []
structVal (v:vals) tag = case v of ValStruct s -> let value = snd . (headNote ("structVal - var '"++tag++"'")) . filter (\(x,y) -> x == tag) $ s
                                                  in value ++ structVal vals tag
                                   _ -> Unknown : structVal vals tag

arrayPosValue :: [Var] -> [Value] -> [Int] -> [Value]
arrayPosValue _ [] _ = []
arrayPosValue vars (x:xs) pos = case x of ValArr lst -> map (valAt lst) pos ++ arrayPosValue vars xs pos
                                          Pointer ref -> let values = findPointerValues vars ref
                                                         in arrayPosValue vars values pos ++ arrayPosValue vars xs pos
                                          _ -> arrayPosValue vars xs pos

findPointerValues :: [Var] -> Name -> [Value]
findPointerValues [] _ = []
findPointerValues l n | (matchRegex (mkRegex "([a-zA-Z][a-zA-Z0-9]*)_([0-9]+)_([0-9]+)") n /= Nothing) = let s = SU.split "_" n
                                                                                                             v = (headNote ("findPointerValues - var '"++n++"'")) s
                                                                                                             p1 = (read::String -> Int) . (headNote ("findPointerValues - var '"++n++"'")) . tail $ s
                                                                                                             p2 = (read::String -> Int) . last $ s
                                                                                                             vals = snd . (headNote ("findPointerValues - var '"++v++"'")) . filter (\(tag, val) -> tag == v) $ l
                                                                                                         in arrayPosValue l (arrayPosValue l vals [p1]) [p2]
                      | (matchRegex (mkRegex "([a-zA-Z][a-zA-Z0-9]*)_([0-9]+)") n /= Nothing) = let s = SU.split "_" n
                                                                                                    v = (headNote ("findPointerValues - var '"++n++"'")) s
                                                                                                    p = (read::String -> Int) . last $ s
                                                                                                    vals = snd . (headNote ("findPointerValues - var '"++v++"'")) . filter (\(tag, val) -> tag == v) $ l
                                                                                                in arrayPosValue l vals [p]
                      | n == "" = []
                      | otherwise = snd . (headNote ("findPointerValues - var '"++n++"'")) . filter (\(tag, v) -> tag == n) $ l

createPointer :: [Var] -> Expr -> [Value]
createPointer lVars (Var n _) = [Pointer n]
createPointer lVars (Index (Var n _) exp _) = map (\x -> Pointer (n ++ "_" ++ x)) . map (show . intVal) $ (expVal lVars exp)
createPointer lVars (Index (Index (Var n _) exp1 _) exp2 _) = let e1 = expVal lVars exp1
                                                                  e2 = expVal lVars exp2
                                                                  i1 = map (show . intVal) e1
                                                                  i2 = map (show . intVal) e2
                                                              in map (\(a,b) -> Pointer (n ++ "_" ++ a ++ "_" ++ b)) (cartProd i1 i2)
createPointer lVars _ = [Pointer ""]

intVal :: Value -> Int
intVal (ValI a) = a
intVal (ValD a) = -1
intVal (ValC a) = ord a
intVal (ValS a) = -1
intVal (NULL) = -1
intVal (Unknown) = -1

divValue :: Value -> Value -> Value
divValue (ValI i1) (ValI i2) = ValI (div i1 i2)
divValue (ValD d1) (ValD d2) = ValD (d1/d2)
divValue (ValI i) (ValD d) = ValD ((fromIntegral i)/d)
divValue (ValD d) (ValI i) = ValD (d/(fromIntegral i))
divValue Unknown _ = Unknown
divValue _ Unknown = Unknown
divValue _ _ = error "divValue : Cannot divide values with such types"

remValue :: Value -> Value -> Value
remValue (ValI i1) (ValI i2) = ValI (rem i1 i2)
remValue _ _ = error "remValue : Cannot calculate Rem of non Integers"


execW :: Gr BB Flow -> Visited -> MapM -> LNode BB -> Configuration -> Int -> Int -> [Var] -> Bool -> (Double, [Var])
execW _ _ _ _ _ _ c v True = (fromIntegral (c+2), v)
execW gr visited m bb@(i, BB((While exp s _), _, _, _)) config max ct vars b | max == ct = (fromIntegral(max+1), vars)
                                                                             | isAllFalse (expVal vars exp) = (fromIntegral(ct+1), vars)
                                                                             | otherwise = let pNode = (headNote (show i++"--ERROR: No returning node for while loop. Bad formed CFG")) . filter (\(ind,b) -> ind > i) $ preBB gr i
                                                                                               nVars = eval gr visited m (i, vars) config pNode
                                                                                               cond = expVal nVars exp
                                                                                               (nCount, r) = if isAllFalse cond then (ct, True) else (ct+1, False)
                                                                                           in execW gr visited m bb config max nCount nVars r
execW _ _ _ _ _ _ _ v _ = (1, v)

evalExpr :: (Value -> Value -> Bool) -> Value -> Value -> Value
evalExpr f a b = if f a b then ValI 1 else ValI 0

negExpr :: Value -> Value
negExpr (ValI 0) = ValI 1
negExpr (ValI 1) = ValI 0
negExpr _ = Unknown

evalExpr' :: (Bool -> Bool -> Bool) -> Value -> Value -> Value
evalExpr' f a b = if f (boolVal a) (boolVal b) then ValI 1 else ValI 0


boolVal :: Value -> Bool
boolVal (ValI 0) = False
boolVal (ValI 1) = True
boolVal _ = False

xor :: Bool -> Bool -> Bool
xor True False = True
xor False True = True
xor _ _ = False

isAllFalse :: [Value] -> Bool
isAllFalse list = let l = length . filter (\x -> x |==| (ValI 0)) $ list 
                  in l == length list

getMaxIter :: Gr BB Flow -> Visited -> MapM -> LNode BB -> Configuration -> (Double, [Var])
getMaxIter cfg visited m bb@(i, (BB (While _ _ _, _, _, _))) conf = execW cfg visited m bb conf maxiter 0 (eval cfg visited m (i, []) conf bb) False
getMaxIter _ _ _ _ _ = (-1,[])
                                                --loop nodes--
allLoopBounds' :: Gr BB Flow -> Visited -> MapM -> [LNode BB] -> Configuration -> Visited
allLoopBounds' _ _ _ [] _ = []
allLoopBounds' gr visited m (l:loops) config = let iter = getMaxIter gr visited m l config
                                                   (i, b, v) = (fst l, fst iter, snd iter)
                                               in (i,b,v) : allLoopBounds' gr ((i,b,v):visited) m loops config

-- alternative to previous definition
-- works if the upper bounds are provided
allLoopBounds :: Gr BB Flow -> Visited -> MapM -> [LNode BB] -> Configuration -> Visited
allLoopBounds _ _ _ [] _ = []
allLoopBounds cfg v m ((i, l):loops) c = let tag = genLoopDescr l
                                             lst = dropWhile (\(a,b) -> a /= tag) boundsElevator -- boundsSumOfMultiples
                                             r = boundVal lst tag
                                         in (i, r, []):allLoopBounds cfg v m loops c

genLoopDescr :: BB -> String
genLoopDescr (BB((While c s p), d, z, y)) = case c of And c1 c2 _ -> genLoopDescr (BB((While c1 s p),d,z,y))
                                                      Or c1 c2 _ -> genLoopDescr (BB((While c1 s p),d,z,y))
                                                      Xor c1 c2 _ -> genLoopDescr (BB((While c1 s p),d,z,y))
                                                      Lt exp _ _ -> exprTag(exp)++"<"
                                                      Gt exp _ _ -> exprTag(exp)++">"
                                                      Le exp _ _ -> exprTag(exp)++"<="
                                                      Ge exp _ _ -> exprTag(exp)++">="
                                                      Eq exp _ _ -> exprTag(exp)++"=="
                                                      Neq exp _ _ -> exprTag(exp)++"!="
                                                      _ -> exprTag(c)

genLoopDescr _ = ""

exprTag :: Expr -> String
exprTag (ConstInt i _)    = show i
exprTag (ConstFloat d _)  = show d
exprTag (ConstChar c _)   = show c
exprTag (ConstString s _) = s
exprTag (Var n _)         = n
exprTag (MemInd _ tag _)  = tag
exprTag (Ind e _)     = "*" ++ (exprTag e)
exprTag (Index a b _) = (exprTag a) ++ "[" ++ exprTag(b) ++ "]"
exprTag (Mul a b _) = (exprTag a) ++ "*" ++ exprTag(b)
exprTag (Div a b _) = (exprTag a) ++ "/" ++ exprTag(b)
exprTag (Rmd a b _) = (exprTag a) ++ "%" ++ exprTag(b)
exprTag (Add a b _) = (exprTag a) ++ "+" ++ exprTag(b)
exprTag (Sub a b _) = (exprTag a) ++ "-" ++ exprTag(b)
exprTag (Shl a b _) = (exprTag a) ++ "<<" ++ exprTag(b)
exprTag (Shr a b _) = (exprTag a) ++ ">>" ++ exprTag(b)
exprTag (Minus a _) = "-" ++ (exprTag a)
exprTag (Comp a _)  = "~" ++ (exprTag a)
exprTag (Neg a _)   = "!" ++ (exprTag a)
exprTag (Cast t a _) = (exprTag a)
-- did we miss something?
exprTag a = error ("there is no tag for expr: " ++ (n_show a))  --[debug]

boundVal :: [(String, Double)] -> String -> Double
boundVal [] tag = error ("No Loop Bound for " ++ tag) --[debug]
boundVal l tag = max 2 ((snd . head $ l)+1)

-- aux...
cartProd :: [a] -> [b] -> [(a,b)]
cartProd xs ys = [(x,y) | x <- xs, y <- ys]

insertAt :: (Int, b) -> Int -> b -> [b] -> [b]
insertAt (i,v) c n [] | i == c = [v]
                      | otherwise = insertAt (i-c,v) 0 n . replicate (i-c) $ n
insertAt (i,v) c n (x:xs) = if i >= 0
                            then if c == i 
                                 then v:xs 
                                 else x:(insertAt (i,v) (c+1) n xs)
                            else (x:xs)

valAt :: [Value] -> Int -> Value
valAt [] _ = NULL
valAt (x:xs) i = if i > 0
                 then valAt xs (i-1)
                 else x

filterNodes :: Gr a b -> (LNode a -> Bool) -> [LNode a]
filterNodes gr f = filter f (labNodes gr)

loopNodes gr = filterNodes gr fun
             where fun (_, BB((While _ _ _), _, _, _)) = True
                   fun _ = False

endNode :: Gr BB b -> Int
endNode cfg = maximum (map fst (labNodes cfg))

third :: (a,b,c) -> c
third (_,_,c) = c

checkCFG :: Gr BB b -> Bool
checkCFG cfg = (length $ filter (\x -> (length x) /= 2) $ map (\(i,_) -> pre cfg i) loops) == 0
             where loops = loopNodes cfg

checkCFG' :: Gr BB b -> [Node]
checkCFG' cfg = map fst . filter (\(i,_) -> not(end `elem` (reachable i cfg))) $ loops
              where end = endNode cfg
                    loops = loopNodes cfg


n_show :: Expr -> String
n_show (ConstInt i _) = show i
n_show (ConstFloat d _) = show d
n_show (ConstChar c _) = show c
n_show (ConstString s _) = "string"
n_show (Var n _) = "Var " ++ n
n_show (Mul a b _) = "Mul (" ++ n_show a ++ ", " ++ n_show b ++ ")"
n_show (Div a b _) = "Div (" ++ n_show a ++ ", " ++ n_show b ++ ")"
n_show (Rmd a b _) = "Rmd (" ++ n_show a ++ ", " ++ n_show b ++ ")"
n_show (Add a b _) = "Add (" ++ n_show a ++ ", " ++ n_show b ++ ")"
n_show (Sub a b _) = "Sub (" ++ n_show a ++ ", " ++ n_show b ++ ")"
n_show (Shl a b _) = "Shl (" ++ n_show a ++ ", " ++ n_show b ++ ")"
n_show (Shr a b _) = "Shr (" ++ n_show a ++ ", " ++ n_show b ++ ")"
n_show (Lt a b _) = "Lt (" ++ n_show a ++ ", " ++ n_show b ++ ")"
n_show (Gt a b _) = "Gt (" ++ n_show a ++ ", " ++ n_show b ++ ")"
n_show (Le a b _) = "Le (" ++ n_show a ++ ", " ++ n_show b ++ ")"
n_show (Ge a b _) = "Ge (" ++ n_show a ++ ", " ++ n_show b ++ ")"
n_show (Eq a b _) = "Eq (" ++ n_show a ++ ", " ++ n_show b ++ ")"
n_show (Neq a b _) = "Neq (" ++ n_show a ++ ", " ++ n_show b ++ ")"
n_show (And a b _) = "And (" ++ n_show a ++ ", " ++ n_show b ++ ")"
n_show (Xor a b _) = "Xor (" ++ n_show a ++ ", " ++ n_show b ++ ")"
n_show (Or a b _) = "Or (" ++ n_show a ++ ", " ++ n_show b ++ ")"
n_show (Adr a _) = "Adr (" ++ n_show a ++ ")"
n_show (Ind a _) = "Ind (" ++ n_show a ++ ")"
n_show (Minus a _) = "Minus (" ++ n_show a ++ ")"
n_show (Comp a _) = "Comp (" ++ n_show a ++ ")"
n_show (Neg a _) = "Neg (" ++ n_show a ++ ")"
n_show (Cast t a _) = "Cast (" ++ n_show a ++ ")"
n_show (Index a b _) = "Index (" ++ n_show a ++ ", " ++ n_show b ++ ")"
n_show (Mem a n _) = "Mem (" ++ n_show a ++ "." ++ n ++ ")"
n_show (MemInd a n _) = "MemInd (" ++ n_show a ++ "->" ++ n ++ ")"
n_show (SizeT t _) = "SizeT"
n_show (SizeE a _) = "SizeE (" ++ n_show a ++ ")"
