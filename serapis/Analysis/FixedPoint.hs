module Analysis.FixedPoint 
(
   fixedPoint,
   localBounds, 
   genNodeInfos,
   getNodeInfo,
   updateStmt,
   update,
   updateByConfig,
   groupByConfig
) where

import Analysis.CFG
import Analysis.Models

import Language.CIL.Types
import Language.CIL.StmtCore

import Data.Graph.Inductive

import GHC.Base
import Data.List

type NodeInfos = [(Int, LiftedMapLattice)]

--type LiftedMapLattice = [(Configuration, [Component])]
type LiftedMapLattice = [(Configuration, Element)]

type Element = State
{-
data Component = CPU (Element Int) -- for now: 0->active; 1->inactive
    deriving (Eq, Show)

data Element a = Top
               | Bottom
               | Elem a -- 'a' can be anything: list, string, char, int, ...
    deriving (Eq, Show)
-}
machine = arqModel
initElem = initState

localBounds :: Gr BB Flow -> NodeInfos -> EnergyModel -> [(Name, Type)] -> Gr BB Flow
localBounds gr c model vars = let nodes = map (boundNode c model vars) . labNodes $ gr
                                  edges = labEdges gr
                              in mkGraph nodes edges

boundNode :: NodeInfos -> EnergyModel -> [(Name, Type)] -> LNode BB -> LNode BB
boundNode cons model vars (i, bb) = let ll = getNodeInfo cons i
                                        res = map (energyBound bb model vars) ll
                                    in case bb of BB (stmt, depth, conf, _) -> (i, BB(stmt, depth, conf, res))
                                                  Begin e -> (i, Begin res)
                                                  End e -> (i, End res)

energyBound :: BB -> EnergyModel -> [(Name, Type)] -> (Configuration, Element) -> EBound
energyBound (BB(stmt, _, conf, b)) model vars (c, e) | chkConfig conf c = (c, energyValue stmt e model vars)
                                                     | otherwise = (c, (0,[]))
energyBound _ _ _ (c, e) = (c, (0, []))

getNodeInfo :: NodeInfos -> Int -> LiftedMapLattice
getNodeInfo [] _ = error "getNodeInfo: no lattice found for index "
getNodeInfo ((j,l):t) i | i == j = l
                        | otherwise = getNodeInfo t i

fixedPoint :: Gr BB Flow -> NodeInfos -> NodeInfos
fixedPoint gr cl = let newC = map (update gr cl) cl
                       resC = if cl /= newC
                              then fixedPoint gr newC
                              else newC
                   in resC

genNodeInfos :: [Configuration] -> Gr BB Flow -> NodeInfos
genNodeInfos configs gr = let x = map (\c -> (c, initElem)) configs
                          in map (\(i, bb) -> (i, x)) . labNodes $ gr

-- generic update function 
--        The graph  -> the lattice before analysis -> the lattice after analysis
update :: Gr BB Flow -> NodeInfos -> (Int, LiftedMapLattice) -> (Int, LiftedMapLattice)
update gr cl c@(i, ll) = case lab gr i of Just label -> let prell | length (pre gr i) > 0 = (map (\x -> (x, getNodeInfo cl i)) (pre gr i))
                                                                  | otherwise = [c]
                                                        in updateStmt label i prell
                                          Nothing -> badLabel

-- :: Stmt to analyze -> Index -> (predecessors' index, lattice) -> the analysis result
updateStmt :: BB -> Int -> [(Int, LiftedMapLattice)] -> (Int, LiftedMapLattice)
updateStmt bb i llPre = (i, map (updateByConfig bb) joined)
                        where joined = map (\(x,y) -> (x, joinL y)) grpCnf
                              grpCnf = groupByConfig . map (\(a,b) -> b) $ llPre

groupByConfig :: [[(Configuration, Element)]] -> [(Configuration, [Element])]
groupByConfig [] = []
groupByConfig l = zip xs ys
                  where xs = fst $ unzip $ head l
                        ys = transpose $ map (snd . unzip) l

--                   ->      JOINED LATTICE      ->
updateByConfig :: BB -> (Configuration, Element) -> (Configuration, Element)
updateByConfig bb@(BB (s, _, conf, _)) a@(c, elem) | chkConfig conf c = (c, fun elem (bbToVoc bb))
                                                   | otherwise = a
                                                   where fun = getDelta machine
updateByConfig _ a = a

-- ERROR CODE --
badLabel :: a
badLabel = errorNoLabelForNode "update"

errorNoLabelForNode :: String -> a
errorNoLabelForNode fun = error (fun ++ " : non-existing node or no label found for node")
