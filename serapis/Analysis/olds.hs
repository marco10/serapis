eval :: Gr BB Flow -> Visited -> MapM -> (Int, [Var]) -> Configuration -> LNode BB -> [Var]
eval gr _ _ _ _ (i, Begin _ ) = []
eval gr visited m lVars conf node@(i, bb@(BB(s, c, eb))) =
  force $ if chkConfig c conf then aux1 else aux3
  where
    types = fetchTypes gr
    --
    aux1 = case s of
            VariableDef{} -> aux1VarDef s ++ aux3
            AssignExpr ex1 ex2 _ -> nubValues $ nubBy ((==) `on` fst) (evalAssign aux3 s ++ aux3)
            StmtApply{}   -> aux1Stmt   s
            AssignApply{} -> aux1AssApp s
            While{}       -> aux1Whl    s
            _ -> aux3
    --
    aux3 = aux4 (preBB gr i)
    --
    aux4 a = join (map (eval gr visited m lVars conf) a)
    --
    aux1VarDef (VariableDef name tp (Just init) _)              = [(name, initVal aux3 init)]
    aux1VarDef (VariableDef name (Array sz1 (Array sz2 t)) _ _) = [(name, initMatrix sz1 sz2 (varType types t))]
    aux1VarDef (VariableDef name (Array size t) _ _)            = [(name, initArray size (varType types t))]
    aux1VarDef (VariableDef name (Ptr t) _ p)                   = [("pointedBy_" ++ name, [resetByType (varType types t)])
                                                                  ,(name, createPointer [] (Var ("pointedBy_" ++ name) p))]
    aux1VarDef (VariableDef name tp _ _)                        = [(name, [resetByType (varType types tp)])]
    --
    aux1Stmt (StmtApply (Apply (Var n _) el) _) = nubValues $ nubFst (v ++ ff aux3 el ++ aux3)
      where v | isJust (matchRegex (mkRegex "srand") n) = [varInput m "pointedBy_imright" , varInput m "pointedBy_imleft"]
              | otherwise = []
    --
    aux1AssApp (AssignApply (Var n _) (Apply (Var f _) el)  _) = nubValues $ nubFst (v : (ff aux3 el) ++ aux3)
      where v | isJust (matchRegex (mkRegex "fread") f) = (n, [ValI 128]) -- used for a specific loop bound
              | otherwise = resetVar aux3 n
    --
    aux1Whl (While exp stm _) = fromJust ((third <$> (headMay $ dropWhile (\(a,_,_) -> a /= i) visited)) <|> Just alt)
      where alt = if i == (fst lVars)
                    then if null (snd lVars) then x else snd lVars
                    else if not (isNested gr (fst lVars) i)
                         then snd (execW gr visited m node conf maxiter 0 x False)
                         else x
            x = aux4 (filter ((<i) . fst) (preBB gr i))

nubFst = nubBy ((==) `on` fst)

--
join' :: [[Var]] -> [Var]
join' l = let tmp = (nub . concat . map extractVars) l   -- [String] with all var names
              joined = map (extractValues l) tmp
          in joined

--
extractVars :: [Var] -> [String]
extractVars a = map (\(x,y) -> x) a

--
extractValues :: [[Var]] -> String -> (String, [Value])
extractValues l s = (s, concat . concat . map f $ l)
                  where f h = map (\(a,b) -> b) . filter (\(x,y) -> x==s) $ h


-- ENERGY MODEL

exprEnergy (Add (Var _ _) (ConstInt _ _) _) s model vars = instructionEnergy "ASS_ADD_VAR_INT" s model
exprEnergy (Add (Var _ _) (ConstFloat _ _) _) s model vars = instructionEnergy "ASS_ADD_VAR_DOUBLE" s model
exprEnergy (Add e (ConstInt _ _) _) s model vars = exprEnergy e s model vars + instructionEnergy "ASS_ADD_VAR_INT" s model
exprEnergy (Add e (ConstFloat _ _) _) s model vars = exprEnergy e s model vars + instructionEnergy "ASS_ADD_VAR_DOUBLE" s model
exprEnergy (Add (ConstInt _ _) (Var _ _)_) s model vars = instructionEnergy "ASS_ADD_VAR_VAR_INT" s model
exprEnergy (Add (ConstFloat _ _) (Var _ _)_) s model vars = instructionEnergy "ASS_ADD_VAR_DOUBLE" s model
exprEnergy (Add (ConstInt _ _) e _) s model vars = exprEnergy e s model vars + instructionEnergy "ASS_ADD_VAR_INT" s model
exprEnergy (Add (ConstFloat _ _) e _) s model vars = exprEnergy e s model vars + instructionEnergy "ASS_ADD_VAR_DOUBLE" s model
exprEnergy (Add e1@(Var _ _) e _) s model vars | getExpType vars e1 `elem`[Int32, Int16] = exprEnergy e s model vars + instructionEnergy "ASS_ADD_VAR_VAR_INT" s model
                                               | getExpType vars e1 == Float = exprEnergy e s model vars + instructionEnergy "ASS_ADD_VAR_VAR_FLOAT" s model
                                               | getExpType vars e1 == Double = exprEnergy e s model vars + instructionEnergy "ASS_ADD_VAR_VAR_DOUBLE" s model
                                               | otherwise = instructionEnergy "ADDR_CALC" s model
exprEnergy (Add e e1@(Var _ _) _) s model vars | getExpType vars e1 `elem`[Int32, Int16] = exprEnergy e s model vars + instructionEnergy "ASS_ADD_VAR_VAR_INT" s model
                                               | getExpType vars e1 == Float = exprEnergy e s model vars + instructionEnergy "ASS_ADD_VAR_VAR_FLOAT" s model
                                               | getExpType vars e1 == Double = exprEnergy e s model vars + instructionEnergy "ASS_ADD_VAR_VAR_DOUBLE" s model
                                               | otherwise = instructionEnergy "ADDR_CALC" s model
exprEnergy (Add e1 e2 _) s model vars | getExpType vars e1 `elem`[Int32, Int16] = exprEnergy e1 s model vars + exprEnergy e2 s model vars + instructionEnergy "ASS_ADD_VAR_VAR_INT" s model
                                      | getExpType vars e1 == Float = exprEnergy e1 s model vars + exprEnergy e2 s model vars + instructionEnergy "ASS_ADD_VAR_VAR_FLOAT" s model
                                      | otherwise = exprEnergy e1 s model vars + exprEnergy e2 s model vars + instructionEnergy "ASS_ADD_VAR_VAR_DOUBLE" s model

exprEnergy (Sub (Var _ _) (ConstInt _ _) _) s model vars = instructionEnergy "ASS_SUB_VAR_INT" s model
exprEnergy (Sub (Var _ _) (ConstFloat _ _) _) s model vars = instructionEnergy "ASS_SUB_VAR_DOUBLE" s model
exprEnergy (Sub e (ConstInt _ _) _) s model vars = exprEnergy e s model vars + instructionEnergy "ASS_SUB_VAR_INT" s model
exprEnergy (Sub e (ConstFloat _ _) _) s model vars = exprEnergy e s model vars + instructionEnergy "ASS_SUB_VAR_DOUBLE" s model
exprEnergy (Sub (ConstInt _ _) (Var _ _)_) s model vars = instructionEnergy "ASS_SUB_VAR_VAR_INT" s model
exprEnergy (Sub (ConstFloat _ _) (Var _ _)_) s model vars = instructionEnergy "ASS_SUB_VAR_DOUBLE" s model
exprEnergy (Sub (ConstInt _ _) e _) s model vars = exprEnergy e s model vars + instructionEnergy "ASS_SUB_VAR_VAR_INT" s model
exprEnergy (Sub (ConstFloat _ _) e _) s model vars = exprEnergy e s model vars + instructionEnergy "ASS_SUB_VAR_DOUBLE" s model
exprEnergy (Sub e1@(Var _ _) e _) s model vars | getExpType vars e1 `elem`[Int32, Int16] = exprEnergy e s model vars + instructionEnergy "ASS_SUB_VAR_VAR_INT" s model
                                               | getExpType vars e1 == Float = exprEnergy e s model vars + instructionEnergy "ASS_SUB_VAR_VAR_FLOAT" s model
                                               | getExpType vars e1 == Double = exprEnergy e s model vars + instructionEnergy "ASS_SUB_VAR_VAR_DOUBLE" s model
                                               | otherwise = exprEnergy e s model vars + instructionEnergy "ASS_SUB_VAR_VAR_DOUBLE" s model  -- overestimation
exprEnergy (Sub e e1@(Var _ _) _) s model vars | getExpType vars e1 `elem`[Int32, Int16] = exprEnergy e s model vars + instructionEnergy "ASS_SUB_VAR_VAR_INT" s model
                                               | getExpType vars e1 == Float = exprEnergy e s model vars + instructionEnergy "ASS_SUB_VAR_VAR_FLOAT" s model
                                               | getExpType vars e1 == Double = exprEnergy e s model vars + instructionEnergy "ASS_SUB_VAR_VAR_DOUBLE" s model
                                               | otherwise = exprEnergy e s model vars + instructionEnergy "ASS_SUB_VAR_VAR_DOUBLE" s model  -- overestimation
exprEnergy (Sub e1 e2 _) s model vars | getExpType vars e1 `elem`[Int32, Int16] = exprEnergy e1 s model vars + exprEnergy e2 s model vars + instructionEnergy "ASS_SUB_VAR_VAR_INT" s model
                                      | getExpType vars e1 == Float = exprEnergy e1 s model vars + exprEnergy e2 s model vars + instructionEnergy "ASS_SUB_VAR_VAR_FLOAT" s model
                                      | otherwise = exprEnergy e1 s model vars + exprEnergy e2 s model vars + instructionEnergy "ASS_SUB_VAR_VAR_DOUBLE" s model

exprEnergy (Mul (Var _ _) (ConstInt _ _) _) s model vars = instructionEnergy "ASS_MUL_VAR_INT" s model
exprEnergy (Mul (Var _ _) (ConstFloat _ _) _) s model vars = instructionEnergy "ASS_MUL_VAR_DOUBLE" s model
exprEnergy (Mul e (ConstInt _ _) _) s model vars = exprEnergy e s model vars + instructionEnergy "ASS_MUL_VAR_INT" s model
exprEnergy (Mul e (ConstFloat _ _) _) s model vars = exprEnergy e s model vars + instructionEnergy "ASS_MUL_VAR_DOUBLE" s model
exprEnergy (Mul (ConstInt _ _) (Var _ _)_) s model vars = instructionEnergy "ASS_MUL_VAR_VAR_INT" s model
exprEnergy (Mul (ConstFloat _ _) (Var _ _)_) s model vars = instructionEnergy "ASS_MUL_VAR_DOUBLE" s model
exprEnergy (Mul (ConstInt _ _) e _) s model vars = exprEnergy e s model vars + instructionEnergy "ASS_MUL_VAR_VAR_INT" s model
exprEnergy (Mul (ConstFloat _ _) e _) s model vars = exprEnergy e s model vars + instructionEnergy "ASS_MUL_VAR_DOUBLE" s model
exprEnergy (Mul e1@(Var _ _) e _) s model vars | getExpType vars e1 `elem`[Int32, Int16] = exprEnergy e s model vars + instructionEnergy "ASS_MUL_VAR_VAR_INT" s model
                                               | getExpType vars e1 == Float = exprEnergy e s model vars + instructionEnergy "ASS_MUL_VAR_VAR_FLOAT" s model
                                               | getExpType vars e1 == Double = exprEnergy e s model vars + instructionEnergy "ASS_MUL_VAR_VAR_DOUBLE" s model
                                               | otherwise = exprEnergy e s model vars + instructionEnergy "ASS_MUL_VAR_VAR_DOUBLE" s model  --overestimation
exprEnergy (Mul e e1@(Var _ _) _) s model vars | getExpType vars e1 `elem`[Int32, Int16] = exprEnergy e s model vars + instructionEnergy "ASS_MUL_VAR_VAR_INT" s model
                                               | getExpType vars e1 == Float = exprEnergy e s model vars + instructionEnergy "ASS_MUL_VAR_VAR_FLOAT" s model
                                               | getExpType vars e1 == Double = exprEnergy e s model vars + instructionEnergy "ASS_MUL_VAR_VAR_DOUBLE" s model
                                               | otherwise = exprEnergy e s model vars + instructionEnergy "ASS_MUL_VAR_VAR_DOUBLE" s model  --overestimation
exprEnergy (Mul e1 e2 _) s model vars | getExpType vars e1 `elem`[Int32, Int16] = exprEnergy e1 s model vars + exprEnergy e2 s model vars + instructionEnergy "ASS_MUL_VAR_VAR_INT" s model
                                      | getExpType vars e1 == Float = exprEnergy e1 s model vars + exprEnergy e2 s model vars + instructionEnergy "ASS_MUL_VAR_VAR_FLOAT" s model
                                      | otherwise = exprEnergy e1 s model vars + exprEnergy e2 s model vars + instructionEnergy "ASS_MUL_VAR_VAR_DOUBLE" s model

exprEnergy (Div (Var _ _) (ConstInt _ _) _) s model vars = instructionEnergy "ASS_DIV_VAR_INT" s model
exprEnergy (Div (Var _ _) (ConstFloat _ _) _) s model vars = instructionEnergy "ASS_DIV_VAR_DOUBLE" s model
exprEnergy (Div e (ConstInt _ _) _) s model vars = exprEnergy e s model vars + instructionEnergy "ASS_DIV_VAR_INT" s model
exprEnergy (Div e (ConstFloat _ _) _) s model vars = exprEnergy e s model vars + instructionEnergy "ASS_DIV_VAR_DOUBLE" s model
exprEnergy (Div (ConstInt _ _) (Var _ _)_) s model vars = instructionEnergy "ASS_DIV_VAR_VAR_INT" s model
exprEnergy (Div (ConstFloat _ _) (Var _ _)_) s model vars = instructionEnergy "ASS_DIV_VAR_DOUBLE" s model
exprEnergy (Div (ConstInt _ _) e _) s model vars = exprEnergy e s model vars + instructionEnergy "ASS_DIV_VAR_VAR_INT" s model
exprEnergy (Div (ConstFloat _ _) e _) s model vars = exprEnergy e s model vars + instructionEnergy "ASS_DIV_VAR_DOUBLE" s model
exprEnergy (Div e1@(Var _ _) e _) s model vars | getExpType vars e1 `elem`[Int32, Int16] = exprEnergy e s model vars + instructionEnergy "ASS_DIV_VAR_VAR_INT" s model
                                               | getExpType vars e1 == Float = exprEnergy e s model vars + instructionEnergy "ASS_DIV_VAR_VAR_FLOAT" s model
                                               | getExpType vars e1 == Double = exprEnergy e s model vars + instructionEnergy "ASS_DIV_VAR_VAR_DOUBLE" s model
                                               | otherwise = 0
exprEnergy (Div e e1@(Var _ _) _) s model vars | getExpType vars e1 `elem`[Int32, Int16] = exprEnergy e s model vars + instructionEnergy "ASS_DIV_VAR_VAR_INT" s model
                                               | getExpType vars e1 == Float = exprEnergy e s model vars + instructionEnergy "ASS_DIV_VAR_VAR_FLOAT" s model
                                               | getExpType vars e1 == Double = exprEnergy e s model vars + instructionEnergy "ASS_DIV_VAR_VAR_DOUBLE" s model
                                               | otherwise = 0
exprEnergy (Div e1 e2 _) s model vars | getExpType vars e1 `elem`[Int32, Int16] = exprEnergy e1 s model vars + exprEnergy e2 s model vars + instructionEnergy "ASS_DIV_VAR_VAR_INT" s model
                                      | getExpType vars e1 == Float = exprEnergy e1 s model vars + exprEnergy e2 s model vars + instructionEnergy "ASS_DIV_VAR_VAR_FLOAT" s model
                                      | otherwise = exprEnergy e1 s model vars + exprEnergy e2 s model vars + instructionEnergy "ASS_DIV_VAR_VAR_DOUBLE" s model


-- CFG CREATION --
While e st p -> let newDepth = (0,(dp++[d]))
                    x = analyseNodes (num+1) newDepth config allF [st]
                    (a,b) = head . reverse $ x
                in (num+1, BB ((While e Null p), dp, config, [])) : x ++ analyseNodes a (d+1, dp) config allF ss