module Analysis.CFG 
(
    createNodes, 
    analyseNodes, 
    createEdges, 
    analyseEdges, 
    createCFG, 
    isStartNode, 
    isEndNode, 
    extendCase, 
    Flow(..), 
    Configuration(..), 
    BB(..), 
    EBound, 
    Depth, 
    chkConfig,
    checkEdges, 
    nodeDepth, 
    isNested,
    getEnergyTokens,
    --module SPL.Feature, 
    module Data.Graph.Inductive
) where

import Data.Graph.Inductive

import SPL.Feature

import Language.CIL.Types
import Language.CIL.StmtCore
import Language.C.Data.Position


data Flow = Condition Expr
          | Next
    deriving (Eq, Show)

{-
instance Eq Configuration where
	(==) All All = True
	(==) (Features l1) (Features l2) = l1 == l2
	(==) _ _ = False
-}

type Depth = [Int]

data BB = BB (Stmt, Depth, Configuration, [EBound])
        | Begin [EBound]
        | End [EBound]
    deriving Show

type EBound = (Configuration, (Double, [String]))

getEnergyTokens :: Configuration -> LNode BB -> [String] -- [EBound]
getEnergyTokens conf (i, (BB (s, d, c, e))) = concat . map (snd . snd) . filter (\(x,y) -> x == conf) $ e
getEnergyTokens conf (i, (Begin e)) = concat . map (snd . snd) . filter (\(x,y) -> x == conf) $ e
getEnergyTokens conf (i, (End e)) = concat . map (snd . snd) . filter (\(x,y) -> x == conf) $ e

createNodes :: Stmt -> [(Int, BB)]
createNodes s@(Compound _ sts _) = let allF = getFeatures s
                                       x = analyseNodes 0 (0,[]) All allF sts
                                       si = length x
                                   in (0, Begin []) : (si+1, End []) : x

testFF :: (Int, Int, Flow) -> [(Int, Int, Flow)] -> Bool
testFF _ [] = False
testFF (a,b,c) ((s,d,f):t) = if a==s && b==d
                             then True
                             else testFF (a,b,c) t

createEdges :: Stmt -> [(Int, Int, Flow)]
createEdges (Compound _ sts _) = let x = analyseEdges 0 0 sts
                                     end = length $ analyseNodes 0 (0,[]) All [] sts
                                     y = replaceEndNodes (end+1) x
                                     final = if not(testFF (end, end+1, Next) y)
                                             then [(end,end+1,Next)]
                                             else []
                                 in (0, 1, Next) : y ++ final

analyseNodes :: Int -> (Int,Depth) -> Configuration -> [Name] -> [Stmt] -> [(Int, BB)]
analyseNodes _ _ _ _ [] = []
analyseNodes num depth@(d,dp) config allF (s:ss) = case s of Null -> analyseNodes num depth config allF ss
                                                             StmtApply a _ -> if isFeature a 
                                                                              then analyseNodes num depth (nodeConfig a allF) allF ss 
                                                                              else if isEndFeature a 
                                                                                   then analyseNodes num depth All allF ss
                                                                                   else (num+1, BB (s, dp, config, [])) : analyseNodes (num+1) depth config allF ss
                                                             While e st p -> let newDepth = (0,(dp++[num+1]))
                                                                                 x = analyseNodes (num+1) newDepth config allF [st]
                                                                                 (a,b) = head . reverse $ x
                                                                             in (num+1, BB ((While e Null p), dp, config, [])) : x ++ analyseNodes a (d+1, dp) config allF ss
                                                             If e s1 s2 p -> let x = analyseNodes (num+1) depth config allF [s1]
                                                                                 (a,b) = if length x > 0
                                                                                         then head . reverse $ x
                                                                                         else (num+1, End [])
                                                                                 y = analyseNodes a depth config allF [s2]
                                                                                 (c,d) = if length y > 0
                                                                                         then head . reverse $ y
                                                                                         else (a, End [])
                                                                                 (n,nul) = (c+1, BB((Null), dp, config, []))
                                                                             in (num+1, BB ((If e Null Null p), dp, config, [])) : x ++ y ++ [(n,nul)] ++ analyseNodes n depth config allF ss
                                                             Switch e st p -> let ext = extendCase e st
                                                                                  x = analyseNodes num depth config allF [ext]
                                                                                  (a,b) = head . reverse $ x
                                                                              in x ++ analyseNodes a depth config allF ss
                                                             Case e st p -> let x = analyseNodes (num+1) depth config allF [st]
                                                                                (a,b) = if length x > 0
                                                                                        then head . reverse $ x
                                                                                        else (num+1, End [])
                                                                            in (num+1, BB ((Case e Null p), dp, config, [])) : x ++ analyseNodes a depth config allF ss
                                                             Default st p -> let x = analyseNodes num depth config allF [st]
                                                                                 (a,b) = if length x > 0
                                                                                         then head . reverse $ x
                                                                                         else (num, End [])
                                                                             in x ++ analyseNodes a depth config allF ss
                                                             FunctionDef _ _ _ st _-> let x = analyseNodes num depth config allF [st]
                                                                                          (a,b) = head . reverse $ x
                                                                                      in x ++ analyseNodes a depth config allF ss
                                                             Compound _ sts _ -> let x = analyseNodes num depth config allF sts
                                                                                     (a,b) = if length x > 0
                                                                                             then head . reverse $ x
                                                                                             else (num, End [])
                                                                                 in x ++ analyseNodes a depth config allF ss
                                                             VariableDef n _ _ _ -> if isFeature' n
                                                                                    then analyseNodes num depth (nodeConfig' n allF) allF ss
                                                                                    else if isEndFeature' n
                                                                                         then analyseNodes num depth All allF ss
                                                                                         else (num+1, BB (s, dp, config, [])) : analyseNodes (num+1) depth config allF ss
                                                             _ -> (num+1, BB (s, dp, config, [])) : analyseNodes (num+1) depth config allF ss    -- AssignExpr, AssignApply, TypeDecl, VariableDef, ...

analyseEdges :: Int -> Int -> [Stmt] -> [(Int, Int, Flow)]
analyseEdges _ _ [] = []
analyseEdges num end (s1:ss) = case s1 of Null -> analyseEdges num end ss
                                          StmtApply a p -> if not (isFeature a || isEndFeature a)
                                                           then (num+1, num+2, Next) : analyseEdges (num+1) end ss
                                                           else analyseEdges num end ss
                                          VariableDef n _ _ p -> if not (isFeature' n || isEndFeature' n)
                                                                 then (num+1, num+2, Next) : analyseEdges (num+1) end ss
                                                                 else analyseEdges num end ss
                                          While e st p -> let xn = analyseNodes (num+1) (0,[]) All [] [st]
                                                              x = analyseEdges (num+1) (num+1+(length xn)) [st]
                                                              b1 = fst . head $ xn
                                                              b2 = fst . head . reverse $ xn
                                                          in (num+1, num+2, Condition e) : (b2, num+1, Next) : (num+1, b2+1, (Condition (negateExpr e)))  : x ++ analyseEdges b2 (b2+1) ss
                                          If e st1 st2 p -> let nodes = analyseNodes (num+1) (0,[]) All [] [s1]
                                                                xn = analyseNodes (num+1) (0,[]) All [] [st1]
                                                                x = analyseEdges (num+1) (num+1+(length nodes)) [st1]
                                                                b1 = if length xn > 0
                                                                     then fst . head $ xn
                                                                     else num+1
                                                                b2 = if length xn > 0 
                                                                     then fst . head . reverse $ xn
                                                                     else num+1
                                                                yn = analyseNodes b2 (0,[]) All [] [st2]
                                                                y = analyseEdges b2 (num+1+(length nodes)) [st2]
                                                                e1 = if length yn > 0 
                                                                     then fst . head $ yn
                                                                     else b2
                                                                e2 = if length yn > 0 
                                                                     then fst . head . reverse $ yn
                                                                     else b2
                                                                sts1 = ifStmts st1
                                                                sts2 = ifStmts st2
                                                                xx1 = case (head . reverse $ sts1) of Return _ _ -> (b2, -1, Next) : x
                                                                                                      Break _ -> (b2, end+1, Next) : x
                                                                                                      Null -> x
                                                                                                      While _ _ _ -> x ++ [(b2, (e2+1), Next)] --problem here! An 'IF' stmt with an 'else' branch makes the while false edge point to 1 node earlier than it should
                                                                                                      _ -> (b2, (e2+1), Next) : x
                                                                xx2 = case (head . reverse $ sts2) of Return _ _ -> (e2, -1, Next) : y
                                                                                                      Break _ -> (e2, end+1, Next) : y
                                                                                                      Null -> y
                                                                                                      While _ _ _ -> y ++ [(e2, (e2+1), Next)] 
                                                                                                      _ -> (e2, (e2+1), Next) : y
                                                                edgesT = if length xx1 == 0 then [(num+1, e2+1, Condition e)] else [(num+1, b1, Condition e)]++xx1
                                                                edgesF = if length xx2 == 0 then [(num+1, e2+1, Condition (negateExpr e))] else [(num+1, e1, Condition (negateExpr e))]++xx2
                                                            in edgesT ++ edgesF ++ [(e2+1, e2+2, Next)] ++ analyseEdges (e2+1) (end+1) ss
                                          Switch e st p -> let ext = extendCase e st
                                                               x = analyseEdges num end [ext]
                                                               (a,b,c) = head . reverse $ x
                                                           in x ++ analyseEdges b (b+1) ss
                                          Case e st@(Compound _ sts _) p -> let x = analyseEdges (num+1) end [st]
                                                                                xn = analyseNodes (num+1) (0,[]) All [] [st]
                                                                                (a1,b1,c1) = if length x > 0 
                                                                                             then head x
                                                                                             else if length xn > 0 
                                                                                                  then (num+2, num+3, Next)
                                                                                                  else (num, num+1, Next)
                                                                                (a2,b2,c2) = if length x > 0
                                                                                             then head . reverse $ x
                                                                                             else if length xn > 0
                                                                                                  then (num+1, num+2, Next)
                                                                                                  else (num, num+1, Next)
                                                                                bn = b2+1
                                                                                (l1,l2,l3) = case (head . reverse $ sts) of Break _ -> (b2, end, Next)
                                                                                                                            Return _ _ -> (b2, -1, Next)
                                                                                                                            _ -> (b2, bn, Next)
                                                                                edges = if length xn == 0
                                                                                        then (num+1, bn, Condition e):(num+1, b2+1, Condition (negateExpr e)):x
                                                                                        else (num+1, a1, Condition e):(num+1, bn, Condition (negateExpr e)):(l1,l2,l3):x
                                                                            in edges ++ analyseEdges b2 end ss
                                          Default st@(Compound _ sts _) p -> let xn = analyseNodes num (0,[]) All [] [st]
                                                                                 x = analyseEdges num end [st]
                                                                                 (a,b,c) = if length x > 0 
                                                                                           then head . reverse $ x
                                                                                           else if length xn > 0 
                                                                                                then (num, num+1, Next)
                                                                                                else (0, 0, Next)
                                                                                 (l1,l2,l3) = case (head . reverse $ sts) of Return _ _ -> (b, -1, Next)
                                                                                                                             _ -> (b, b+1, Next)
                                                                                 edges = if length xn > 0 
                                                                                         then (l1,l2,l3):x 
                                                                                         else []
                                                                             in (edges) ++ analyseEdges b end ss
                                          FunctionDef _ _ _ st _-> let x = analyseEdges num end [st]
                                                                       (a,b,c) = head . reverse $ x
                                                                   in x ++ analyseEdges b end ss
                                          Compound _ sts _ -> analyseEdges num end (sts++ss)
                                          --Return e p -> (num+1, -1, Next) : analyseEdges (num+1) end ss
                                          --Break p -> (num+1, end, Next) : analyseEdges (num+1) end ss
                                          _ -> if length ss > 0 
                                               then (num+1, num+2, Next) : analyseEdges (num+1) end ss
                                               else []

createCFG :: Stmt -> Gr BB Flow
createCFG s = mkGraph nodes edges
  where nodes = createNodes s
        edges = reverse . checkEdges . reverse . createEdges $ s

-- AUXILIAR FUNCTIONS --
isNested :: Gr BB Flow -> Node -> Node -> Bool
isNested cfg child parent = parent `elem` (nodeDepth cfg child)

getDepth :: BB -> Depth
getDepth (BB (_,d,_,_)) = d
getDepth _ = []

nodeDepth :: Gr BB Flow -> Node -> Depth
nodeDepth cfg n = let label = lab cfg n
                      r = case label of Just bb -> getDepth bb
                                        _ -> error "No label found for node"
                  in r

checkEdges :: [(Int, Int, Flow)] -> [(Int, Int, Flow)]
checkEdges [] = []
checkEdges ((a,b,f):xs) = case f of Next -> if doubleEdge a f xs then checkEdges xs else (a,b,f):checkEdges xs
                                    _ -> (a,b,f):checkEdges xs

doubleEdge :: Int -> Flow -> [(Int, Int, Flow)] -> Bool
doubleEdge index flow list = length (filter(\(s,d,f) -> s == index && flow == f) list) > 0

ifStmts :: Stmt -> [Stmt]
ifStmts (Compound _ [] _) = [Null]
ifStmts (Compound _ s _) = s
ifStmts (Null) = [Null]
ifStmts a = [a]

isStartNode :: BB -> Bool
isStartNode (Begin _) = True
isStartNode _ = False

isEndNode :: BB -> Bool
isEndNode (End _) = True
isEndNode _ = False

extendCase :: Expr -> Stmt -> Stmt
extendCase e s = case s of Compound n sts p -> Compound n (groupCases . map (extendCase e) $ sts) p
                           Case e2 s p -> Case (Eq e e2 p) s p
                           _ -> s

groupCases :: [Stmt] -> [Stmt]
groupCases [] = []
groupCases (h:t) = case h of Case e s p -> (Case e (Compound [] (s:removeCases t) p) p) : groupCases t
                             Default s p -> (Default (Compound [] (s:removeCases t) p) p): groupCases t
                             _ -> groupCases t

removeCases :: [Stmt] -> [Stmt]
removeCases [] = []
removeCases (h:t) = case h of Case _ _ _ -> []
                              Default _ _ -> []
                              _ -> h:removeCases t

replaceEndNodes :: Int -> [(Int, Int, Flow)] -> [(Int, Int, Flow)]
replaceEndNodes _ [] = []
replaceEndNodes end (o@(s,d,f):xs) = if d == -1
                                     then (s, end, f):replaceEndNodes end xs
                                     else o:replaceEndNodes end xs

negateExpr :: Expr -> Expr
negateExpr e = case e of Lt     exp1 exp2 p -> Ge exp1 exp2 p
                         Gt     exp1 exp2 p -> Le exp1 exp2 p
                         Le     exp1 exp2 p -> Gt exp1 exp2 p
                         Ge     exp1 exp2 p -> Lt exp1 exp2 p
                         Eq     exp1 exp2 p -> Neq exp1 exp2 p
                         Neq    exp1 exp2 p -> Eq exp1 exp2 p
                         Neg    exp1 p -> exp1
                         And    exp1 exp2 p -> Or (negateExpr exp1) (negateExpr exp2) p
                         Xor    exp1 exp2 p -> Eq exp1 exp2 p
                         Or     exp1 exp2 p -> And (negateExpr exp1) (negateExpr exp2) p
                         Var    exp1 p -> Eq (Var exp1 p) (ConstInt 0 p) p
                         Ind    exp1 p -> Eq (Ind exp1 p) (ConstInt 0 p) p
                         Mem    exp1 name p -> Eq (Mem exp1 name p) (ConstInt 0 p) p
                         MemInd exp1 name p -> Eq (MemInd exp1 name p) (ConstInt 0 p) p
                         Index  exp1 exp2 p -> Eq (Index exp1 exp2 p) (ConstInt 0 p) p
                         Cast   tp exp2 p -> negateExpr exp2
                         ConstInt i p -> Neg (ConstInt (i) p) p
                         _ -> e
