module Utils.Util where

import Data.List

countGroup :: Eq a => [a] -> [(a, Int)]
countGroup [] = []
countGroup (x:xs) = (x, 1+length (takeWhile (==x) xs)):countGroup (dropWhile (==x) xs)

count :: Eq a => a -> [a] -> Int
count x = length . filter (==x)


