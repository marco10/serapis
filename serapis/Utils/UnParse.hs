module Utils.UnParse 
(
    unParse
) where

import Language.CIL
import Language.CIL.Types
import Language.CIL.StmtCore

-- Unparsing & transformation to String functions

-- not at 100%
unParse :: Stmt -> String
unParse s = case s of Null -> ""
                      Compound n stmts p -> foldr (++) "" $ map unParse stmts
                      TypeDecl name t p -> typeToString t ++ " " ++ name ++ ";\n"
                      VariableDef name t (Just init) p -> typeToString t ++ " " ++ name ++ " = " ++ initToString init ++ ";\n"
                      VariableDef name t (Nothing) p -> typeToString t ++ " " ++ name ++ "\n"
                      FunctionDef name t args s p -> typeToString t ++ " " ++ name ++ "(" ++ (foldr (++) "" (map argsToString args)) ++ "){\n" ++  unParse s ++ "\n}"
                      AssignApply exp ap p -> expToString exp ++ " = " ++ applyToString ap ++ ";\n"
                      AssignExpr exp1 exp2 p -> expToString exp1 ++ " = " ++ expToString exp2 ++ ";\n"
                      StmtApply ap p -> applyToString ap ++ ";"
                      While exp s p -> "while(" ++ expToString exp ++"){\n" ++ unParse s ++ "}\n"
                      If exp s1 s2 p -> "if(" ++ expToString exp ++"){\n" ++ unParse s1 ++ "}else{\n" ++ unParse s2 ++ "}\n"
                      Return (Just exp) p -> "return " ++ expToString exp ++ ";\n"
                      Return (Nothing) p -> "return;\n"
                      Goto name p -> []
                      Break p -> "break;\n"
                      Switch exp s p -> "switch " ++ expToString exp ++ "{" ++ unParse s ++ "}\n"
                      Case exp s p -> "case " ++ expToString exp ++ ":\n" ++ unParse s
                      Default s p -> "default: \n" ++ unParse s

typeToString :: Type -> String
typeToString t = case t of Void -> "void"
                           Array i t -> typeToString t ++ "[" ++ show i ++ "]"
                           Ptr t -> typeToString t ++ "*"
                           Volatile t -> []
                           Typedef t -> []
                           Struct args -> []
                           Union args -> []
                           Enum args -> []
                           BitField t args -> []
                           StructRef  n -> []
                           UnionRef   n -> []
                           EnumRef    n -> []
                           TypedefRef n -> []
                           Function t ts -> []
                           Int8 -> "int"
                           Int16 -> "int"
                           Int32 -> "int"
                           Word8 -> []
                           Word16 -> []
                           Word32 -> []
                           Float -> "float"
                           Double -> "double"

initToString :: Init -> String
initToString i = case i of Init e -> expToString e
                           InitList il -> foldr (++) "" (map initToString il)

applyToString :: Apply -> String
applyToString (Apply exp expl) = expToString exp ++ "(" ++ foldr (++) "" (map expToString expl) ++ ");"

expToString :: Expr -> String
expToString e = case e of ConstInt    i    p -> show i
                          ConstFloat  d p -> show d
                          ConstChar   c   p -> show c
                          ConstString s p -> s
                          Var    name      p -> name
                          Mul    exp1 exp2 p -> expToString exp1 ++ "*" ++ expToString exp2
                          Div    exp1 exp2 p -> expToString exp1 ++ "/" ++ expToString exp2
                          Rmd    exp1 exp2 p -> expToString exp1 ++ "%" ++ expToString exp2
                          Add    exp1 exp2 p -> expToString exp1 ++ "+" ++ expToString exp2
                          Sub    exp1 exp2 p -> expToString exp1 ++ "-" ++ expToString exp2
                          Shl    exp1 exp2 p -> expToString exp1 ++ "<<" ++ expToString exp2
                          Shr    exp1 exp2 p -> expToString exp1 ++ ">>" ++ expToString exp2
                          Lt     exp1 exp2 p -> expToString exp1 ++ "<" ++ expToString exp2
                          Gt     exp1 exp2 p -> expToString exp1 ++ ">" ++ expToString exp2
                          Le     exp1 exp2 p -> expToString exp1 ++ "<=" ++ expToString exp2
                          Ge     exp1 exp2 p -> expToString exp1 ++ ">=" ++ expToString exp2
                          Eq     exp1 exp2 p -> expToString exp1 ++ "==" ++ expToString exp2
                          Neq    exp1 exp2 p -> expToString exp1 ++ "!=" ++ expToString exp2
                          And    exp1 exp2 p -> expToString exp1 ++ "&&" ++ expToString exp2
                          Xor    exp1 exp2 p -> expToString exp1 ++ "^" ++ expToString exp2
                          Or     exp1 exp2 p -> expToString exp1 ++ "||" ++ expToString exp2
                          Adr    exp      p -> "&" ++ expToString exp
                          Ind    exp      p -> "*" ++ expToString exp
                          Minus  exp      p -> "-" ++ expToString exp
                          Comp   exp      p -> "~" ++ expToString exp
                          Neg    exp      p -> "!" ++ expToString exp
                          Cast   t exp p -> "(" ++ typeToString t ++ ")" ++ expToString exp
                          Index  exp1 exp2 p -> expToString exp1 ++ "[" ++ expToString exp2 ++ "]"
                          Mem    exp name p -> expToString exp ++ "." ++ name
                          MemInd exp name p -> expToString exp ++ "->" ++ name
                          SizeT  t      p -> [] -- sizeof(type)
                          SizeE  exp      p -> [] -- sizeof(expr)

argsToString :: (Name, Type) -> String
argsToString (n,t) = typeToString t ++ " " ++ n