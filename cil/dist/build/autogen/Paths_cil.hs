{-# LANGUAGE CPP #-}
{-# OPTIONS_GHC -fno-warn-missing-import-lists #-}
{-# OPTIONS_GHC -fno-warn-implicit-prelude #-}
module Paths_cil (
    version,
    getBinDir, getLibDir, getDataDir, getLibexecDir,
    getDataFileName, getSysconfDir
  ) where

import qualified Control.Exception as Exception
import Data.Version (Version(..))
import System.Environment (getEnv)
import Prelude

#if defined(VERSION_base)

#if MIN_VERSION_base(4,0,0)
catchIO :: IO a -> (Exception.IOException -> IO a) -> IO a
#else
catchIO :: IO a -> (Exception.Exception -> IO a) -> IO a
#endif

#else
catchIO :: IO a -> (Exception.IOException -> IO a) -> IO a
#endif
catchIO = Exception.catch

version :: Version
version = Version [0,1,1] []
bindir, libdir, datadir, libexecdir, sysconfdir :: FilePath

bindir     = "/home/marco/.cabal/bin"
libdir     = "/home/marco/.cabal/lib/x86_64-linux-ghc-7.10.3/cil-0.1.1-C4QPIGATGuVGbFaq4Yg9v3"
datadir    = "/home/marco/.cabal/share/x86_64-linux-ghc-7.10.3/cil-0.1.1"
libexecdir = "/home/marco/.cabal/libexec"
sysconfdir = "/home/marco/.cabal/etc"

getBinDir, getLibDir, getDataDir, getLibexecDir, getSysconfDir :: IO FilePath
getBinDir = catchIO (getEnv "cil_bindir") (\_ -> return bindir)
getLibDir = catchIO (getEnv "cil_libdir") (\_ -> return libdir)
getDataDir = catchIO (getEnv "cil_datadir") (\_ -> return datadir)
getLibexecDir = catchIO (getEnv "cil_libexecdir") (\_ -> return libexecdir)
getSysconfDir = catchIO (getEnv "cil_sysconfdir") (\_ -> return sysconfdir)

getDataFileName :: FilePath -> IO FilePath
getDataFileName name = do
  dir <- getDataDir
  return (dir ++ "/" ++ name)
